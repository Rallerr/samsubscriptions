import React, { useEffect, useState } from 'react';
import SidePanel from '../../Components/SidePanel/SidePanel';
import {
  GetEnterpriseInfoByIdFromHubspot,
  GetEnterprisesFromHubspot,
} from '../../Services/EnterpriseService';
import './EnterprisePage.css';
import { Redirect } from 'react-router';
import { ValidateToken } from '../../Services/AuthService';
import InputField from '../../Components/InputField/InputField';

const EnterprisePage = ({ authenticated, setAuthenticated }) => {
  const [enterprises, setEnterprises] = useState([{}]);
  const [selectedEnterprise, setSelectedEnterprise] = useState({});
  const [activeMenuItem, setActiveMenuItem] = useState(' ');
  const [enterpriseLoaded, setEnterpriseLoaded] = useState(false);
  const [editModeDisabled, setEditModeDisabled] = useState(true);

  //Check if token is valid
  useEffect(() => {
    const auth = async () => {
      const isAuth = await ValidateToken();
      sessionStorage.setItem('authenticated', isAuth);
      if (isAuth === false) {
        setAuthenticated(false);
      }
    };

    auth();
  });

  //Get enterprises
  useEffect(() => {
    const getData = async () => {
      const data = await GetEnterprisesFromHubspot();
      console.log(data);
      setEnterprises(data);
    };
    getData();
  }, []);

  //Redirect to login if token not valid
  if (sessionStorage.getItem('authenticated') === 'false') {
    sessionStorage.removeItem('user');
    return <Redirect to='/login' />;
  }

  //Get info about selected enterprise
  const handleOnItemClick = async (enterpriseName) => {
    var clickedItem = document.getElementById(enterpriseName);
    clickedItem.classList.add('active');

    var enterpriseId = clickedItem.dataset.id;

    var prevClickedItem = document.getElementById(activeMenuItem);
    if (prevClickedItem !== null) {
      prevClickedItem.classList.remove('active');
    }

    const data = await GetEnterpriseInfoByIdFromHubspot(enterpriseId);
    console.log(data);
    setSelectedEnterprise(data);
    setActiveMenuItem(enterpriseName);
    setEnterpriseLoaded(true);
  };

  //Update state when enterprise is selected
  const handleChange = (e) => {
    const { name, value } = e.target;
    setSelectedEnterprise((prevEnterprise) => ({
      ...prevEnterprise,
      [name]: value,
    }));
  };

  return (
    <div className='enterprise-grid-container'>
      <div className='enterprise-side-menu'>
        <SidePanel menuItems={enterprises} onItemClick={handleOnItemClick} />
      </div>
      <div className='enterprise-content'>
        <form className='enterprise-form'>
          <div className='enterprise-no-name-container'>
            <div className='no-container'>
              <InputField
                label='No.'
                type='text'
                id='enterpriseNo'
                className='inputfield'
                name='companyId'
                disabled={editModeDisabled}
                onChange={handleChange}
                value={
                  enterpriseLoaded === true ? selectedEnterprise.companyId : 0
                }
              />
            </div>
            <div className='enterprise-name-container'>
              <InputField
                id='name'
                type='text'
                name='properties.name.value'
                label='Name'
                className='inputfield'
                disabled={editModeDisabled}
                onChange={handleChange}
                value={enterpriseLoaded === true ? selectedEnterprise.name : ''}
              />
            </div>
          </div>
          <br />
          <div className='enterprise-phone-invoiceemail-container'>
            <div className='enterprise-phone-container'>
              <InputField
                className='inputfield'
                label='Phone'
                id='phone'
                type='text'
                name='phone'
                disabled={editModeDisabled}
                onChange={handleChange}
                value={enterpriseLoaded ? selectedEnterprise.phone : ''}
              />
            </div>
            <div className='enterprise-invoiceemail-container'>
              <InputField
                className='inputfield'
                label='Invoice Email'
                id='invoiceEmail'
                type='email'
                name='invoiceEmail'
                disabled={editModeDisabled}
                /*onChange={handleChange}
              value={enterpriseLoaded ? selectedEnterprise.invoiceEmail : ''}*/
              />
            </div>
          </div>
          <br />
          <div className='enterprise-address-container'>
            <InputField
              className='inputfield'
              label='Address'
              type='text'
              id='address'
              name='address'
              disabled={editModeDisabled}
              onChange={handleChange}
              value={enterpriseLoaded ? selectedEnterprise.address : ''}
            />
          </div>
          <div className='enterprise-zip-city-country-container'>
            <div className='enterprise-zip-container'>
              <InputField
                className='inputfield'
                label='Zip'
                type='number'
                id='zip'
                name='zip'
                disabled={editModeDisabled}
                onChange={handleChange}
                value={enterpriseLoaded ? selectedEnterprise.zip : 0}
              />
            </div>
            <div className='enterprise-city-container'>
              <InputField
                className='inputfield'
                label='City'
                id='city'
                type='text'
                name='city'
                disabled={editModeDisabled}
                onChange={handleChange}
                value={enterpriseLoaded ? selectedEnterprise.city : ''}
              />
            </div>
            <div className='enterprise-country-container'>
              <InputField
                className='inputfield'
                label='Country'
                type='text'
                id='country'
                name='country'
                disabled={editModeDisabled}
                onChange={handleChange}
                value={enterpriseLoaded ? selectedEnterprise.country : ''}
              />
            </div>
          </div>
          <div className='enterprise-adminname-container'>
            <InputField
              className='inputfield'
              label='Admin Name'
              type='text'
              id='adminName'
              name='adminName'
              disabled={editModeDisabled}
              /*onChange={handleChange}
              value={enterpriseLoaded ? selectedEnterprise.adminName : ''}*/
            />
          </div>
          <div className='enterprise-adminemail-container'>
            <InputField
              className='inputfield'
              label='Admin Email'
              type='text'
              id='adminEmail'
              name='adminEmail'
              disabled={editModeDisabled}
              /*onChange={handleChange}
              value={enterpriseLoaded ? selectedEnterprise.adminEmail : ''}*/
            />
          </div>
          {/*<div className='view-subscription-button-container'>
            <input type='button' value='View subscription' />
          </div>*/}
        </form>
      </div>
    </div>
  );
};

export default EnterprisePage;
