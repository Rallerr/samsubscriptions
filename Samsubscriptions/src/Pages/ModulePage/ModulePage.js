import React, { useEffect, useState } from 'react';
import DeleteButton from '../../Components/DeleteButton/DeleteButton';
import EditButton from '../../Components/EditButton/EditButton';
import SaveButton from '../../Components/SaveButton/SaveButton';
import SidePanel from '../../Components/SidePanel/SidePanel';
import UndoButton from '../../Components/UndoButton/UndoButton';
import {
  CreateModule,
  GetModuleById,
  GetModules,
  UpdateModule,
} from '../../Services/ModuleService';
import './ModulePage.css';
import CheckboxTable from '../../Components/CheckboxTable/CheckboxTable';
import {
  CreateModuleSetting,
  GetModuleSettingsByModule,
} from '../../Services/ModuleSettingService';
import { Redirect } from 'react-router';
import { ValidateToken } from '../../Services/AuthService';
import {
  CreateRequiredModule,
  GetRequiredModulesByModuleId,
} from '../../Services/RequiredModuleService';
import InfoPopup from '../../Components/InfoPopup/InfoPopup';
import NewModuleSetting from '../../Components/NewModuleSetting/NewModuleSetting';
import AddButton from '../../Components/AddButton/AddButton';
import ModuleSettingTable from '../../Components/ModuleSettingTable/ModuleSettingTable';
import InputField from '../../Components/InputField/InputField';
import EconomicsProductGroupDropdown from '../../Components/EconomicsProductGroupDropdown/EconomicsProductGroupDropdown';
import ModuleGroupDropdown from '../../Components/ModuleGroupDropdown/ModuleGroupDropdown';
import PaymentFrequencyDropdown from '../../Components/PaymentFrequencyDropdown/PaymentFrequencyDropdown';
import {
  CreateEconomicsProduct,
  GetEconomicsProductById,
  UpdateEconomicsProduct,
} from '../../Services/EconomicsServices/EconomicsProductService';
import _ from 'lodash';
import EconomicsUnitDropdown from '../../Components/EconomicsUnitDropdown/EconomicsUnitDropdown';
import LoadingSpinner from '../../Components/LoadingSpinner/LoadingSpinner';

const ModulePage = ({
  authenticated,
  setAuthenticated,
  editState,
  setEditState,
}) => {
  const [modules, setModules] = useState([]);
  const [showLoading, setShowLoading] = useState(false);
  const [activeMenuItem, setActiveMenuItem] = useState();
  const [selectedModuleGroup, setSelectedModuleGroup] = useState();
  const [selectedPaymentFrequency, setSelectedPaymentFrequency] = useState();
  const [selectedModule, setSelectedModule] = useState({
    id: '',
    name: '',
    price: 0,
    description: '',
    groupId: '',
    paymentFrequencyId: '',
    samModule: false,
    firsttimePrice: 0,
    economicsProductId: '',
  });
  const [moduleSettings, setModuleSettings] = useState([]);
  const [enabledRequiredModules, setEnabledRequiredModules] = useState([]);
  const [undoButtonDisabled, setUndoButtonDisabled] = useState(true);
  const [editButtonDisabled, setEditButtonDisabled] = useState(true);
  const [saveButtonDisabled, setSaveButtonDisabled] = useState(true);
  const [deleteSettingButtonDisabled, setDeleteSettingButtonDisabled] =
    useState(true);
  const [addSettingButtonDisabled, setAddSettingButtonDisabled] =
    useState(true);
  const [selectedRequiredModules, setSelectedRequiredModules] = useState([]);
  const [popupText, setPopupText] = useState();
  const [showPopup, setShowPopup] = useState(false);
  const [showNewModuleSetting, setShowNewModuleSetting] = useState(false);
  const [selectedEconomicsProductGroup, setSelectedEconomicsProductGroup] =
    useState();
  const [economicsProduct, setEconomicsProduct] = useState();
  const [selectedEconomicsUnit, setSelectedEconomicsUnit] = useState();
  const [editModeDisabled, setEditModeDisabled] = useState(true);

  //Check if token is valid
  useEffect(() => {
    const auth = async () => {
      const isAuth = await ValidateToken();
      sessionStorage.setItem('authenticated', isAuth);
      setAuthenticated(isAuth);
    };

    auth();
  }, [setAuthenticated]);

  useEffect(() => {
    setShowLoading(true);
    const getData = async () => {
      const data = await GetModules();
      setModules(data);
      setShowLoading(false);
    };
    getData();
  }, []);

  useEffect(() => {
    if (editState === '' || editState === undefined) {
      disableEditMode();
      //setEditButtonDisabled(true);
    }

    if (editState === 'NEW_MODULE') {
      resetModule();

      enableEditMode();

      setAddSettingButtonDisabled(false);
      setDeleteSettingButtonDisabled(false);

      const checkboxes = document
        .getElementById('modules-settings-required-container')
        .getElementsByTagName('input');

      for (let i = 0; i < checkboxes.length; i++) {
        checkboxes[i].removeAttribute('disabled', '');
      }
    }
  }, [editState]);

  if (sessionStorage.getItem('authenticated') === 'false') {
    sessionStorage.removeItem('user');
    return <Redirect to='/login' />;
  }

  const resetModule = () => {
    setSelectedModule({
      id: '',
      name: '',
      price: 0,
      description: '',
      groupId: '',
      paymentFrequencyId: '',
      samModule: false,
      firsttimePrice: 0,
      economicsProductId: '',
    });
    setModuleSettings([]);
    setEnabledRequiredModules([]);
    setSelectedRequiredModules([]);
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setSelectedModule((prevModule) => ({
      ...prevModule,
      [name]: value,
    }));
  };

  const handleMenuItemClick = async (moduleId) => {
    setShowLoading(true);
    var clickedItem = document.getElementById(moduleId);
    clickedItem.classList.add('active');

    var prevClickedItem = document.getElementById(activeMenuItem);
    if (prevClickedItem !== null && prevClickedItem !== clickedItem) {
      prevClickedItem.classList.remove('active');
    }

    //Get module info
    const moduleData = await GetModuleById(moduleId);
    const settings = await GetModuleSettingsByModule(moduleData.id);
    const reqModules = await GetRequiredModulesByModuleId(moduleId);
    const economicsProductInfo = await GetEconomicsProductById(
      moduleData.economicsProductId
    );

    //Set module info
    if (economicsProductInfo !== undefined) {
      setEconomicsProduct(economicsProductInfo);
      setSelectedEconomicsProductGroup(
        economicsProductInfo.productGroup.productGroupNumber
      );
      setSelectedEconomicsUnit(economicsProductInfo.unit.unitNumber);
    }

    setSelectedModule(moduleData);
    setSelectedModuleGroup(moduleData.groupId);
    setSelectedPaymentFrequency(moduleData.paymentFrequencyId);
    setModuleSettings(settings);
    setEnabledRequiredModules(reqModules);
    setActiveMenuItem(moduleId);
    setEditButtonDisabled(false);
    setShowLoading(false);
  };

  const enableEditMode = () => {
    setSaveButtonDisabled(false);
    setUndoButtonDisabled(false);
    setEditButtonDisabled(true);
    setEditModeDisabled(false);
  };

  //Disable all inputs
  const disableEditMode = () => {
    setSaveButtonDisabled(true);
    setEditButtonDisabled(false);
    setUndoButtonDisabled(true);
    setEditModeDisabled(true);
  };

  const handleEditButtonClick = (e) => {
    e.preventDefault();

    setEditState('EDIT_MODULE');

    enableEditMode();
  };

  const handleUndoButtonClick = (e) => {
    e.preventDefault();

    disableEditMode();

    if (editState === 'EDIT_MODULE') {
      handleMenuItemClick(selectedModule.id);
      setEditButtonDisabled(false);
      setEditState();
      return;
    }

    if (editState === 'NEW_MODULE') {
      //resetModule();
      setEditState();
      disableEditMode();
      return;
    }

    if (selectedModule === undefined || selectedModule.id === '') {
      setEditButtonDisabled(true);
      return;
    }
  };
  const handleSaveButtonClick = async () => {
    if (editState === 'NEW_MODULE') {
      setShowLoading(true);
      try {
        const newEconomicsProduct = {
          name: selectedModule.name,
          description: selectedModule.description,
          salesPrice: selectedModule.price,
          barred: false,
          productGroup: {
            productGroupNumber: selectedEconomicsProductGroup,
          },
          unit: {
            unitNumber: selectedEconomicsUnit,
          },
        };

        const createdEconomicsProduct = await CreateEconomicsProduct(
          newEconomicsProduct
        );

        const newModule = {
          name: selectedModule.name,
          description: selectedModule.description,
          price: selectedModule.price,
          firsttimePrice: selectedModule.firsttimePrice,
          samModule: selectedModule.samModule,
          groupId: selectedModuleGroup,
          paymentFrequencyId: selectedPaymentFrequency,
          economicsProductId: createdEconomicsProduct.productNumber,
        };
        const createdModule = await CreateModule(newModule);

        if (selectedRequiredModules.length > 0) {
          selectedRequiredModules.map(async (item) => {
            const newRequiredModule = {
              moduleId: createdModule.id,
              requiredModuleId: item.id,
            };

            await CreateRequiredModule(newRequiredModule);
          });
        }

        //Create module settings
        moduleSettings.map(async (item) => {
          const newSetting = {
            name: item.name,
            type: item.type,
            moduleId: createdModule.id,
          };
          await CreateModuleSetting(newSetting);
        });
      } catch (err) {
        setPopupText('Creating module failed');
        setShowPopup(true);
        setShowLoading(false);
        return;
      }
      //resetModule();
      disableEditMode();
      setPopupText('Module created');
      setShowPopup(true);
      setEditState();
      setShowLoading(false);
      window.location.reload();
      return;
    }

    if (editState === 'EDIT_MODULE') {
      try {
        setShowLoading(true);
        const oldModule = await GetModuleById(selectedModule.id);

        selectedModule.groupId = selectedModuleGroup;
        selectedModule.paymentFrequencyId = selectedPaymentFrequency;
        await UpdateModule(selectedModule);

        if (_.isEqual(oldModule, selectedModule) === false) {
          await UpdateModule(selectedModule);
        }

        economicsProduct.name = selectedModule.name;
        economicsProduct.description = selectedModule.description;
        economicsProduct.salesPrice = selectedModule.price;
        economicsProduct.barred = false;
        economicsProduct.productGroup.productGroupNumber =
          selectedEconomicsProductGroup;
        economicsProduct.unit.unitNumber = selectedEconomicsUnit;

        const oldEconomicsProduct = await GetEconomicsProductById(
          economicsProduct.productNumber
        );

        if (_.isEqual(oldEconomicsProduct, economicsProduct) === false) {
          await UpdateEconomicsProduct(economicsProduct);
        }
      } catch (err) {
        setPopupText('Update module failed');
        setShowPopup(true);
        setShowLoading(false);
        return;
      }

      disableEditMode();
      handleMenuItemClick(selectedModule.id);
      setPopupText('Module updated');
      setShowPopup(true);
      setEditState();
      setShowLoading(false);
      return;
    }
  };

  return (
    <div className='modules-grid'>
      <div className='modules-loadingspinner'>
        {showLoading ? <LoadingSpinner /> : <></>}
      </div>
      {showNewModuleSetting ? (
        <NewModuleSetting
          moduleSettings={moduleSettings}
          setModuleSettings={setModuleSettings}
          setShowNewModuleSetting={setShowNewModuleSetting}
        />
      ) : (
        <></>
      )}
      <div className='modules-info-popup'>
        {showPopup ? (
          <InfoPopup popupText={popupText} triggerState={setShowPopup} />
        ) : (
          <></>
        )}
      </div>
      <div className='modules-sidepanel'>
        <SidePanel menuItems={modules} onItemClick={handleMenuItemClick} />
      </div>
      <div className='modules-moduleinfo' id='moduleInfo'>
        <div className='modules-economics-productid-name-container'>
          <div className='modules-economics-productid-container'>
            <InputField
              label='Economics product id'
              type='text'
              id='economicsProductId'
              className='inputfield'
              onChange={handleChange}
              value={selectedModule.economicsProductId}
              disabled={editModeDisabled}
              name='economicsProductId'
            />
          </div>
          <div className='modules-name-container topmargin-input'>
            <InputField
              label='Module name'
              type='text'
              id='moduleName'
              className='inputfield modules-input-name'
              onChange={handleChange}
              value={selectedModule.name}
              disabled={editModeDisabled}
              name='name'
            />
          </div>
        </div>
        <div className='modules-description-container topmargin-input'>
          <label htmlFor='moduleDescription'>Description</label>
          <textarea
            id='moduleDescription'
            className='inputfield modules-input-description'
            onChange={(e) => handleChange(e)}
            value={selectedModule.description}
            disabled={editModeDisabled}
            name='description'
          />
        </div>
        <div className='modules-prices'>
          <div className='topmargin-input'>
            <InputField
              type='number'
              label='Price'
              id='modulePrice'
              name='price'
              className='inputfield'
              onChange={handleChange}
              value={selectedModule.price}
              disabled={editModeDisabled}
            />
          </div>
          <div className='topmargin-input'>
            <InputField
              type='number'
              label='Start up price'
              id='moduleFirstTimePrice'
              className='inputfield'
              onChange={handleChange}
              value={selectedModule.firsttimePrice}
              name='firsttimePrice'
              disabled={editModeDisabled}
            />
          </div>
        </div>
        <div className='modules-dropdowns'>
          <div className='modules-dropdown-modulegroup topmargin-input'>
            <label htmlFor='moduleGroup'>Group</label>
            <ModuleGroupDropdown
              setShowLoading={setShowLoading}
              onChange={setSelectedModuleGroup}
              value={selectedModuleGroup}
              id='moduleGroup'
              editState={editState}
              disabled={editModeDisabled}
            />
          </div>
          <div className='modules-dropdown-paymentfrequency topmargin-input'>
            <label htmlFor='modulePaymentFrequency'>Payment frequency</label>
            <PaymentFrequencyDropdown
              setShowLoading={setShowLoading}
              onChange={setSelectedPaymentFrequency}
              value={selectedPaymentFrequency}
              id='modulePaymentFrequency'
              editState={editState}
              disabled={editModeDisabled}
            />
          </div>
        </div>

        <div className='modules-economics-dropdowns'>
          <div className='modules-economics-productgroup-container topmargin-input'>
            <label>Economics product group</label>
            <EconomicsProductGroupDropdown
              setShowLoading={setShowLoading}
              onChange={setSelectedEconomicsProductGroup}
              value={selectedEconomicsProductGroup}
              id='economicsProductGroup'
              editState={editState}
              disabled={editModeDisabled}
            />
          </div>
          <div className='modules-economics-unit-container topmargin-input'>
            <label htmlFor='moduleEconomicsUnitDropdown'>Unit</label>
            <EconomicsUnitDropdown
              setShowLoading={setShowLoading}
              onChange={setSelectedEconomicsUnit}
              value={selectedEconomicsUnit}
              id='moduleEconomicsUnitDropdown'
              editState={editState}
              disabled={editModeDisabled}
            />
          </div>
        </div>

        <div className='checkbox-container topmargin-input'>
          <input
            className='checkbox'
            type='checkbox'
            name='samModule'
            value={selectedModule.samModule}
            checked={selectedModule.samModule}
            onChange={(e) =>
              setSelectedModule((prevModule) => ({
                ...prevModule,
                [e.target.name]: e.target.checked,
              }))
            }
            disabled={editModeDisabled}
          />
          <label htmlFor='moduleSAMModule'>SAM Module</label>
        </div>
        <div
          className='modules-settings-required-container'
          id='modules-settings-required-container'
        >
          <div className='modules-settingtable-container'>
            <div className='modules-settingtable-header'>
              <AddButton
                disabled={addSettingButtonDisabled}
                onClick={(e) => setShowNewModuleSetting(true)}
              />
              <label>Settings</label>
            </div>
            <div className='modules-settings-container'>
              <ModuleSettingTable
                moduleSettings={moduleSettings}
                setModuleSettings={setModuleSettings}
                disabled={deleteSettingButtonDisabled}
              />
            </div>
          </div>
          <div className='modules-requiredtable-container'>
            <label>Required Modules</label>
            <div className='modules-requiredmodules-container'>
              <CheckboxTable
                items={modules}
                selectedModule={selectedModule}
                enabledItems={enabledRequiredModules}
                setSelectedItems={setSelectedRequiredModules}
                type='reqModules'
              />
            </div>
          </div>
        </div>
      </div>
      <div className='modules-buttons buttons-style'>
        <SaveButton
          disabled={saveButtonDisabled}
          onClick={handleSaveButtonClick}
          id='subscription-save-button'
        />
        <EditButton
          disabled={editButtonDisabled}
          onClick={handleEditButtonClick}
          id='subscription-edit-button'
        />
        <UndoButton
          disabled={undoButtonDisabled}
          onClick={handleUndoButtonClick}
          id='subscription-undo-button'
        />
        <DeleteButton id='subscription-delete-button' />
      </div>
    </div>
  );
};

export default ModulePage;
