import React from 'react';
import LoginForm from '../../Components/LoginForm/LoginForm';
import './LoginPage.css';

const LoginPage = ({ authenticated, setAuthenticated, user, setUser }) => {
  return (
    <div className='login-root'>
      <LoginForm
        user={user}
        setUser={setUser}
        authenticated={authenticated}
        setAuthenticated={setAuthenticated}
      />
    </div>
  );
};

export default LoginPage;
