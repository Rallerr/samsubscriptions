import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router';
import _ from 'lodash';
import { ValidateToken } from '../../Services/AuthService';
import SidePanel from '../../Components/SidePanel/SidePanel';
import { GetBillingPeriods } from '../../Services/BillingPeriodService';
import {
  CreateEnterprise,
  GetEnterpriseById,
  GetEnterpriseInfoByIdFromHubspot,
  GetEnterprisesWithSubscription,
} from '../../Services/EnterpriseService';
import {
  CreateActivePriceMultiplier,
  CreateInactivePriceMultiplier,
  DeleteActivePriceMultiplier,
  GetActivePriceMultipliersBySubscription,
  GetPriceMultiplier,
} from '../../Services/PriceMultiplierService';
import {
  GetSubscriptionTypeById,
  GetSubscriptionTypes,
} from '../../Services/SubscriptionTypeService';
import './SubscriptionPage.css';
import {
  CreateActiveModule,
  CreateInactiveModule,
  DeleteActiveModule,
  GetModules,
} from '../../Services/ModuleService';
import {
  CreateModuleSettingValue,
  DeleteModuleSettingValue,
  GetActiveModuleSettingValuesByActiveModuleId,
  GetModuleSettingsByModule,
} from '../../Services/ModuleSettingService';
import { GetActiveModulesBySubscription } from '../../Services/ActiveModuleService';
import {
  CreateSubscription,
  GetSubscriptionByEnterprise,
  UpdateSubscription,
} from '../../Services/SubscriptionService';
import {
  CreateActiveConcurrentUsers,
  GetActiveConcurrentUsersBySubscriptionId,
} from '../../Services/ConcurrentUsersService';
import { GetCurrencies, GetCurrencyById } from '../../Services/CurrencyService';
import SaveButton from '../../Components/SaveButton/SaveButton';
import EditButton from '../../Components/EditButton/EditButton';
import DeleteButton from '../../Components/DeleteButton/DeleteButton';
import UndoButton from '../../Components/UndoButton/UndoButton';
import InfoPopup from '../../Components/InfoPopup/InfoPopup';
import SelectEnterprise from '../../Components/SelectEnterprise/SelectEnterprise';
import ModuleTable from '../../Components/ModuleTable/ModuleTable';
import { GetNotPaidInactiveModules } from '../../Services/InactiveModuleService';
import GenerateInvoiceButton from '../../Components/GenererateInvoiceButton/GenerateInvoiceButton';
import EconomicsCustomerGroupDropdown from '../../Components/EconomicsCustomerGroupDropdown/EconomicsCustomerGroupDropdown';
import EconomicsPaymentTermsDropdown from '../../Components/EconomicsPaymentTermsDropdown/EconomicsPaymentTermsDropdown';
import EconomicsVatZoneDropdown from '../../Components/EconomicsVatZoneDropdown/EconomicsVatZoneDropdown';
import { GetEconomicsPaymentTerms } from '../../Services/EconomicsServices/EconomicsPaymentTermsService';
import { GetEconomicsCustomerGroups } from '../../Services/EconomicsServices/EconomicsCustomerGroupService';
import { GetEconomicsVatZones } from '../../Services/EconomicsServices/EconomicsVatZoneService';
import {
  CreateEconomicsCustomer,
  GetEconomicsCustomerById,
  UpdateEconomicsCustomer,
} from '../../Services/EconomicsServices/EconomicsCustomerService';
import SubscriptionTypeDropdown from '../../Components/SubscriptionTypeDropdown/SubscriptionTypeDropdown';
import InputField from '../../Components/InputField/InputField';
import BillingPeriodDropdown from '../../Components/BillingPeriodDropdown/BillingPeriodDropdown';
import PriceMultiplierTable from '../../Components/PriceMultiplierTable/PriceMultiplierTable';
import CurrencyDropdown from '../../Components/CurrencyDropdown/CurrencyDropdown';
import { GetEconomicsLayouts } from '../../Services/EconomicsServices/EconomicsLayoutService';
import EconomicsLayoutDropdown from '../../Components/EconomicsLayoutDropdown/EconomicsLayoutDropdown';
import GenerateMultipleInvoicesButton from '../../Components/GenerateMultipleInvoicesButton/GenerateMultipleInvoicesButton';
import { GenerateEconomicsDraftInvoice } from '../../Services/EconomicsServices/EconomicsDraftInvoice';
import LoadingSpinner from '../../Components/LoadingSpinner/LoadingSpinner';
import { UpdateConcurrentUsers } from '../../Utils/SubscriptionUtils';
import SubscriptionPrices from '../../Components/SubscriptionPrices/SubscriptionPrices';

const SubscriptionPage = ({
  authenticated,
  setAuthenticated,
  editState,
  setEditState,
}) => {
  const [enterprises, setEnterprises] = useState([]);
  const [subscription, setSubscription] = useState({ discount: 0 });
  const [activeMenuItem, setActiveMenuItem] = useState(' ');
  const [modules, setModules] = useState([]);
  const [moduleSettingValues, setModuleSettingValues] = useState([]);
  const [selectedEnterprise, setSelectedEnterprise] = useState({
    id: '',
    name: '',
  });
  const [activeModules, setActiveModules] = useState([]);
  const [notPaidInactiveModules, setNotPaidInactiveModules] = useState([]);
  const [subscriptionTypes, setSubscriptionTypes] = useState([]);
  const [billingPeriods, setBillingPeriods] = useState([]);
  const [priceMultipliers, setPriceMultipliers] = useState([]);
  const [activePriceMultipliers, setActivePriceMulipliers] = useState([]);
  const [selectedActivePriceMultipliers, setSelectedActivePriceMultipliers] =
    useState([]);
  const [concurrentUsers, setConcurrentUsers] = useState({ camo: 0, mro: 0 });
  const [currencies, setCurrencies] = useState([]);
  const [subscriptionLoaded, setSubscriptionLoaded] = useState(false);
  const [selectedModules, setSelectedModules] = useState([]);
  const [undoButtonDisabled, setUndoButtonDisabled] = useState(true);
  const [editButtonDisabled, setEditButtonDisabled] = useState(true);
  const [saveButtonDisabled, setSaveButtonDisabled] = useState(true);
  const [selectedSubscriptionType, setSelectedSubscriptionType] = useState({
    id: ' ',
    name: ' ',
  });
  const [selectedBillingPeriod, setSelectedBillingPeriod] = useState();
  const [selectedCurrency, setSelectedCurrency] = useState();
  const [showSelectEnterprise, setShowSelectEnterprise] = useState(false);
  const [popupText, setPopupText] = useState('');
  const [showPopup, setShowPopup] = useState(false);
  const [economicsPaymentTerms, setEconomicsPaymentTerms] = useState([]);
  const [selectedEconomicsPaymentTerm, setSelectedEconomicsPaymentTerm] =
    useState();
  const [economicsCustomerGroups, setEconomicsCustomerGroups] = useState([]);
  const [selectedEconomicsCustomerGroup, setSelectedEconomicsCustomerGroup] =
    useState();
  const [economicsVatZones, setEconomicsVatZones] = useState([]);
  const [selectedEconomicsVatZone, setSelectedEconomicsVatZone] = useState();
  const [economicsLayouts, setEconomicsLayouts] = useState([]);
  const [selectedEconomicsLayout, setSelectedEconomicsLayout] = useState();
  const [economicsVat, setEconomicsVat] = useState('');
  const [showLoading, setShowLoading] = useState(false);
  const [editModeDisabled, setEditModeDisabled] = useState(true);

  //Check if token is valid
  useEffect(() => {
    const auth = async () => {
      const isAuth = await ValidateToken();
      sessionStorage.setItem('authenticated', isAuth);
      setAuthenticated(isAuth);
    };

    auth();
  }, [setAuthenticated]);

  //Load enterprises
  useEffect(() => {
    const getData = async () => {
      const data = await GetEnterprisesWithSubscription();
      setEnterprises(data);
    };
    getData();
  }, []);

  //Get subscription types
  useEffect(() => {
    const getData = async () => {
      const data = await GetSubscriptionTypes();
      setSubscriptionTypes(data);
      try {
        setSelectedSubscriptionType({ id: data[0].id, name: data[0].type });
      } catch (err) {
        console.log(err);
      }
    };
    getData();
  }, []);

  //Get billing periods
  useEffect(() => {
    const getData = async () => {
      const data = await GetBillingPeriods();
      setBillingPeriods(data);

      try {
        setSelectedBillingPeriod(data[0].id);
      } catch (err) {
        console.log(err);
      }
    };
    getData();
  }, []);

  //Get price multipliers
  useEffect(() => {
    const getData = async () => {
      const data = await GetPriceMultiplier();
      setPriceMultipliers(data);
    };
    getData();
  }, []);

  //Get modules
  useEffect(() => {
    const getData = async () => {
      const data = await GetModules();

      if (data !== undefined && data.length > 0) {
        data.map((item) => {
          item.isChecked = false;
          return '';
        });
      }
      setModules(data);
    };
    getData();
  }, []);

  //Get currencies
  useEffect(() => {
    const getData = async () => {
      const data = await GetCurrencies();
      setCurrencies(data);
      try {
        setSelectedCurrency(data[0].id);
      } catch (err) {
        console.log(err);
      }
    };
    getData();
  }, []);

  //Get economics payment terms, customer groups and vat zones
  useEffect(() => {
    const getData = async () => {
      const paymentTerms = await GetEconomicsPaymentTerms();
      setEconomicsPaymentTerms(paymentTerms);
      if (paymentTerms !== undefined && paymentTerms.length > 0) {
        setSelectedEconomicsPaymentTerm(paymentTerms[0].paymentTermsNumber);
      }

      const customerGroups = await GetEconomicsCustomerGroups();
      if (
        customerGroups !== undefined &&
        customerGroups.collection.length > 0
      ) {
        setEconomicsCustomerGroups(customerGroups.collection);
        setSelectedEconomicsCustomerGroup(
          customerGroups.collection[0].customerGroupNumber
        );
      }

      const vatZones = await GetEconomicsVatZones();
      setEconomicsVatZones(vatZones);
      if (vatZones !== undefined && vatZones.length > 0) {
        setSelectedEconomicsVatZone(vatZones[0].vatZoneNumber);
      }

      const layouts = await GetEconomicsLayouts();
      setEconomicsLayouts(layouts);
      if (layouts !== undefined && layouts.length > 0) {
        setSelectedEconomicsLayout(layouts[0].layoutNumber);
      }
    };

    getData();
  }, []);

  useEffect(() => {
    if (editState === '' || editState === undefined) {
      disableEditMode();
      setEditButtonDisabled(true);
    }

    if (editState === 'NEW_SUBSCRIPTION') {
      resetSubscription();
      setShowSelectEnterprise(true);
      enableEditMode();
    }
  }, [editState]);

  if (sessionStorage.getItem('authenticated') === 'false') {
    sessionStorage.removeItem('user');
    return <Redirect to='/login' />;
  }

  //Reset all inputs to original state.
  const resetSubscription = () => {
    setSelectedEnterprise({ id: '', name: '' });
    setSelectedSubscriptionType({
      id: subscriptionTypes[0].id,
      name: subscriptionTypes[0].type,
    });
    setSubscription({ discount: 0 });
    setNotPaidInactiveModules([]);
    setActiveMenuItem(' ');
    setModuleSettingValues([]);
    setActiveModules([]);
    setActivePriceMulipliers([]);
    setSelectedActivePriceMultipliers([]);
    setConcurrentUsers({ camo: 0, mro: 0 });
  };

  //Click on side menu item
  const handleOnItemClick = async (enterpriseId) => {
    setShowLoading(true);
    var clickedItem = document.getElementById(enterpriseId);
    clickedItem.classList.add('active');

    var prevClickedItem = document.getElementById(activeMenuItem);
    if (prevClickedItem !== null && prevClickedItem !== clickedItem) {
      prevClickedItem.classList.remove('active');
    }

    //Get subscription info
    const enterpriseInfo = await GetEnterpriseById(enterpriseId);
    const subscriptionData = await GetSubscriptionByEnterprise(
      enterpriseInfo.id
    );

    const subType = await GetSubscriptionTypeById(
      subscriptionData.subscriptionTypeId
    );

    //Get concurrent users
    const ccUsers = await GetActiveConcurrentUsersBySubscriptionId(
      subscriptionData.id
    );

    //Get activemodules
    const activeModuleList = await GetActiveModulesBySubscription(
      subscriptionData.id
    );

    //Get not paid inactive modules
    /*const inactiveModuleList = await GetNotPaidInactiveModules(
      subscriptionData.id
    );
    setNotPaidInactiveModules(inactiveModuleList);*/

    //Get active pricemultipliers
    const activeMultipliers = await GetActivePriceMultipliersBySubscription(
      subscriptionData.id
    );
    const economicsCustomerInfo = await GetEconomicsCustomerById(
      enterpriseInfo.economicsCustomerNumber
    );

    //Set subscription info
    setSelectedEnterprise(enterpriseInfo);
    setSubscription(subscriptionData);
    setSelectedSubscriptionType({
      id: subType.id,
      name: subType.type,
    });
    setSelectedBillingPeriod(subscriptionData.billingPeriodId);
    setSelectedCurrency(subscriptionData.currencyId);
    setConcurrentUsers(ccUsers);
    setActiveModules(activeModuleList);
    setActivePriceMulipliers(activeMultipliers);

    setSelectedEconomicsCustomerGroup(
      economicsCustomerInfo.customerGroup.customerGroupNumber
    );
    setSelectedEconomicsPaymentTerm(
      economicsCustomerInfo.paymentTerms.paymentTermsNumber
    );
    setSelectedEconomicsVatZone(economicsCustomerInfo.vatZone.vatZoneNumber);
    setSelectedEconomicsLayout(economicsCustomerInfo.layout.layoutNumber);
    setEconomicsVat(economicsCustomerInfo.corporateIdentificationNumber);

    setActiveMenuItem(enterpriseId);
    setSubscriptionLoaded(true);
    setEditButtonDisabled(false);
    setShowLoading(false);
  };

  //Enable all inputs
  const enableEditMode = () => {
    setSaveButtonDisabled(false);
    setUndoButtonDisabled(false);
    setEditButtonDisabled(true);
    setEditModeDisabled(false);
  };

  const handleEditButtonClick = (e) => {
    e.preventDefault();

    setEditState('EDIT_SUBSCRIPTION');

    enableEditMode();
  };

  //Disable all inputs
  const disableEditMode = () => {
    setSaveButtonDisabled(true);
    setEditButtonDisabled(false);
    setUndoButtonDisabled(true);
    setEditModeDisabled(true);
  };

  const handleUndoButtonClick = (e) => {
    e.preventDefault();

    disableEditMode();

    if (editState === 'EDIT_SUBSCRIPTION') {
      handleOnItemClick(selectedEnterprise.id);
    }

    if (
      selectedEnterprise === undefined ||
      selectedEnterprise.id === '' ||
      SelectEnterprise.name === ''
    ) {
      setEditButtonDisabled(true);
    }
    if (editState === 'NEW_SUBSCRIPTION') {
      resetSubscription();
      setEditState('');
    }
  };

  const updateCCUsers = async () => {
    UpdateConcurrentUsers(concurrentUsers, subscription.id);
  };

  const handleSaveButtonClick = async () => {
    if (editState === 'EDIT_SUBSCRIPTION') {
      try {
        // update subscription
        const updatedSubscription = {
          id: subscription.id,
          enterpriseId: subscription.enterpriseId,
          billingPeriodId: selectedBillingPeriod,
          currencyId: selectedCurrency,
          aircraftCountId: subscription.aircraftCountId,
          SubscriptionTypeId: selectedSubscriptionType.id,
          discount: parseFloat(subscription.discount),
        };

        if (_.isEqual(subscription, updatedSubscription) === false) {
          console.log('not equals');
          await UpdateSubscription(updatedSubscription);
        }

        // update concurrent users
        const oldCCUsers = await GetActiveConcurrentUsersBySubscriptionId(
          subscription.id
        );

        if (_.isEqual(oldCCUsers, concurrentUsers) === false) {
          await updateCCUsers();
        }

        //Set unselected multipliers to inactive
        const oldMultipliers = await GetActivePriceMultipliersBySubscription(
          subscription.id
        );

        oldMultipliers.map(async (oldMultiplier) => {
          //check if old multiplier is selected
          const multiplier = selectedActivePriceMultipliers.find(
            (element) => element.id === oldMultiplier.priceMultiplierId
          );

          //If old multiplier is not selected
          if (multiplier === undefined) {
            const newInactivePriceMultiplier = {
              subscriptionId: oldMultiplier.subscriptionId,
              priceMultiplierId: oldMultiplier.priceMultiplierId,
              start:
                oldMultiplier.lastPaid === null
                  ? oldMultiplier.start
                  : oldMultiplier.lastPaid,
              expiration: oldMultiplier.nextPayment,
              paid: oldMultiplier.lastPaid === null ? false : true,
            };

            await CreateInactivePriceMultiplier(newInactivePriceMultiplier);

            await DeleteActivePriceMultiplier(oldMultiplier.id);
          }
        });

        //Set selected multipliers to active
        if (selectedActivePriceMultipliers.length > 0) {
          const today = new Date();
          const lastDayOfMonth = new Date(
            today.getFullYear(),
            today.getMonth() + 1,
            0
          );

          selectedActivePriceMultipliers.map(async (selectedMultiplier) => {
            const oldMultiplier = oldMultipliers.find(
              (element) => element.priceMultiplierId === selectedMultiplier.id
            );

            if (oldMultiplier === undefined) {
              const newActiveMultiplier = {
                subscriptionId: subscription.id,
                priceMultiplierId: selectedMultiplier.id,
                start: new Date(),
                nextPayment: lastDayOfMonth,
              };

              await CreateActivePriceMultiplier(newActiveMultiplier);
            }
          });
        }

        //Set unselected modules to inactive
        activeModules.map(async (activeModule) => {
          const module = selectedModules.find(
            (m) => m.id === activeModule.moduleId
          );

          if (module === undefined) {
            const newInactiveModule = {
              enterpriseId: activeModule.enterpriseId,
              moduleId: activeModule.moduleId,
              start:
                activeModule.lastPaid === null
                  ? activeModule.start
                  : activeModule.lastPaid,
              expiration: activeModule.nextPayment,
              firsttimePayment: activeModule.firsttimePayment,
              paid: activeModule.lastPaid === null ? false : true,
              subscriptionId: subscription.id,
            };

            const createdInactiveModule = await CreateInactiveModule(
              newInactiveModule
            );

            //Set module setting value to inactive
            const settingVals =
              await GetActiveModuleSettingValuesByActiveModuleId(
                activeModule.id
              );

            if (settingVals !== undefined && settingVals.length > 0) {
              settingVals.map(async (value) => {
                const inactiveSettingValue = {
                  moduleSettingId: value.moduleSettingId,
                  value: value.value,
                  inactiveModuleId: createdInactiveModule.id,
                };

                await CreateModuleSettingValue(inactiveSettingValue);

                await DeleteModuleSettingValue(value.id);
              });
            }

            await DeleteActiveModule(activeModule.id);
          }
        });

        //Set selected modules to active
        if (selectedModules.length > 0) {
          const today = new Date();
          const lastDayOfMonth = new Date(
            today.getFullYear(),
            today.getMonth() + 1,
            0
          );
          selectedModules.map(async (selectedModule) => {
            const activeModule = activeModules.find(
              (element) => element.moduleId === selectedModule.id
            );

            if (activeModule === undefined) {
              const newActiveModule = {
                enterpriseId: selectedEnterprise.id,
                moduleId: selectedModule.id,
                start: new Date(),
                firsttimePayment: true,
                subscriptionId: subscription.id,
                NextPayment: lastDayOfMonth,
              };

              const createdActiveModule = await CreateActiveModule(
                newActiveModule
              );

              //Create module setting value if module has settings

              const settings = await GetModuleSettingsByModule(
                createdActiveModule.moduleId
              );

              if (settings !== undefined && settings.length > 0) {
                settings.map(async (item) => {
                  const settingValue = moduleSettingValues.find(
                    (value) => value.moduleSettingId === item.id
                  );

                  const newSettingvalue = {
                    moduleSettingId: item.id,
                    value: settingValue.value,
                    activeModuleId: createdActiveModule.id,
                  };

                  await CreateModuleSettingValue(newSettingvalue);
                });
              }
            }
          });
        }

        const currentEconomicsCustomer = await GetEconomicsCustomerById(
          selectedEnterprise.economicsCustomerNumber
        );

        if (
          currentEconomicsCustomer.customerGroup.customerGroupNumber !==
            selectedEconomicsCustomerGroup ||
          currentEconomicsCustomer.paymentTerms.paymentTermsNumber !==
            selectedEconomicsPaymentTerm ||
          currentEconomicsCustomer.vatZone.vatZoneNumber !==
            selectedEconomicsVatZone ||
          currentEconomicsCustomer.layout.layoutNumber !==
            selectedEconomicsLayout ||
          currentEconomicsCustomer.corporateIdentificationNumber !==
            economicsVat
        ) {
          currentEconomicsCustomer.customerGroup.customerGroupNumber =
            selectedEconomicsCustomerGroup;

          currentEconomicsCustomer.paymentTerms.paymentTermsNumber =
            selectedEconomicsPaymentTerm;

          currentEconomicsCustomer.vatZone.vatZoneNumber =
            selectedEconomicsVatZone;

          currentEconomicsCustomer.layout.layoutNumber =
            selectedEconomicsLayout;

          currentEconomicsCustomer.corporateIdentificationNumber = economicsVat;

          await UpdateEconomicsCustomer(currentEconomicsCustomer);
        }

        setPopupText('Subscription updated');
        setShowPopup(true);
        handleOnItemClick(selectedEnterprise.id);
        disableEditMode();
        setEditState('');
        return;
      } catch (err) {
        console.log(err);
        setPopupText('Updating subscription failed');
        setShowPopup(true);
      }
    }

    if (editState === 'NEW_SUBSCRIPTION') {
      try {
        //Check if any modules is selected
        if (selectedModules.length === 0) {
          alert('No modules selected');
          return;
        }

        const today = new Date();
        const lastDayOfMonth = new Date(
          today.getFullYear(),
          today.getMonth() + 1,
          0
        );

        const hubspotEnterpriseInfo = await GetEnterpriseInfoByIdFromHubspot(
          selectedEnterprise.id
        );

        const currencyInfo = await GetCurrencyById(selectedCurrency);
        console.log(currencyInfo);

        const newEconomicsCustomer = {
          address: hubspotEnterpriseInfo.address,
          city: hubspotEnterpriseInfo.city,
          country: hubspotEnterpriseInfo.country,
          currency: currencyInfo.currencyName,
          customerGroup: {
            customerGroupNumber: selectedEconomicsCustomerGroup,
          },
          //email: hubspotEnterpriseInfo.invoiceEmail,
          paymentTerms: {
            paymentTermsNumber: selectedEconomicsPaymentTerm,
          },
          mobilePhone: hubspotEnterpriseInfo.phone,
          vatZone: {
            vatZoneNumber: selectedEconomicsVatZone,
          },
          website: hubspotEnterpriseInfo.website,
          name: hubspotEnterpriseInfo.name,
          zip: hubspotEnterpriseInfo.zip,
          layout: {
            layoutNumber: selectedEconomicsLayout,
          },
          corporateIdentificationNumber: economicsVat,
        };

        const createdEconomicsCustomer = await CreateEconomicsCustomer(
          newEconomicsCustomer
        );

        console.log(createdEconomicsCustomer);

        //Create enterprise
        const newEnterprise = {
          name: selectedEnterprise.name,
          enterpriseNo: selectedEnterprise.id,
          economicsCustomerNumber: createdEconomicsCustomer.customerNumber,
        };

        const createdEnterprise = await CreateEnterprise(newEnterprise);

        //Create new subscription
        const newSubscription = {
          enterpriseId: createdEnterprise.id,
          billingPeriodId: selectedBillingPeriod,
          currencyId: selectedCurrency,
          subscriptionTypeId: selectedSubscriptionType.id,
          discount: subscription.discount,
        };

        const createdSubscription = await CreateSubscription(newSubscription);

        //Create concurrent users
        const newActiveUsers = {
          enterpriseId: createdEnterprise.id,
          camo: concurrentUsers.camo,
          mro: concurrentUsers.mro,
          start: new Date(),
          subscriptionId: createdSubscription.id,
          nextPayment: lastDayOfMonth,
        };
        await CreateActiveConcurrentUsers(newActiveUsers);

        //Set selected modules to active
        selectedModules.map(async (item) => {
          const newActiveModule = {
            enterpriseId: createdEnterprise.id,
            moduleId: item.id,
            start: new Date(),
            firsttimePayment: true,
            subscriptionId: createdSubscription.id,
            nextPayment: lastDayOfMonth,
          };

          const createdActiveModule = await CreateActiveModule(newActiveModule);

          //Create setting values if module has settings
          const settings = await GetModuleSettingsByModule(
            createdActiveModule.moduleId
          );

          if (settings !== undefined && settings.length > 0) {
            settings.map(async (item) => {
              const settingValue = moduleSettingValues.find(
                (value) => value.moduleSettingId === item.id
              );

              const newSettingvalue = {
                moduleSettingId: item.id,
                value: settingValue.value,
                activeModuleId: createdActiveModule.id,
              };

              await CreateModuleSettingValue(newSettingvalue);
            });
          }
        });

        if (selectedActivePriceMultipliers.length > 0) {
          selectedActivePriceMultipliers.map(async (selectedMultiplier) => {
            const newActiveMultiplier = {
              subscriptionId: createdSubscription.id,
              priceMultiplierId: selectedMultiplier.id,
              start: new Date(),
              NextPayment: lastDayOfMonth,
            };

            await CreateActivePriceMultiplier(newActiveMultiplier);
          });
        }

        setPopupText('Subscription created');
        setShowPopup(true);
        resetSubscription();
        disableEditMode();
        setEditState('');
        window.location.reload();
      } catch (err) {
        console.log(err);
        setPopupText('Creating subscription failed');
        setShowPopup(true);
      }
    }
  };

  const handleCCUsersChange = (e) => {
    setConcurrentUsers((prevUsers) => ({
      ...prevUsers,

      [e.target.name]: Number.isNaN(e.target.value) ? 0 : e.target.value,
    }));
  };

  const handleDiscountChange = (e) => {
    console.log(e);
    setSubscription((prevDiscount) => ({
      ...prevDiscount,
      [e.target.name]: Number.isNaN(e.target.value) ? 0 : e.target.value,
    }));
  };

  const handleGenerateInvoice = async () => {
    if (selectedEnterprise.id === '' || selectedEnterprise.id === undefined) {
      alert('No subscription selected');
      return;
    }
    await GenerateEconomicsDraftInvoice(selectedEnterprise.id);
  };

  const handleGenerateMultipleInvoices = async () => {
    const enterprises = await GetEnterprisesWithSubscription();
    if (enterprises !== undefined && enterprises.length > 0) {
      enterprises.map(async (item, index) => {
        await GenerateEconomicsDraftInvoice(item.id);
        return '';
      });
    }
  };

  return (
    <div className='subscription-grid-container'>
      {showLoading ? (
        <div className='subscription-loadingspinner'>
          <LoadingSpinner height='50px' width='50px' />
        </div>
      ) : (
        <></>
      )}
      {showSelectEnterprise ? (
        <SelectEnterprise
          selectedEnterprise={selectedEnterprise}
          setSelectedEnterprise={setSelectedEnterprise}
          setShowSelectEnterprise={setShowSelectEnterprise}
          setEditState={setEditState}
        />
      ) : (
        <></>
      )}
      <div className='subscription-info-popup'>
        {showPopup ? (
          <InfoPopup popupText={popupText} triggerState={setShowPopup} />
        ) : (
          <></>
        )}
      </div>
      <div className='subscription-side-menu'>
        <SidePanel menuItems={enterprises} onItemClick={handleOnItemClick} />
      </div>
      <div className='subscription-settings' id='subscription-settings'>
        <div className='subscriptiontype-container'>
          <label htmlFor='subscriptionTypeDropdown'>Subscription Type</label>
          <SubscriptionTypeDropdown
            disabled={editModeDisabled}
            items={subscriptionTypes}
            id='subscriptionTypeDropdown'
            value={selectedSubscriptionType}
            onChange={setSelectedSubscriptionType}
          />
        </div>
        <div className='concurrentusers-container'>
          <div className='camo-container'>
            <InputField
              label='CAMO'
              name='camo'
              id='camousers'
              type='number'
              onChange={handleCCUsersChange}
              className='inputfield'
              disabled={editModeDisabled}
              value={concurrentUsers.camo}
            />
          </div>
          <div className='mro-container'>
            <InputField
              label='MRO'
              name='mro'
              id='mrousers'
              type='number'
              onChange={handleCCUsersChange}
              className='inputfield'
              disabled={editModeDisabled}
              value={concurrentUsers.mro}
            />
          </div>
          <div className='aircraft-container'>
            <label htmlFor='aircraftusers'>AC</label>
            <input
              className='inputfield'
              type='number'
              id='aircraftusers'
              readOnly
              disabled={editModeDisabled}
            />
          </div>
        </div>
        <div className='billingperiod-container'>
          <label htmlFor='billingPeriodDropdown'>Billing period</label>
          <BillingPeriodDropdown
            id='billingPeriodDropdown'
            items={billingPeriods}
            onChange={setSelectedBillingPeriod}
            value={selectedBillingPeriod}
            disabled={editModeDisabled}
          />
        </div>
        <div className='pricemultiplier-container' id='pricemultipliers'>
          <label>Price multipliers</label>
          <PriceMultiplierTable
            editState={editState}
            setSelectedActivePriceMultipliers={
              setSelectedActivePriceMultipliers
            }
            activePriceMultipliers={activePriceMultipliers}
            priceMultipliers={priceMultipliers}
            disabled={editModeDisabled}
          />
        </div>
        <div className='currency-discount-container'>
          <div className='currency-container'>
            <label htmlFor='subscriptionCurrencyDropdown'>Currency</label>
            <CurrencyDropdown
              items={currencies}
              value={selectedCurrency}
              onChange={setSelectedCurrency}
              id='subscriptionCurrencyDropdown'
              disabled={editModeDisabled}
            />
          </div>
          <div className='discount-container'>
            <InputField
              label='Discount'
              id='subscriptionDiscount'
              type='number'
              className='inputfield'
              name='discount'
              onChange={handleDiscountChange}
              value={subscription.discount}
              disabled={editModeDisabled}
            />
          </div>
        </div>
      </div>
      <div
        className='subscription-active-modules'
        id='subscription-active-modules'
      >
        <div className='subscription-modules-grid'>
          <div className='subscription-modules-container'>
            <ModuleTable
              modules={modules}
              setModules={setModules}
              subscriptionType={selectedSubscriptionType.name}
              editState={editState}
              setSelectedModules={setSelectedModules}
              concurrentUsers={concurrentUsers}
              activeModules={activeModules}
              selectedActivePriceMultipliers={selectedActivePriceMultipliers}
              subscriptionLoaded={subscriptionLoaded}
              moduleSettingValues={moduleSettingValues}
              setModuleSettingValues={setModuleSettingValues}
              notPaidInactiveModules={notPaidInactiveModules}
              disabled={editModeDisabled}
            />
          </div>
          <div className='prices-container'>
            <SubscriptionPrices
              selectedModules={selectedModules}
              activeModules={activeModules}
              discount={subscription.discount}
              subscriptionLoaded={subscriptionLoaded}
            />
          </div>
        </div>
      </div>
      <div className='subscription-modules' id='subscription-modules'>
        <div className='subscription-customergroup-container'>
          <label htmlFor='subscriptionEconomicsCustomerGroups'>
            Customer group
          </label>
          <EconomicsCustomerGroupDropdown
            id='subscriptionEconomicsCustomerGroups'
            onChange={setSelectedEconomicsCustomerGroup}
            value={selectedEconomicsCustomerGroup}
            items={economicsCustomerGroups}
            disabled={editModeDisabled}
          />
        </div>
        <div className='subscription-paymentterms-container'>
          <label htmlFor='subscriptionEconomicsPaymentTerms'>
            Payment terms
          </label>
          <EconomicsPaymentTermsDropdown
            id='subscriptionEconomicsPaymentTerms'
            onChange={setSelectedEconomicsPaymentTerm}
            value={selectedEconomicsPaymentTerm}
            items={economicsPaymentTerms}
            disabled={editModeDisabled}
          />
        </div>
        <div className='subscription-vatzones-container'>
          <label htmlFor='subscriptionEconomicsVatZones'>Vat zone</label>
          <EconomicsVatZoneDropdown
            id='subscriptonEconomicsVatZones'
            onChange={setSelectedEconomicsVatZone}
            value={selectedEconomicsVatZone}
            items={economicsVatZones}
            disabled={editModeDisabled}
          />
        </div>
        <div className='subscription-layouts-container'>
          <label htmlFor='subscriptionEconomicsLayout'>Layout</label>
          <EconomicsLayoutDropdown
            id='subscriptionEconomicsLayout'
            onChange={setSelectedEconomicsLayout}
            value={selectedEconomicsLayout}
            items={economicsLayouts}
            disabled={editModeDisabled}
          />
        </div>
        <div className='subscription-vat-container'>
          <InputField
            label='VAT'
            type='text'
            className='inputfield'
            id='subscriptionEconomicsVat'
            value={economicsVat}
            onChange={(e) => setEconomicsVat(e.target.value)}
            disabled={editModeDisabled}
          />
        </div>
      </div>
      <div className='subscription-buttons buttons-style'>
        <SaveButton
          onClick={handleSaveButtonClick}
          disabled={saveButtonDisabled}
          id='subscription-save-button'
        />
        <EditButton
          disabled={editButtonDisabled}
          onClick={handleEditButtonClick}
          id='subscription-edit-button'
        />
        <UndoButton
          disabled={undoButtonDisabled}
          onClick={handleUndoButtonClick}
          id='subscription-undo-button'
        />
        <DeleteButton id='subscription-delete-button' />
        <GenerateInvoiceButton
          id='subscription-generate-invoice'
          onClick={handleGenerateInvoice}
        />
        <GenerateMultipleInvoicesButton
          id='subscription-generate-multiple-invoices'
          onClick={handleGenerateMultipleInvoices}
        />
      </div>
    </div>
  );
};

export default SubscriptionPage;
