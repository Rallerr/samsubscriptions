import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router';
import InvoiceList from '../../Components/InvoiceList/InvoiceList';
import SidePanel from '../../Components/SidePanel/SidePanel';
import { ValidateToken } from '../../Services/AuthService';
import { GetEnterprisesWithSubscription } from '../../Services/EnterpriseService';
import './InvoicePage.css';

const InvoicePage = ({ setAuthenticated }) => {
  const [enterprises, setEnterprises] = useState([]);
  const [selectedEnterprise, setSelectedEnterprise] = useState();
  const [showLoading, setShowLoading] = useState(false);

  useEffect(() => {
    const auth = async () => {
      const isAuth = await ValidateToken();
      sessionStorage.setItem('authenticated', isAuth);
      setAuthenticated(isAuth);
    };

    auth();
  }, [setAuthenticated]);

  useEffect(() => {
    const getData = async () => {
      const enterpriseList = await GetEnterprisesWithSubscription();
      if (enterpriseList !== undefined && enterpriseList.length > 0) {
        setEnterprises(enterpriseList);
      }
    };

    getData();
  }, []);

  if (sessionStorage.getItem('authenticated') === 'false') {
    sessionStorage.removeItem('user');
    return <Redirect to='/login' />;
  }

  const handleMenuItemClick = async (id) => {
    var enterprise = enterprises.find((item) => item.id === id);
    setSelectedEnterprise(enterprise.economicsCustomerNumber);
  };

  return (
    <div className='invoice-grid-container'>
      <div className='invoice-sidepanel'>
        <SidePanel onItemClick={handleMenuItemClick} menuItems={enterprises} />
      </div>
      <div className='invoice-content'>
        <div className='invoicelist-container'>
          <InvoiceList
            customerNumber={selectedEnterprise}
            showLoading={showLoading}
            setShowLoading={setShowLoading}
          />
        </div>
      </div>
    </div>
  );
};

export default InvoicePage;
