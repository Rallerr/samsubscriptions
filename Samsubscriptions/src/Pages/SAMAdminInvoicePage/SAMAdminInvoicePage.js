import React, { useEffect, useState } from 'react';
import InvoiceList from '../../Components/InvoiceList/InvoiceList';
import { GetEnterpriseById } from '../../Services/EnterpriseService';
import './SAMAdminInvoicePage.css';

const SAMAdminInvoicePage = () => {
  const [enterprise, setEnterprise] = useState();
  const [showLoading, setShowLoading] = useState(false);

  useEffect(() => {
    setShowLoading(true);
    const enterpriseId = window.location.pathname.substring(
      window.location.pathname.indexOf('/', 2) + 1
    );

    const getData = async () => {
      const enterpriseData = await GetEnterpriseById(enterpriseId);

      console.log(enterpriseData);
      if (enterpriseData !== undefined) {
        setEnterprise(enterpriseData);
      }
    };
    getData();
    //setShowLoading(false);
  }, []);

  return (
    <div className='samadmin-invoices-grid'>
      <div className='samadmin-invoices-leftspace'></div>
      <div className='samadmin-invoices-content'>
        {enterprise === undefined ? (
          <></>
        ) : (
          <InvoiceList
            customerNumber={enterprise.economicsCustomerNumber}
            showLoading={showLoading}
            setShowLoading={setShowLoading}
          />
        )}
      </div>
      <div className='samadmin-invoices-rightspace'></div>
    </div>
  );
};

export default SAMAdminInvoicePage;
