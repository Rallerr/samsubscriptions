import React, { useEffect, useState } from 'react';
import EditButton from '../../Components/EditButton/EditButton';
import LoadingSpinner from '../../Components/LoadingSpinner/LoadingSpinner';
import SAMAdminModuleTable from '../../Components/SAMAdminModuleTable/SAMAdminModuleTable';
import SaveButton from '../../Components/SaveButton/SaveButton';
import SubscriptionPrices from '../../Components/SubscriptionPrices/SubscriptionPrices';
import UndoButton from '../../Components/UndoButton/UndoButton';
import { GetActiveModulesBySubscription } from '../../Services/ActiveModuleService';
import { ValidateToken } from '../../Services/AuthService';
import { GetActiveConcurrentUsersBySubscriptionId } from '../../Services/ConcurrentUsersService';
import {
  GetActivePriceMultipliersBySubscription,
  GetPriceMultiplierById,
} from '../../Services/PriceMultiplierService';
import { GetSubscriptionByEnterprise } from '../../Services/SubscriptionService';
import { GetSubscriptionTypeById } from '../../Services/SubscriptionTypeService';
import InputField from '../../Components/InputField/InputField';
import './SAMAdminSubscriptionPage.css';
import {
  UpdateConcurrentUsers,
  UpdateModules,
} from '../../Utils/SubscriptionUtils';
import { Redirect } from 'react-router';
import { GetEnterpriseById } from '../../Services/EnterpriseService';

const SAMAdminSubscriptionPage = ({
  setAuthenticated,
  editState,
  setEditState,
}) => {
  const [enterpriseId, setEnterpriseId] = useState('');
  const [subscription, setSubscription] = useState({ discount: 0 });
  const [samModules, setSamModules] = useState([]);
  const [concurrentUsers, setConcurrentUsers] = useState({ camo: 0, mro: 0 });
  const [activeModules, setActiveModules] = useState([]);
  const [activePriceMultipliers, setActivePriceMultipliers] = useState([]);
  const [subscriptionType, setSubscriptionType] = useState([]);
  const [moduleSettingValues, setModuleSettingValues] = useState([]);
  const [subscriptionLoaded, setSubscriptionLoaded] = useState(false);
  const [selectedModules, setSelectedModules] = useState([]);
  const [showLoading, setShowLoading] = useState(false);
  const [saveButtonDisabled, setSaveButtonDisabled] = useState(true);
  const [editButtonDisabled, setEditButtonDisabled] = useState(true);
  const [undoButtonDisabled, setUndoButtonDisabled] = useState(true);
  const [editModeDisabled, setEditModeDisabled] = useState(true);
  const [notFound, setNotFound] = useState(false);

  useEffect(() => {
    const auth = async () => {
      const isAuth = await ValidateToken();
      sessionStorage.setItem('authenticated', isAuth);
      setAuthenticated(isAuth);
    };

    auth();
  }, [setAuthenticated]);

  useEffect(() => {
    const getData = async () => {
      const entId = window.location.pathname.substring(
        window.location.pathname.indexOf('/', 2) + 1
      );
      const response = await GetEnterpriseById(entId);
      console.log(response);
      if (response === undefined || response === null || response === '') {
        setNotFound(true);
      }
    };

    getData();
  }, [enterpriseId]);

  console.log(enterpriseId);
  console.log(notFound);
  useEffect(() => {
    const enterprise = window.location.pathname.substring(
      window.location.pathname.indexOf('/', 2) + 1
    );

    setEnterpriseId(enterprise);
  }, []);

  if (sessionStorage.getItem('authenticated') === 'false') {
    sessionStorage.removeItem('user');
    return <Redirect to='/login' />;
  }

  if (notFound === true) {
    return <Redirect to='/login' />;
  }

  const loadSubscription = async () => {
    setShowLoading(true);
    //Get subscription
    const subscriptionData = await GetSubscriptionByEnterprise(enterpriseId);
    console.log(subscriptionData);
    if (subscriptionData !== undefined && subscriptionData !== null) {
      setSubscription(subscriptionData);
    }

    //Get active concurrent users
    const users = await GetActiveConcurrentUsersBySubscriptionId(
      subscriptionData.id
    );

    if (users !== undefined) {
      setConcurrentUsers(users);
    }

    //Get active pricemultipliers
    if (subscriptionData !== undefined) {
      const activeMultiplierList =
        await GetActivePriceMultipliersBySubscription(subscriptionData.id);

      if (
        activeMultiplierList !== undefined &&
        activeMultiplierList.length > 0
      ) {
        activeMultiplierList.map(async (item) => {
          const multiplier = await GetPriceMultiplierById(
            item.priceMultiplierId
          );
          if (multiplier !== undefined) {
            setActivePriceMultipliers((oldMultipliers) => [
              ...oldMultipliers,
              multiplier,
            ]);
          }
        });
      }
    }

    //Get subscription type
    const subType = await GetSubscriptionTypeById(
      subscriptionData.subscriptionTypeId
    );

    if (subType !== undefined) {
      setSubscriptionType(subType);
    }

    //Get active modules
    var activeModuleList = await GetActiveModulesBySubscription(
      subscriptionData.id
    );
    if (activeModuleList !== undefined && activeModuleList.length > 0) {
      setActiveModules(activeModuleList);
    }

    setEditButtonDisabled(false);

    setShowLoading(false);
  };

  const resetSubscription = () => {
    setSubscription({ discount: 0 });
    setConcurrentUsers({ camo: 0, mro: 0 });
    setActiveModules([]);
    setActivePriceMultipliers([]);
    setSubscriptionType([]);
    setModuleSettingValues([]);
    setSelectedModules([]);
  };

  const handleCCUsersChange = (e) => {
    setConcurrentUsers((prevUsers) => ({
      ...prevUsers,

      [e.target.name]: Number.isNaN(e.target.value) ? 0 : e.target.value,
    }));
  };

  const handleEditButtonClick = () => {
    setEditState('EDIT_SUBSCRIPTION');
    setEditModeDisabled(false);
    setSaveButtonDisabled(false);
    setUndoButtonDisabled(false);
    setEditButtonDisabled(true);
  };

  const handleUndoButtonClicked = () => {
    resetSubscription();
    loadSubscription();
    setEditModeDisabled(true);
    setSaveButtonDisabled(true);
    setUndoButtonDisabled(true);
    setEditButtonDisabled(false);
  };

  const handleSaveButtonClicked = async () => {
    if (editState === 'EDIT_SUBSCRIPTION') {
      try {
        setShowLoading(true);
        await UpdateConcurrentUsers(concurrentUsers, subscription.id);

        await UpdateModules(samModules, activeModules, subscription);
        setShowLoading(false);
      } catch (err) {}
    }
  };

  return (
    <div className='samadmin-subscription-grid'>
      <div className='samadmin-subscription-leftspace'></div>
      <div className='samadmin-subscription-content'>
        <div className='samadmin-subscription-settings'>
          <div className='samadmin-loadsubscription-container'>
            <button
              className='samadmin-loadsubscription-button'
              onClick={loadSubscription}
            >
              Load Subscription
            </button>
          </div>
          <div className='samadmin-subscription-ccusers-container'>
            <div style={{ marginBottom: '15px' }}>
              <InputField
                className='samadmin-subscription-inputfield'
                label='CAMO'
                name='camo'
                type='number'
                value={concurrentUsers.camo}
                disabled={editModeDisabled}
                onChange={handleCCUsersChange}
              />
            </div>
            <div>
              <InputField
                className='samadmin-subscription-inputfield'
                label='MRO'
                name='mro'
                type='number'
                disabled={editModeDisabled}
                value={concurrentUsers.mro}
                onChange={handleCCUsersChange}
              />
            </div>
          </div>
        </div>
        <div className='samadmin-subscription-modules-grid'>
          <div className='samadmin-subscription-modules'>
            {showLoading ? (
              <div className='samadmin-subscription-loadingspinner '>
                <LoadingSpinner height='50px' width='50px' />
              </div>
            ) : (
              <></>
            )}
            <SAMAdminModuleTable
              samModules={samModules}
              setSamModules={setSamModules}
              activeModules={activeModules}
              setActiveModules={setActiveModules}
              editState={editState}
              activePriceMultipliers={activePriceMultipliers}
              subscriptionType={subscriptionType.type}
              moduleSettingValues={moduleSettingValues}
              setModuleSettingValues={setModuleSettingValues}
              subscriptionLoaded={subscriptionLoaded}
              setSubscriptionLoaded={setSubscriptionLoaded}
              concurrentUsers={concurrentUsers}
              setSelectedModules={setSelectedModules}
              setShowLoading={setShowLoading}
              showLoading={showLoading}
              disabled={editModeDisabled}
            />
          </div>
          <div className='samadmin-subscription-prices'>
            <SubscriptionPrices
              selectedModules={selectedModules}
              activeModules={activeModules}
              discount={subscription.discount}
              subscriptionLoaded={subscriptionLoaded}
            />
          </div>
        </div>
      </div>
      <div className='samadmin-subscription-buttons buttons-style'>
        <SaveButton
          disabled={saveButtonDisabled}
          onClick={handleSaveButtonClicked}
        />
        <EditButton
          disabled={editButtonDisabled}
          onClick={handleEditButtonClick}
        />
        <UndoButton
          disabled={undoButtonDisabled}
          onClick={handleUndoButtonClicked}
        />
      </div>
      <div className='samadin-subscription-rightspace'></div>
    </div>
  );
};

export default SAMAdminSubscriptionPage;
