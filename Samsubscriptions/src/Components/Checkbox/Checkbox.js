import React, { useEffect, useState } from 'react';

const Checkbox = ({
  value,
  id,
  title,
  enabledItems,
  type,
  setSelectedItems,
}) => {
  const [checked, setChecked] = useState(false);

  useEffect(() => {
    if (type === 'settings') {
      if (enabledItems !== undefined && enabledItems.length > 0) {
        enabledItems.map((item) => {
          if (item.settingId === id) {
            setChecked(true);
          } else {
            setChecked(false);
          }
          return '';
        });
      } else {
        setChecked(false);
      }
    } else if (type === 'reqModules') {
      if (enabledItems !== undefined && enabledItems.length > 0) {
        enabledItems.map((item) => {
          if (item.requiredModuleId === id) {
            setChecked(true);
          } else {
            setChecked(false);
          }
          return '';
        });
      } else {
        setChecked(false);
      }
    }
  }, [enabledItems, id, type]);

  useEffect(() => {
    //Get all checkboxes and set state if checked
    const checkboxes = document.querySelectorAll(`[name=${type}`);
    let selectedItemsArr = [];
    for (let i = 0; i < checkboxes.length; i++) {
      const checked = checkboxes[i].checked;
      const id = checkboxes[i].id;
      const value = checkboxes[i].value;
      if (checked === true) {
        selectedItemsArr.push({ id: id, checked: checked, value: value });
      }
    }
    setSelectedItems(selectedItemsArr);
  }, [checked, setSelectedItems, id, type]);

  return (
    <>
      <div className='checkbox-container'>
        <input
          type='checkbox'
          className='checkbox'
          value={value}
          name={type}
          id={id}
          onChange={(e) => setChecked(e.target.checked)}
          checked={checked}
          disabled
        />
        <label htmlFor={id}>{title}</label>
      </div>
      <br />
    </>
  );
};

export default Checkbox;
