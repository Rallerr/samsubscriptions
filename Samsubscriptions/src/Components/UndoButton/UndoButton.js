import React from 'react';
import { IoArrowUndo } from 'react-icons/io5';
import './UndoButton.css';

const UndoButton = ({ disabled, onClick, id }) => {
  return (
    <button
      className='undo-button'
      id={id}
      onClick={onClick}
      disabled={disabled}
    >
      <IoArrowUndo size='2em' color='white' />
    </button>
  );
};

export default UndoButton;
