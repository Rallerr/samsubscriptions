import React from 'react';
import DropdownItem from '../DropdownItem/DropdownItem';

const SubscriptionTypeDropdown = ({
  id,
  items,
  onChange,
  value,
  className,
  disabled,
}) => {
  return (
    <select
      disabled={disabled}
      className={className === undefined ? 'dropdown' : className}
      id={id}
      value={value.id}
      onChange={(e) => onChange({ id: e.target.value, name: e.target.name })}
    >
      {items !== undefined && items.length > 0 ? (
        items.map((item, index) => {
          return <DropdownItem label={item.type} value={item.id} key={index} />;
        })
      ) : (
        <></>
      )}
    </select>
  );
};

export default SubscriptionTypeDropdown;
