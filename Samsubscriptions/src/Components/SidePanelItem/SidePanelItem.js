import React from 'react';
import './SidePanelItem.css';

const SidePanelItem = ({ title, onClick, dataId }) => {
  return (
    <div
      className='sidepanel-menuitem'
      onClick={onClick}
      id={dataId}
      data-id={dataId}
    >
      {title}
    </div>
  );
};

export default SidePanelItem;
