import React, { useEffect, useState } from 'react';
import { FiClock } from 'react-icons/fi';
import { CalcModulePrice } from '../../Utils/Calculations';
import { GetModuleGroupById } from '../../Services/ModuleGroupService';
import { GetModuleSettingsByModule } from '../../Services/ModuleSettingService';
import { GetPaymentFrequencyById } from '../../Services/PaymentFrequencyService';
import { GetRequiredModulesByModuleId } from '../../Services/RequiredModuleService';
import ModuleSetting from '../ModuleSetting/ModuleSetting';
import './SubscriptionModule.css';
import useForceUpdate from 'use-force-update';

const SubscriptionModule = ({
  editState,
  module,
  activeModules,
  subscriptionType,
  concurrentUsers,
  selectedActivePriceMultipliers,
  subscriptionLoaded,
  setSelectedModules,
  moduleSettingValues,
  setModuleSettingValues,
  change,
  notPaidInactiveModules,
  disabled,
  setShowLoading,
}) => {
  const [moduleSettings, setModuleSettings] = useState([]);
  const [activeModule, setActiveModule] = useState();
  const [modulePrice, setModulePrice] = useState(module.price);
  const [paymentFrequency, setPaymentFrequency] = useState({});
  const [requiredModules, setRequiredModules] = useState([]);
  const [priceLoaded, setPriceLoaded] = useState(false);
  const [group, setGroup] = useState({ name: '' });
  const [isInactive, setIsInactive] = useState(false);
  const [inactiveIconTooltipText, setInactiveIconTooltipText] = useState('');

  const forceUpdate = useForceUpdate();

  //Get all enabled module settings
  useEffect(() => {
    const getData = async () => {
      const settings = await GetModuleSettingsByModule(module.id);
      setModuleSettings(settings);

      const moduleGroup = await GetModuleGroupById(module.groupId);
      setGroup(moduleGroup);

      const reqModule = await GetRequiredModulesByModuleId(module.id);
      setRequiredModules(reqModule);

      const frequency = await GetPaymentFrequencyById(
        module.paymentFrequencyId
      );
      setPaymentFrequency(frequency);
    };
    getData();
  }, [module]);

  /*useEffect(() => {
    const getData = async () => {
      const moduleGroup = await GetModuleGroupById(module.groupId);
      setGroup(moduleGroup);
    };
    getData();
  }, [module]);

  useEffect(() => {
    const getData = async () => {
      const reqModule = await GetRequiredModulesByModuleId(module.id);
      setRequiredModules(reqModule);
    };
    getData();
  }, [module]);*/

  /*useEffect(() => {
    const foundInactiveModule = notPaidInactiveModules.find(
      (item) => item.moduleId === module.id
    );
    if (foundInactiveModule !== undefined) {
      setIsInactive(true);
      setInactiveIconTooltipText(
        `Expire at: ${foundInactiveModule.expiration}`
      );
    } else {
      setIsInactive(false);
    }
  }, [module, notPaidInactiveModules]);*/

  //Check if module is active

  useEffect(() => {
    const foundActiveModule = activeModules.find(
      (element) => element.moduleId === module.id
    );

    setActiveModule(foundActiveModule);

    if (foundActiveModule === undefined) {
      change(false, module.id);
    } else {
      change(true, module.id);
    }
    forceUpdate();
  }, [activeModules, notPaidInactiveModules, module]);

  //Set module's payment frequency
  /*useEffect(() => {
    const getPaymentFrequency = async () => {
      const frequency = await GetPaymentFrequencyById(
        module.paymentFrequencyId
      );
      setPaymentFrequency(frequency);
    };
    getPaymentFrequency();
  }, [module]);*/

  //Set calculated module price
  useEffect(() => {
    const getPrice = async () => {
      const price = CalcModulePrice(
        module,
        activeModule,
        subscriptionType,
        concurrentUsers.camo,
        concurrentUsers.mro,
        selectedActivePriceMultipliers,
        moduleSettingValues,
        group.name,
        module.price
      );
      setModulePrice(price);
      //console.log('price', module.name, price);
    };
    if (subscriptionLoaded === true || editState === 'NEW_SUBSCRIPTION') {
      if (
        !Number.isNaN(concurrentUsers.camo) &&
        !Number.isNaN(concurrentUsers.mro)
      ) {
        getPrice();
      }
      setPriceLoaded(true);
    }
  }, [
    module,
    activeModule,
    editState,
    subscriptionLoaded,
    module.price,
    subscriptionType,
    concurrentUsers.camo,
    concurrentUsers.mro,
    selectedActivePriceMultipliers,
    moduleSettingValues,
  ]);

  //get all checkboxes and add to state if checked
  useEffect(() => {
    if (priceLoaded === true) {
      const checkboxes = document.querySelectorAll('[name=module');
      let moduleArr = [];
      for (let i = 0; i < checkboxes.length; i++) {
        const checked = checkboxes[i].checked;
        if (checked === true) {
          /*const isModuleInactive = notPaidInactiveModules.find(
            (item) => item.moduleId === checkboxes[i].id
          );
          if (isModuleInactive === undefined) {
            
          }*/
          moduleArr.push(checkboxes[i]);
        }
      }

      setSelectedModules(moduleArr);
    }
  }, [module.isChecked, modulePrice, priceLoaded, setSelectedModules]);

  //Set required modules
  useEffect(() => {
    requiredModules.map((item) => {
      if (module.isChecked === true) {
        change(true, item.requiredModuleId);
      }
      return '';
    });
  }, [module.isChecked, requiredModules]);

  return (
    <div className='subscription-module-container'>
      <input
        disabled={disabled}
        type='checkbox'
        className='subscription-module-checkbox'
        value={modulePrice}
        data-paymentfrequency={paymentFrequency.name}
        data-firsttimeprice={module.firsttimePrice}
        name='module'
        id={module.id}
        onChange={(e) => change(e.target.checked, e.target.id)}
        checked={module.isChecked}
      />
      {isInactive ? (
        <FiClock color='red' title={inactiveIconTooltipText} />
      ) : (
        <></>
      )}
      <div className='subscription-module-title-container'>
        <label htmlFor={module.id}>{module.name}</label>
      </div>
      {moduleSettings.length > 0 ? (
        moduleSettings.map((item, index) => {
          return (
            <div className='subscription-setting-container' key={index}>
              <ModuleSetting
                editState={editState}
                moduleSetting={item}
                index={index}
                activeModule={activeModule}
                module={module}
                disabled={disabled}
                moduleSettingValues={moduleSettingValues}
                setModuleSettingValues={setModuleSettingValues}
              />
            </div>
          );
        })
      ) : (
        <></>
      )}
      <div className='subscription-module-price-container'>
        {paymentFrequency.name}
        <span style={{ marginLeft: '25px' }}>{modulePrice}</span>
      </div>
    </div>
  );
};

export default SubscriptionModule;
