import React from 'react';
import DropdownItem from '../DropdownItem/DropdownItem';

const EconomicsLayoutDropdown = ({
  onChange,
  value,
  items,
  id,
  className,
  disabled,
}) => {
  return (
    <select
      id={id}
      value={value}
      className={className === undefined ? 'dropdown' : className}
      onChange={(e) => {
        onChange(e.target.value);
      }}
      disabled={disabled}
    >
      {items !== undefined && items.length > 0 ? (
        items.map((item, index) => {
          return (
            <DropdownItem
              label={item.name}
              value={item.layoutNumber}
              key={index}
            />
          );
        })
      ) : (
        <></>
      )}
    </select>
  );
};

export default EconomicsLayoutDropdown;
