import React, { useEffect, useState } from 'react';
import { GetModuleGroups } from '../../Services/ModuleGroupService';
import DropdownItem from '../DropdownItem/DropdownItem';

const ModuleGroupDropdown = ({
  onChange,
  value,
  id,
  className,
  editState,
  setShowLoading,
  disabled,
}) => {
  const [moduleGroups, setModuleGroups] = useState([]);

  useEffect(() => {
    const getData = async () => {
      setShowLoading(true);
      const data = await GetModuleGroups();
      setModuleGroups(data);
      if (data !== undefined && data.length > 0) {
        onChange(data[0].id);
      }
      setShowLoading(false);
    };
    getData();
  }, []);

  useEffect(() => {
    if (
      moduleGroups !== undefined &&
      moduleGroups.length > 0 &&
      editState === 'NEW_MODULE'
    ) {
      onChange(moduleGroups[0].id);
    }
  }, [editState]);

  return (
    <select
      disabled={disabled}
      className={className === undefined ? 'dropdown' : className}
      onChange={(e) => onChange(e.target.value)}
      value={value}
      id={id}
    >
      {moduleGroups !== undefined && moduleGroups.length > 0 ? (
        moduleGroups.map((item, index) => {
          return <DropdownItem label={item.name} value={item.id} key={index} />;
        })
      ) : (
        <></>
      )}
    </select>
  );
};

export default ModuleGroupDropdown;
