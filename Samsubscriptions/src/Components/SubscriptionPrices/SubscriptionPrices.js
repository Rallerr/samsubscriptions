import React, { useEffect, useState } from 'react';
import {
  CalculateMonthlyPrice,
  CalculateNextMonthlyCCUsersBill,
  CalculateStartUpPrice,
} from '../../Utils/Calculations';
import './SubscriptionPrices.css';

const SubscriptionPrices = ({
  selectedModules,
  activeModules,
  discount,
  subscriptionLoaded,
}) => {
  const [startupPayment, setStartupPayment] = useState(0);
  const [monthlyPayment, setMonthlyPayment] = useState(0);
  const [nextInvoice, setNextInvoice] = useState(0);

  useEffect(() => {
    var startPrice = CalculateStartUpPrice(
      selectedModules,
      activeModules,
      discount
    );
    setStartupPayment(startPrice);
  }, [selectedModules, activeModules, discount]);

  useEffect(() => {
    var monthlyPrice = CalculateMonthlyPrice(selectedModules, discount);
    setMonthlyPayment(monthlyPrice);
  }, [selectedModules, discount]);

  useEffect(() => {
    if (subscriptionLoaded === true) {
      var nextInvoicePrice = CalculateNextMonthlyCCUsersBill(
        startupPayment,
        monthlyPayment
      );
      setNextInvoice(nextInvoicePrice);
    }
  }, [startupPayment, monthlyPayment]);

  return (
    <div className='subscription-prices-container'>
      <div className='subscription-prices-flex'>
        <div>Startup payment</div>
        <div className='subscription-prices-pricestyle'>{startupPayment}</div>
      </div>
      <div className='subscription-prices-flex'>
        <div>Monthly payment</div>
        <div className='subscription-prices-pricestyle'>{monthlyPayment}</div>
      </div>
      <div className='subscription-prices-flex'>
        <div>Next invoice</div>
        <div className='subscription-prices-pricestyle'>{nextInvoice}</div>
      </div>
    </div>
  );
};

export default SubscriptionPrices;
