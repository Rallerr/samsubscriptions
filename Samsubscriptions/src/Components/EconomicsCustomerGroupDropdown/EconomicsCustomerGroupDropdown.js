import React from 'react';
import DropdownItem from '../DropdownItem/DropdownItem';

const EconomicsCustomerGroupDropdown = ({
  onChange,
  value,
  items,
  id,
  className,
  disabled,
}) => {
  return (
    <select
      className={className === undefined ? 'dropdown' : className}
      value={value}
      onChange={(e) => onChange(e.target.value)}
      id={id}
      disabled={disabled}
    >
      {items !== undefined && items.length > 0 ? (
        items.map((item, index) => {
          return (
            <DropdownItem
              key={index}
              value={item.customerGroupNumber}
              id={item.customerGroupNumber}
              label={item.name}
            />
          );
        })
      ) : (
        <></>
      )}
    </select>
  );
};

export default EconomicsCustomerGroupDropdown;
