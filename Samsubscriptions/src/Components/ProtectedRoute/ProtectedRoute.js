import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const ProtectedRoute = ({ children, roles, ...rest }) => {
  return (
    <Route
      {...rest}
      render={() => {
        const user = JSON.parse(sessionStorage.getItem('user'));
        const isAuth = sessionStorage.getItem('authenticated');
        if (!user) {
          return <Redirect to='/login' />;
        }

        if (isAuth === 'false') {
          return <Redirect to='/login' />;
        }

        if (roles && roles.indexOf(user.role) === -1) {
          return <Redirect to='/login' />;
        }

        return children;
      }}
    />
  );
};

export default ProtectedRoute;
