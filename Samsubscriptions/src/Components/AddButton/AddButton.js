import React from 'react';
import { FiPlus } from 'react-icons/fi';
import './AddButton.css';

const AddButton = ({ id, disabled, onClick }) => {
  return (
    <button
      className='add-button'
      id={id}
      disabled={disabled}
      onClick={onClick}
    >
      <FiPlus color='white' size='1.5em' />
    </button>
  );
};

export default AddButton;
