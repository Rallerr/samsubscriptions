import React from 'react';
import './InputField.css';

const InputField = ({
  value,
  label,
  name,
  placeholder,
  type,
  className,
  onChange,
  disabled,
  id,
}) => {
  return (
    <>
      <label htmlFor={id}>{label}</label>
      <input
        type={type}
        id={id}
        value={value}
        name={name}
        className={className}
        placeholder={placeholder}
        onChange={(e) => onChange(e)}
        disabled={disabled}
      />
    </>
  );
};

export default InputField;
