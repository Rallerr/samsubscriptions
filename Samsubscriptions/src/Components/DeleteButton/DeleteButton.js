import React from 'react';
import { MdOutlineDeleteOutline } from 'react-icons/md';
import './DeleteButton.css';

const DeleteButton = ({ id }) => {
  return (
    <button className='delete-button' id={id}>
      <MdOutlineDeleteOutline color='white' size='2em' />
    </button>
  );
};

export default DeleteButton;
