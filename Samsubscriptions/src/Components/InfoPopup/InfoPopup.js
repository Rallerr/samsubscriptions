import React, { useEffect, useState } from 'react';
import './InfoPopup.css';

const InfoPopup = ({ popupText, triggerState }) => {
  const [show, setShow] = useState(true);

  useEffect(() => {
    const timer = setTimeout(() => {
      setShow(false);
    }, 3000);

    const triggerTimer = setTimeout(() => {
      triggerState(false);
    }, 4000);

    return () => {
      clearTimeout(timer);
      clearTimeout(triggerTimer);
    };
  });

  return (
    <div className={show ? 'info-popup fadeIn' : 'info-popup fadeOut'}>
      <div className='info-popup-text'>{popupText}</div>
    </div>
  );
};

export default InfoPopup;
