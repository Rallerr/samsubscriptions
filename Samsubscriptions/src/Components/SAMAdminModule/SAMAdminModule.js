import React, { useEffect, useState } from 'react';
import { GetModuleGroupById } from '../../Services/ModuleGroupService';
import { GetModuleSettingsByModule } from '../../Services/ModuleSettingService';
import { GetRequiredModulesByModuleId } from '../../Services/RequiredModuleService';
import { CalcModulePrice } from '../../Utils/Calculations';
import ModuleSetting from '../ModuleSetting/ModuleSetting';
import useForceUpdate from 'use-force-update';

const SAMAdminModule = ({
  editState,
  module,
  activeModules,
  subscriptionType,
  concurrentUsers,
  selectedActivePriceMultipliers,
  subscriptionLoaded,
  setSelectedModules,
  moduleSettingValues,
  setModuleSettingValues,
  change,
  notPaidInactiveModules,
}) => {
  const [moduleSettings, setModuleSettings] = useState([]);
  const [activeModule, setActiveModule] = useState();
  const [modulePrice, setModulePrice] = useState(module.price);
  const [paymentFrequency, setPaymentFrequency] = useState({});
  const [requiredModules, setRequiredModules] = useState([]);
  const [priceLoaded, setPriceLoaded] = useState(false);
  const [group, setGroup] = useState({ name: '' });
  const [isInactive, setIsInactive] = useState(false);

  //Get all enabled module settings
  useEffect(() => {
    const getData = async () => {
      const settings = await GetModuleSettingsByModule(module.id);
      setModuleSettings(settings);

      const moduleGroup = await GetModuleGroupById(module.groupId);
      setGroup(moduleGroup);

      const reqModule = await GetRequiredModulesByModuleId(module.id);
      setRequiredModules(reqModule);
    };
    getData();
  }, [module]);

  useEffect(() => {
    const getPrice = async () => {
      const price = CalcModulePrice(
        module,
        activeModule,
        subscriptionType,
        concurrentUsers.camo,
        concurrentUsers.mro,
        selectedActivePriceMultipliers,
        moduleSettingValues,
        group.name,
        module.price
      );
      setModulePrice(price);
    };
    if (subscriptionLoaded === true || editState === 'NEW_SUBSCRIPTION') {
      if (
        !Number.isNaN(concurrentUsers.camo) &&
        !Number.isNaN(concurrentUsers.mro)
      ) {
        getPrice();
      }

      setPriceLoaded(true);
    }
  }, [
    module,
    activeModule,
    editState,
    subscriptionLoaded,
    module.price,
    subscriptionType,
    concurrentUsers.camo,
    concurrentUsers.mro,
    selectedActivePriceMultipliers,
    moduleSettingValues,
    group.name,
  ]);

  return (
    <div className='subscription-module-container'>
      <input
        disabled
        type='checkbox'
        className='subscription-module-checkbox'
        value={modulePrice}
        data-paymentfrequency={paymentFrequency.name}
        data-firsttimeprice={module.firsttimePrice}
        name='module'
        id={module.id}
        onChange={(e) => change(e.target.checked, e.target.id)}
        checked={module.isChecked}
      />
      <div className='subscription-module-title-container'>
        <label htmlFor={module.id}>{module.name}</label>
      </div>
      {moduleSettings.length > 0 ? (
        moduleSettings.map((item, index) => {
          return (
            <div className='subscription-setting-container' key={index}>
              <ModuleSetting
                editState={editState}
                moduleSetting={item}
                index={index}
                activeModule={activeModule}
                module={module}
                moduleSettingValues={moduleSettingValues}
                setModuleSettingValues={setModuleSettingValues}
              />
            </div>
          );
        })
      ) : (
        <></>
      )}
      <div className='subscription-module-price-container'>
        {paymentFrequency.name}
        <span style={{ marginLeft: '25px' }}>{modulePrice}</span>
      </div>
    </div>
  );
};

export default SAMAdminModule;
