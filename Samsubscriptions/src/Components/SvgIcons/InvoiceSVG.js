import React from 'react';
import invoice from '../../Images/invoice.svg';

const InvoiceSVG = ({ height, width }) => {
  return <img src={invoice} height={height} width={width} alt='' />;
};

export default InvoiceSVG;
