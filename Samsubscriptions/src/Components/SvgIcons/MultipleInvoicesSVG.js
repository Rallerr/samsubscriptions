import React from 'react';
import invoice from '../../Images/multiple_Invoices2.svg';

const MultipleInvoicesSVG = () => {
  return <img src={invoice} height='100%' width='100%' alt='' />;
};

export default MultipleInvoicesSVG;
