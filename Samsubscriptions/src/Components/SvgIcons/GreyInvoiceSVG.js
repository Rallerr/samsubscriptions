import React from 'react';
import invoice from '../../Images/invoice_black.svg';

const GreyInvoiceSVG = ({ height, width }) => {
  return <img src={invoice} height={height} width={width} alt='' />;
};

export default GreyInvoiceSVG;
