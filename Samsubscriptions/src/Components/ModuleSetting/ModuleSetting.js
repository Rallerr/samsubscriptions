import React, { useEffect, useState } from 'react';
import { GetActiveModuleSettingValue } from '../../Services/ModuleSettingService';

const ModuleSetting = ({
  moduleSetting,
  activeModule,
  moduleSettingValues,
  setModuleSettingValues,
  disabled,
}) => {
  const [settingValue, setSettingValue] = useState({
    settingValueId: '',
    moduleSettingId: moduleSetting.id,
    activeModuleId: '',
    moduleId: moduleSetting.moduleId,
    value: '1',
  });

  useEffect(() => {
    const getData = async () => {
      const data = await GetActiveModuleSettingValue(
        moduleSetting.id,
        activeModule.id
      );
      if (data !== undefined) {
        setSettingValue((prevSettingValue) => ({
          ...prevSettingValue,
          value: data.value,
          activeModuleId: data.activeModuleId,
          settingValueId: data.id,
        }));
      }
    };

    if (activeModule !== undefined) {
      getData();
    }
  }, [activeModule]);

  useEffect(() => {
    const item = moduleSettingValues.find(
      (item) => item.moduleSettingId === moduleSetting.id
    );

    if (item === undefined) {
      setModuleSettingValues((prevValueArr) => [...prevValueArr, settingValue]);
    } else {
      const index = moduleSettingValues.indexOf(item);

      const arr = [...moduleSettingValues];

      arr[index] = settingValue;
      setModuleSettingValues(arr);
    }
  }, [settingValue]);

  return (
    <>
      <label>{moduleSetting.name}</label>
      <input
        disabled={disabled}
        type={moduleSetting.type}
        className='inputfield module-setting'
        id={moduleSetting.id}
        onChange={(e) => {
          setSettingValue((prevSettingValue) => ({
            ...prevSettingValue,
            value: e.target.value,
          }));
        }}
        value={settingValue.value}
      />
    </>
  );
};

export default ModuleSetting;
