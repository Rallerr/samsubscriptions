import React from 'react';

const DropdownItem = ({ label, value }) => {
  return <option value={value}>{label}</option>;
};

export default DropdownItem;
