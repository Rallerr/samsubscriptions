import React from 'react';
import DropdownItem from '../DropdownItem/DropdownItem';

const BillingPeriodDropdown = ({
  id,
  items,
  onChange,
  value,
  className,
  disabled,
}) => {
  return (
    <select
      disabled={disabled}
      className={className === undefined ? 'dropdown' : className}
      id={id}
      value={value}
      onChange={(e) => onChange(e.target.value)}
    >
      {items !== undefined && items.length > 0 ? (
        items.map((item, index) => {
          return (
            <DropdownItem label={item.period} value={item.id} key={index} />
          );
        })
      ) : (
        <></>
      )}
    </select>
  );
};

export default BillingPeriodDropdown;
