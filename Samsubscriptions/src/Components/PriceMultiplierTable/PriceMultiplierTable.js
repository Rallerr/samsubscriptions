import React from 'react';
import PriceMultiplier from '../PriceMultiplier/PriceMultiplier';
import './PriceMultiplierTable.css';

const PriceMultiplierTable = ({
  priceMultipliers,
  editState,
  setSelectedActivePriceMultipliers,
  activePriceMultipliers,
  disabled,
}) => {
  return (
    <div className='pricemultiplier-table' id='pricemultipliers'>
      {priceMultipliers !== undefined && priceMultipliers.length > 0 ? (
        priceMultipliers.map((item, index) => {
          return (
            <PriceMultiplier
              editState={editState}
              priceMultiplier={item}
              setSelectedActivePriceMultipliers={
                setSelectedActivePriceMultipliers
              }
              activePriceMultipliers={activePriceMultipliers}
              key={index}
              disabled={disabled}
            />
          );
        })
      ) : (
        <></>
      )}
    </div>
  );
};

export default PriceMultiplierTable;
