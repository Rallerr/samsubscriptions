import React, { useEffect, useState } from 'react';
import { GetPaymentFrequencies } from '../../Services/PaymentFrequencyService';
import DropdownItem from '../DropdownItem/DropdownItem';

const PaymentFrequencyDropdown = ({
  onChange,
  value,
  id,
  className,
  editState,
  setShowLoading,
  disabled,
}) => {
  const [paymentFrequencies, setPaymentFrequencies] = useState([]);

  useEffect(() => {
    const getData = async () => {
      setShowLoading(true);
      const data = await GetPaymentFrequencies();
      if (data !== undefined && data.length > 0) {
        setPaymentFrequencies(data);
        onChange(data[0].id);
      }
      setShowLoading(false);
    };
    getData();
  }, []);

  useEffect(() => {
    if (
      paymentFrequencies !== undefined &&
      paymentFrequencies.length > 0 &&
      editState === 'NEW_MODULE'
    ) {
      onChange(paymentFrequencies[0].id);
    }
  }, [editState]);

  return (
    <select
      className={className === undefined ? 'dropdown' : className}
      value={value}
      onChange={(e) => onChange(e.target.value)}
      id={id}
      disabled={disabled}
    >
      {paymentFrequencies !== undefined && paymentFrequencies.length > 0 ? (
        paymentFrequencies.map((item, index) => {
          return (
            <DropdownItem
              key={index}
              value={item.id}
              id={item.id}
              label={item.name}
            />
          );
        })
      ) : (
        <></>
      )}
    </select>
  );
};

export default PaymentFrequencyDropdown;
