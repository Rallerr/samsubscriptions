import React from 'react';
import Checkbox from '../Checkbox/Checkbox';
import './CheckboxTable.css';

const CheckboxTable = ({
  items,
  enabledItems,
  type,
  setSelectedItems,
  selectedModule,
  className,
}) => {
  return (
    <div className={className}>
      {items !== undefined && items.length > 0 ? (
        items.map((item, index) => {
          return (
            <Checkbox
              enabledItems={enabledItems}
              selectedModule={selectedModule}
              setSelectedItems={setSelectedItems}
              type={type}
              id={item.id}
              name={item.name}
              value={item.id}
              title={item.name}
              key={index}
            />
          );
        })
      ) : (
        <></>
      )}
    </div>
  );
};

export default CheckboxTable;
