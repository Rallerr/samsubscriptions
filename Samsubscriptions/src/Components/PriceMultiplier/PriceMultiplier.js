import React, { useEffect, useState } from 'react';

const PriceMultiplier = ({
  editState,
  priceMultiplier,
  activePriceMultipliers,
  setSelectedActivePriceMultipliers,
  disabled,
}) => {
  const [isChecked, setIsChecked] = useState(false);

  useEffect(() => {
    const isMultiplierActive = activePriceMultipliers.find(
      (element) => element.priceMultiplierId === priceMultiplier.id
    );

    if (isMultiplierActive === undefined) {
      setIsChecked(false);
    } else {
      setIsChecked(true);
    }
  }, [activePriceMultipliers, priceMultiplier.Id]);

  useEffect(() => {
    //Get all checkboxes and set state if checked
    const checkboxes = document.querySelectorAll('[name=pricemultiplier');
    let multiplierArr = [];
    for (let i = 0; i < checkboxes.length; i++) {
      const checked = checkboxes[i].checked;
      const id = checkboxes[i].id;
      const value = checkboxes[i].value;
      if (checked === true) {
        multiplierArr.push({ id: id, checked: checked, value: value });
      }
    }
    console.log(multiplierArr);
    setSelectedActivePriceMultipliers(multiplierArr);
  }, [isChecked, setSelectedActivePriceMultipliers]);

  useEffect(() => {
    if (editState === 'NEW_SUBSCRIPTION') {
      setIsChecked(false);
    }
  }, [editState]);

  return (
    <>
      <div className='checkbox-container'>
        <input
          disabled={disabled}
          type={priceMultiplier.type}
          id={priceMultiplier.id}
          value={priceMultiplier.factor}
          name='pricemultiplier'
          onChange={(e) => setIsChecked(e.target.checked)}
          checked={isChecked}
        />
        <label htmlFor={priceMultiplier.id}>{priceMultiplier.title}</label>
      </div>
      <br />
    </>
  );
};

export default PriceMultiplier;
