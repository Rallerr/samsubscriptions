import React from 'react';
import ReactLoading from 'react-loading';
import './LoadingSpinner.css';

const LoadingSpinner = ({ height, width }) => {
  const color = getComputedStyle(document.documentElement).getPropertyValue(
    '--asa-blue'
  );

  return (
    <ReactLoading
      type='spinningBubbles'
      color={color}
      height={height}
      width={width}
    />
  );
};

export default LoadingSpinner;
