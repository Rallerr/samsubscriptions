import React from 'react';
import { DownloadEconomicsBookedInvoice } from '../../Services/EconomicsServices/EconomicsBookedInvoiceService';
import { downloadPDF } from '../../Utils/FileUtils';
import GreyInvoiceSVG from '../SvgIcons/GreyInvoiceSVG';
import './InvoiceItem.css';

const InvoiceItem = ({ invoiceNumber, date, setShowLoading }) => {
  const handleClick = async () => {
    setShowLoading(true);
    const base64String = await DownloadEconomicsBookedInvoice(invoiceNumber);
    downloadPDF(base64String, `Invoice no. ${invoiceNumber} ${date}`);
    setShowLoading(false);
  };

  return (
    <div className='invoiceitem-container'>
      <GreyInvoiceSVG height='25px' width='25px' />
      <label className='invoiceitem-label' onClick={handleClick}>
        Invoice no. {invoiceNumber} {date}
      </label>
    </div>
  );
};

export default InvoiceItem;
