import React, { useEffect, useState } from 'react';
import { BsPlusLg } from 'react-icons/bs';
import { ImCancelCircle } from 'react-icons/im';
import {
  GetEnterprisesFromHubspot,
  GetEnterprisesWithSubscription,
} from '../../Services/EnterpriseService';
import './SelectEnterprise.css';

const SelectEnterprise = ({
  selectedEnterprise,
  setSelectedEnterprise,
  setShowSelectEnterprise,
  setEditState,
}) => {
  const [enterprises, setEnterprises] = useState([]);

  useEffect(() => {
    const getData = async () => {
      const enterpriseList = [];
      const data = await GetEnterprisesFromHubspot();
      const enterprisesWithSub = await GetEnterprisesWithSubscription();
      console.log(enterprisesWithSub);

      data.map((enterprise) => {
        if (enterprisesWithSub !== undefined && enterprisesWithSub.length > 0) {
          enterprisesWithSub.map((enterpriseSub) => {
            if (enterprise.id !== enterpriseSub.enterpriseNo) {
              enterpriseList.push(enterprise);
            }
            return '';
          });
        } else {
          enterpriseList.push(enterprise);
        }
        return '';
      });

      setEnterprises(enterpriseList);
      if (enterpriseList !== undefined && enterpriseList.length > 0) {
        setSelectedEnterprise({
          id: enterpriseList[0].id,
          name: enterpriseList[0].name,
        });
      }
    };
    getData();
  }, [setSelectedEnterprise]);

  const handleCancelOnClick = () => {
    setShowSelectEnterprise(false);
    setEditState('');
    setSelectedEnterprise({ id: '', name: '' });
  };

  const handleSaveOnClick = () => {
    setShowSelectEnterprise(false);

    if (
      selectedEnterprise === undefined ||
      selectedEnterprise.id === '' ||
      selectedEnterprise.name === ''
    ) {
      alert('No enterprise selected');
      setEditState('');
      setShowSelectEnterprise(false);
    }
  };

  return (
    <div className='modal' onClick={handleCancelOnClick}>
      <div
        className='select-enterprise-container'
        onClick={(e) => e.stopPropagation()}
      >
        <div className='select-enterprise-dropdown'>
          <select
            className='select-enterprise-dropdown dropdown'
            onChange={(e) => {
              setSelectedEnterprise({
                Id: e.target.value,
                Name: e.target.dataset.name,
              });
            }}
            value={selectedEnterprise.id}
          >
            {enterprises.map((item, index) => {
              return (
                <option
                  value={item.id}
                  key={index}
                  data-name={item.name}
                  id={item.id}
                >
                  {item.name}
                </option>
              );
            })}
          </select>
        </div>
        <div className='select-enterprise-buttons'>
          <button
            className='save-selected-enterprise'
            onClick={handleSaveOnClick}
          >
            <BsPlusLg size='1.5em' color='white' />
          </button>
          <button
            className='undo-selected-enterprise'
            onClick={handleCancelOnClick}
          >
            <ImCancelCircle size='1.5em' color='white' />
          </button>
        </div>
      </div>
    </div>
  );
};

export default SelectEnterprise;
