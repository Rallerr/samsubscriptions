import React from 'react';
import './FirstTimePrice.css';

const FirstTimePrice = ({ firstTimePrice }) => {
  return (
    <div className='first-time-price-container'>
      <div className='first-time-price-titlte'>First time price</div>
      <div className='first-time-price-price'>{firstTimePrice}</div>
    </div>
  );
};

export default FirstTimePrice;
