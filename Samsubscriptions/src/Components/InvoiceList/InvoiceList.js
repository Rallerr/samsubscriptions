import React, { useEffect, useState } from 'react';
import { GetEconomicsBookedInvoices } from '../../Services/EconomicsServices/EconomicsBookedInvoiceService';
import InvoiceItem from '../InvoiceItem/InvoiceItem';
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner';
import './InvoiceList.css';

const InvoiceList = ({ customerNumber, showLoading, setShowLoading }) => {
  const [invoices, setInvoices] = useState([]);

  useEffect(() => {
    const getData = async () => {
      setShowLoading(true);
      const invoiceList = await GetEconomicsBookedInvoices(customerNumber);
      console.log(invoiceList);
      if (invoiceList !== undefined && invoiceList.length > 0) {
        var sortedList = [...invoiceList].sort(
          (dateA, dateB) => dateB.date - dateA.date
        );
        setInvoices(sortedList);
      }
      setShowLoading(false);
    };

    if (customerNumber !== undefined) {
      getData();
    }
  }, [customerNumber]);

  return (
    <div className='invoicelist'>
      {showLoading ? (
        <div className='invoicelist-loadingspinner'>
          <LoadingSpinner height='50px' width='50px' />
        </div>
      ) : (
        <></>
      )}
      {invoices === undefined || invoices.length === 0 ? (
        <h3 style={{ textAlign: 'center' }}>No invoices</h3>
      ) : (
        invoices.map((item, index) => {
          return (
            <InvoiceItem
              invoiceNumber={item.bookedInvoiceNumber}
              date={item.date}
              key={index}
              setShowLoading={setShowLoading}
            />
          );
        })
      )}
      {/*<InvoiceItem invoiceNumber='1' date='23-05-2021' />
      <InvoiceItem invoiceNumber='1' date='23-05-2021' />
      <InvoiceItem invoiceNumber='1' date='23-05-2021' />
      <InvoiceItem invoiceNumber='1' date='23-05-2021' />
      <InvoiceItem invoiceNumber='1' date='23-05-2021' />
      <InvoiceItem invoiceNumber='1' date='23-05-2021' />
      <InvoiceItem invoiceNumber='1' date='23-05-2021' />*/}
    </div>
  );
};

export default InvoiceList;
