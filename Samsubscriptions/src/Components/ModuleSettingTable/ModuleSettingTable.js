import React, { useEffect } from 'react';
import CreatedModuleSetting from '../CreatedModuleSetting/CreatedModuleSetting';

const ModuleSettingTable = ({
  moduleSettings,
  setModuleSettings,
  disabled,
}) => {
  useEffect(() => {}, [moduleSettings]);

  const handleItemRemove = (name) => {
    setModuleSettings([...moduleSettings.filter((item) => item.name !== name)]);
  };

  return (
    <div>
      {moduleSettings.length > 0 ? (
        moduleSettings.map((item, index) => {
          return (
            <CreatedModuleSetting
              disabled={disabled}
              settingName={item.name}
              key={index}
              onClick={handleItemRemove}
            />
          );
        })
      ) : (
        <></>
      )}
    </div>
  );
};

export default ModuleSettingTable;
