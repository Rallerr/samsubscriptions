import React from 'react';
import { AiOutlineSave } from 'react-icons/ai';
import './SaveButton.css';

const SaveButton = ({ disabled, onClick, id }) => {
  return (
    <button
      className='save-button'
      disabled={disabled}
      onClick={onClick}
      id={id}
    >
      <AiOutlineSave size='2em' color='white'></AiOutlineSave>
    </button>
  );
};

export default SaveButton;
