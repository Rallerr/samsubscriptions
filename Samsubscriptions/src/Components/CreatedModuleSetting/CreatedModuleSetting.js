import React from 'react';
import { HiMinus } from 'react-icons/hi';

import './CreatedModuleSetting.css';

const CreatedModuleSetting = ({ settingName, onClick, disabled }) => {
  return (
    <div className='created-setting-container'>
      <label>{settingName}</label>
      <button
        className='created-setting-delete-button'
        onClick={(e) => onClick(settingName)}
        disabled={disabled}
      >
        <HiMinus color='red' size='1.7em' />
      </button>
    </div>
  );
};

export default CreatedModuleSetting;
