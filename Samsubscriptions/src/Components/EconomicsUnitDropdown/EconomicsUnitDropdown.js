import React, { useEffect, useState } from 'react';
import { GetEconomicsUnits } from '../../Services/EconomicsServices/EconomicsUnitService';
import DropdownItem from '../DropdownItem/DropdownItem';

const EconomicsUnitDropdown = ({
  onChange,
  value,
  id,
  className,
  editState,
  setShowLoading,
  disabled,
}) => {
  const [units, setUnits] = useState([]);

  useEffect(() => {
    const getUnits = async () => {
      setShowLoading(true);
      const economicsUnits = await GetEconomicsUnits();
      if (economicsUnits !== undefined && economicsUnits.length > 0) {
        setUnits(economicsUnits);
        onChange(economicsUnits[0].unitNumber);
      }
      setTimeout(() => {
        setShowLoading(false);
      }, 1000);
    };

    getUnits();
  }, []);

  useEffect(() => {
    if (units !== undefined && units.length > 0 && editState === 'NEW_MODULE') {
      onChange(units[0].unitNumber);
    }
  }, [editState]);

  return (
    <select
      className={className === undefined ? 'dropdown' : className}
      value={value}
      onChange={(e) => onChange(e.target.value)}
      id={id}
      disabled={disabled}
    >
      {units !== undefined && units.length > 0 ? (
        units.map((item, index) => {
          return (
            <DropdownItem
              key={index}
              value={item.unitNumber}
              id={item.unitNumber}
              label={item.name}
            />
          );
        })
      ) : (
        <></>
      )}
    </select>
  );
};

export default EconomicsUnitDropdown;
