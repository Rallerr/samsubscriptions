import React, { useEffect, useState } from 'react';
import './topMenuStyle.css';
import { AiOutlineHome } from 'react-icons/ai';
import { FaUsers } from 'react-icons/fa';
import { RiExchangeDollarLine } from 'react-icons/ri';
import { MdViewModule } from 'react-icons/md';
import { BsPlusLg } from 'react-icons/bs';
import { ImExit } from 'react-icons/im';
import { IoMdSettings } from 'react-icons/io';
import { Link } from 'react-router-dom';
import InvoiceSVG from '../SvgIcons/InvoiceSVG';

const TopMenu = ({ setEditState }) => {
  const [authenticated, setAuthenticated] = useState(false);
  const [role, setRole] = useState();

  useEffect(() => {
    const auth = sessionStorage.getItem('authenticated');
    auth === 'true' ? setAuthenticated(true) : setAuthenticated(false);
  });

  return (
    <div className='topnav'>
      {authenticated ? (
        <>
          <div className='align-icon-left topmenu-center-button'>
            <AiOutlineHome size='2.5em' color='white' />
          </div>

          <Link to='/enterprise'>
            <div className='align-icon-left topmenu-center-button'>
              <FaUsers size='2.5em' color='white' />
            </div>
          </Link>
          <Link to='/subscription'>
            <div className='align-icon-left topmenu-center-button'>
              <RiExchangeDollarLine size='2.5em' color='white' />
            </div>
          </Link>
          <Link to='/module'>
            <div className='align-icon-left topmenu-center-button'>
              <MdViewModule size='2.5em' color='white' />
            </div>
          </Link>
          <Link to='/invoices'>
            <div className='align-icon-left topmenu-center-button'>
              <InvoiceSVG height='100%' width='100%' />
            </div>
          </Link>
          <div
            className='align-icon-left topmenu-center-button'
            onClick={(e) => {
              if (window.location.pathname.toLowerCase() === '/subscription') {
                console.log('subscription');
                setEditState('NEW_SUBSCRIPTION');
              }

              if (window.location.pathname.toLowerCase() === '/module') {
                setEditState('NEW_MODULE');
              }
            }}
          >
            <BsPlusLg size='2.5em' color='white' />
          </div>
          <Link to='/login'>
            <div
              className='align-icon-right topmenu-center-button'
              onClick={() => {
                sessionStorage.removeItem('user');
                sessionStorage.setItem('authenticated', false);
                setAuthenticated(false);
              }}
            >
              <ImExit size='2.5em' color='white' />
            </div>
          </Link>
          <div className='align-icon-right topmenu-center-button'>
            <IoMdSettings size='2.5em' color='white' />
          </div>
        </>
      ) : (
        <></>
      )}
    </div>
  );
};

export default TopMenu;
