import React, { useEffect, useState } from 'react';
import './topMenuStyle.css';
import { RiExchangeDollarLine } from 'react-icons/ri';
import { ImExit } from 'react-icons/im';
import { Link } from 'react-router-dom';
import InvoiceSVG from '../SvgIcons/InvoiceSVG';

const SAMAdminTopMenu = () => {
  const [authenticated, setAuthenticated] = useState(false);
  const [user, setUser] = useState();

  useEffect(() => {
    const auth = sessionStorage.getItem('authenticated');
    auth === 'true' ? setAuthenticated(true) : setAuthenticated(false);

    const user = JSON.parse(sessionStorage.getItem('user'));
    if (user !== null && user !== undefined) {
      setUser(user);
    }
  }, []);

  return (
    <div className='topnav'>
      {authenticated ? (
        <>
          <Link to={`/subscription/${user.enterpriseId}`}>
            <div className='align-icon-left topmenu-center-button'>
              <RiExchangeDollarLine size='2.5em' color='white' />
            </div>
          </Link>
          <Link to={`/invoices/${user.enterpriseId}`}>
            <div className='align-icon-left topmenu-center-button'>
              <InvoiceSVG height='100%' width='100%' />
            </div>
          </Link>

          <Link to='/login'>
            <div
              className='align-icon-right topmenu-center-button'
              onClick={() => {
                sessionStorage.removeItem('user');
                sessionStorage.setItem('authenticated', false);
              }}
            >
              <ImExit size='2.5em' color='white' />
            </div>
          </Link>
        </>
      ) : (
        <></>
      )}
    </div>
  );
};

export default SAMAdminTopMenu;
