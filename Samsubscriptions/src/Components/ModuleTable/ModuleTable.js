import React, { useEffect, useState } from 'react';
import { GetModuleGroups } from '../../Services/ModuleGroupService';
import FirstTimePrice from '../FirstTimePrice/FirstTimePrice';
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner';
import SubscriptionModule from '../SubscriptionModule/SubscriptionModule';
import './ModuleTable.css';

const ModuleTable = ({
  subscriptionType,
  modules,
  setModules,
  editState,
  setSelectedModules,
  concurrentUsers,
  activeModules,
  selectedActivePriceMultipliers,
  subscriptionLoaded,
  moduleSettingValues,
  setModuleSettingValues,
  notPaidInactiveModules,
  disabled,
}) => {
  const [installationModules, setInstallationModules] = useState([]);
  const [camoModules, setCamoModules] = useState([]);
  const [mroModules, setMroModules] = useState([]);
  const [moduleGroups, setModuleGroups] = useState([]);
  const [showLoading, setShowLoading] = useState(false);

  //Get module groups
  useEffect(() => {
    setShowLoading(true);
    const getDate = async () => {
      const data = await GetModuleGroups();
      setModuleGroups(data);
      setShowLoading(false);
    };
    getDate();
  }, []);

  //Filter modules into groups
  useEffect(() => {
    setShowLoading(true);
    if (moduleGroups !== undefined && moduleGroups.length > 0) {
      moduleGroups.map((group) => {
        if (group.name === 'Installation') {
          setInstallationModules([
            ...modules.filter((item) => item.groupId === group.id),
          ]);
        }

        if (group.name === 'CAMO') {
          setCamoModules([
            ...modules.filter((item) => item.groupId === group.id),
          ]);
        }

        if (group.name === 'MRO') {
          setMroModules([
            ...modules.filter((item) => item.groupId === group.id),
          ]);
        }
        return '';
      });
    }
    setShowLoading(false);
  }, [modules]);

  const handleCheckboxChange = (checked, id) => {
    const foundModule = modules.find((item) => item.id === id);

    //Check if module is inactive but not paid
    const foundNotPaidInactiveModule = notPaidInactiveModules.find(
      (item) => item.moduleId === foundModule.id
    );

    //Set checked status
    if (foundModule !== undefined) {
      const index = modules.indexOf(foundModule);
      const newModules = [...modules];

      /*if (foundNotPaidInactiveModule !== undefined) {
        newModules[index].isChecked = true;
        return;
      }*/
      newModules[index].isChecked = checked;

      setModules(newModules);
    }
  };

  return (
    <div className='moduletable'>
      {/*showLoading ? (
        <div className='moduletable-loadingspinner'>
          <LoadingSpinner height='50px' width='50px' />
        </div>
      ) : (
        <></>
      )*/}
      <div>
        <label style={{ margin: '0px 10px 0px 10px' }}>Installation</label>
        {installationModules !== undefined && installationModules.length > 0 ? (
          installationModules.map((item, index) => {
            return (
              <div key={index}>
                <SubscriptionModule
                  module={item}
                  subscriptionType={subscriptionType}
                  editState={editState}
                  setSelectedModules={setSelectedModules}
                  concurrentUsers={concurrentUsers}
                  activeModules={activeModules}
                  selectedActivePriceMultipliers={
                    selectedActivePriceMultipliers
                  }
                  subscriptionLoaded={subscriptionLoaded}
                  moduleSettingValues={moduleSettingValues}
                  setModuleSettingValues={setModuleSettingValues}
                  change={handleCheckboxChange}
                  notPaidInactiveModules={notPaidInactiveModules}
                  setShowLoading={setShowLoading}
                  disabled={disabled}
                />
                {item.firsttimePrice > 0 ? (
                  <FirstTimePrice firstTimePrice={item.firsttimePrice} />
                ) : (
                  <></>
                )}
              </div>
            );
          })
        ) : (
          <></>
        )}
      </div>
      <div>
        <label style={{ margin: '0px 10px 0px 10px' }}>CAMO</label>
        {camoModules !== undefined && camoModules.length > 0 ? (
          camoModules.map((item, index) => {
            return (
              <div key={index}>
                <SubscriptionModule
                  module={item}
                  subscriptionType={subscriptionType}
                  editState={editState}
                  setSelectedModules={setSelectedModules}
                  concurrentUsers={concurrentUsers}
                  activeModules={activeModules}
                  selectedActivePriceMultipliers={
                    selectedActivePriceMultipliers
                  }
                  subscriptionLoaded={subscriptionLoaded}
                  moduleSettingValues={moduleSettingValues}
                  setModuleSettingValues={setModuleSettingValues}
                  moduleIndex={index}
                  change={handleCheckboxChange}
                  notPaidInactiveModules={notPaidInactiveModules}
                  setShowLoading={setShowLoading}
                  disabled={disabled}
                />
                {item.firsttimePrice > 0 ? (
                  <FirstTimePrice firstTimePrice={item.firsttimePrice} />
                ) : (
                  <></>
                )}
              </div>
            );
          })
        ) : (
          <></>
        )}
      </div>
      <div>
        <label style={{ margin: '0px 10px 0px 10px' }}>MRO</label>
        {mroModules !== undefined && mroModules.length > 0 ? (
          mroModules.map((item, index) => {
            return (
              <div key={index}>
                <SubscriptionModule
                  module={item}
                  subscriptionType={subscriptionType}
                  editState={editState}
                  setSelectedModules={setSelectedModules}
                  concurrentUsers={concurrentUsers}
                  activeModules={activeModules}
                  selectedActivePriceMultipliers={
                    selectedActivePriceMultipliers
                  }
                  subscriptionLoaded={subscriptionLoaded}
                  moduleSettingValues={moduleSettingValues}
                  setModuleSettingValues={setModuleSettingValues}
                  moduleIndex={index}
                  change={handleCheckboxChange}
                  notPaidInactiveModules={notPaidInactiveModules}
                  setShowLoading={setShowLoading}
                  disabled={disabled}
                />
                {item.firsttimePrice > 0 ? (
                  <FirstTimePrice firstTimePrice={item.firsttimePrice} />
                ) : (
                  <></>
                )}
              </div>
            );
          })
        ) : (
          <></>
        )}
      </div>
    </div>
  );
};

export default ModuleTable;
