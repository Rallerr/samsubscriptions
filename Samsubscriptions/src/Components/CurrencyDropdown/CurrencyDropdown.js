import React from 'react';

const CurrencyDropdown = ({
  onChange,
  value,
  items,
  id,
  className,
  disabled,
}) => {
  return (
    <select
      id={id}
      className={className === undefined ? 'dropdown' : className}
      value={value}
      onChange={(e) => onChange(e.target.value)}
      disabled={disabled}
    >
      {items !== undefined && items.length > 0 ? (
        items.map((item, index) => {
          return (
            <option data-rate={item.rate} value={item.id} key={index}>
              {item.currencyName}
            </option>
          );
        })
      ) : (
        <></>
      )}
    </select>
  );
};

export default CurrencyDropdown;
