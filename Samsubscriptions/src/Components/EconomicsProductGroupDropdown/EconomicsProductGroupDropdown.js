import React, { useEffect, useState } from 'react';
import { GetProductGroups } from '../../Services/EconomicsServices/EconomicsProductGroupService';
import DropdownItem from '../DropdownItem/DropdownItem';

const EconomicsProductGroupDropdown = ({
  onChange,
  value,
  id,
  className,
  editState,
  setShowLoading,
  disabled,
}) => {
  const [productGroups, setProductGroups] = useState([]);

  useEffect(() => {
    const getData = async () => {
      setShowLoading(true);
      const data = await GetProductGroups();

      if (data !== undefined && data.length > 0) {
        setProductGroups(data);
        onChange(data[0].productGroupNumber);
      }
      setShowLoading(false);
    };
    getData();
  }, []);

  useEffect(() => {
    if (
      productGroups !== undefined &&
      productGroups.length > 0 &&
      editState === 'NEW_MODULE'
    ) {
      onChange(productGroups[0].productGroupNumber);
    }
  }, [editState]);

  return (
    <select
      className={className === undefined ? 'dropdown' : className}
      value={value}
      onChange={(e) => onChange(e.target.value)}
      id={id}
      disabled={disabled}
    >
      {productGroups !== undefined && productGroups.length > 0 ? (
        productGroups.map((item, index) => {
          return (
            <DropdownItem
              key={index}
              value={item.productGroupNumber}
              id={item.productGroupNumber}
              label={item.name}
            />
          );
        })
      ) : (
        <></>
      )}
    </select>
  );
};

export default EconomicsProductGroupDropdown;
