import React from 'react';
import MultipleInvoicesSVG from '../SvgIcons/MultipleInvoicesSVG';
import './GenerateMultipleInvoicesButton.css';

const GenerateMultipleInvoicesButton = ({ disabled, onClick, id }) => {
  return (
    <button
      title='Generate multiple invoices'
      className='generate-multiple-invoices-button'
      disabled={disabled}
      id={id}
      onClick={(e) => onClick(e)}
    >
      <MultipleInvoicesSVG />
    </button>
  );
};

export default GenerateMultipleInvoicesButton;
