import React from 'react';
import './LoginForm.css';
import { useState } from 'react';
import axios from 'axios';
import { Redirect } from 'react-router-dom';
import { baseUrl } from '../../ApiUrl';
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner';
import Role from '../../Enums/Role';

const LoginForm = ({ authenticated, setAuthenticated, user, setUser }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [showErrorMsg, setShowErrorMsg] = useState(false);
  const [isUserValid, setIsUserValid] = useState(false);
  const [showLoading, setShowLoading] = useState(false);

  const handleSubmit = async (e) => {
    e.preventDefault();
    await login();
  };

  //Login user
  const login = async () => {
    setShowLoading(true);
    try {
      const response = await axios.post(`${baseUrl}Users/authenticate`, {
        username: username,
        password: password,
      });

      response.status === 200 ? setShowErrorMsg(false) : setShowErrorMsg(true);
      if (response.status === 200) {
        sessionStorage.setItem('user', JSON.stringify(response.data));
        sessionStorage.setItem('authenticated', true);
        setUser(response.data);
        setAuthenticated(true);
        setIsUserValid(true);
        setShowLoading(false);
      }

      console.log(response);
    } catch (err) {
      setShowErrorMsg(true);
      setShowLoading(false);
      console.log(err);
    }
  };

  if (isUserValid && user.role === Role.Admin) {
    return <Redirect to='/enterprise' />;
  }

  if (isUserValid && user.role === Role.SAMAdmin) {
    return <Redirect to={`/subscription/${user.enterpriseId}`} />;
  }

  return (
    <div className='login-container'>
      <h1 className='login-header'>SAMSubscriptions</h1>
      <form onSubmit={handleSubmit}>
        <div className='input-container'>
          <p className='login-error-msg'>
            {showErrorMsg ? 'Wrong username or password' : ''}
          </p>
          <input
            className='login-input'
            type='text'
            name='username'
            placeholder='Username'
            onChange={(e) => setUsername(e.target.value)}
            value={username}
          />
          <input
            className='login-input'
            type='password'
            name='password'
            placeholder='Password'
            onChange={(e) => setPassword(e.target.value)}
            value={password}
          />
          {showLoading ? (
            <div className='login-loadingspinner'>
              <LoadingSpinner height='50px' width='50px' />
            </div>
          ) : (
            <></>
          )}
        </div>
        <input className='login-button' type='submit' value='Login' />
      </form>
    </div>
  );
};

export default LoginForm;
