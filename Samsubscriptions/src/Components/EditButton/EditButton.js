import React from 'react';
import { FiEdit } from 'react-icons/fi';
import './EditButton.css';

const EditButton = ({ disabled, onClick, id }) => {
  return (
    <button
      className='edit-button'
      disabled={disabled}
      id={id}
      onClick={(e) => onClick(e)}
    >
      <FiEdit color='white' size='2em' />
    </button>
  );
};

export default EditButton;
