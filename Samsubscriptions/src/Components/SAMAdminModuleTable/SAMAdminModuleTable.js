import React, { useEffect, useState } from 'react';
import { GetModuleGroups } from '../../Services/ModuleGroupService';
import { GetSAMModules } from '../../Services/ModuleService';
import FirstTimePrice from '../FirstTimePrice/FirstTimePrice';
import SubscriptionModule from '../SubscriptionModule/SubscriptionModule';
import './SAMAdminModuleTable.css';

const SAMAdminModuleTable = ({
  samModules,
  setSamModules,
  activeModules,
  setActiveModules,
  subscription,
  editState,
  concurrentUsers,
  activePriceMultipliers,
  subscriptionType,
  moduleSettingValues,
  setModuleSettingValues,
  subscriptionLoaded,
  setSubscriptionLoaded,
  disabled,
  setShowLoading,
  showLoading,
  setSelectedModules,
}) => {
  const [camoModules, setCamoModules] = useState([]);
  const [mroModules, setMroModules] = useState([]);
  const [moduleGroups, setModuleGroups] = useState([]);
  const [load, setLoad] = useState(false);

  useEffect(() => {
    const getData = async () => {
      setLoad(true);
      //Get all SAM modules
      var moduleList = await GetSAMModules();
      if (moduleList !== undefined && moduleList.length > 0) {
        moduleList.map((item) => {
          item.isChecked = false;
          return '';
        });
        setSamModules(moduleList);
      }

      //Get all module groups
      var moduleGroupList = await GetModuleGroups();
      if (moduleGroupList !== undefined && moduleGroupList.length > 0) {
        setModuleGroups(moduleGroupList);
      }
      setSubscriptionLoaded(true);
      setLoad(false);
    };
    getData();
  }, []);

  useEffect(() => {
    //Filter modules into groups.
    setLoad(true);
    if (subscriptionLoaded) {
      if (moduleGroups !== undefined && moduleGroups.length > 0) {
        moduleGroups.map((group) => {
          if (group.name === 'CAMO') {
            setCamoModules([
              ...samModules.filter((item) => item.groupId === group.id),
            ]);
          }

          if (group.name === 'MRO') {
            setMroModules([
              ...samModules.filter((item) => item.groupId === group.id),
            ]);
          }
          return '';
        });
      }
    }
    setLoad(false);
  }, [moduleGroups, subscriptionLoaded]);

  const handleCheckboxChange = (checked, id) => {
    const foundModule = samModules.find((item) => item.id === id);

    //Set checked status
    if (foundModule !== undefined) {
      const index = samModules.indexOf(foundModule);
      const newSamModules = [...samModules];
      newSamModules[index].isChecked = checked;

      setSamModules(newSamModules);
    }
  };

  return (
    <div className='samadmin-moduletable'>
      {load ? (
        <></>
      ) : (
        <>
          <div>
            <label>CAMO</label>
            {camoModules.map((item, index) => {
              return (
                <div key={index}>
                  <SubscriptionModule
                    module={item}
                    subscriptionType={subscriptionType}
                    editState={editState}
                    setSelectedModules={setSelectedModules}
                    concurrentUsers={concurrentUsers}
                    activeModules={activeModules}
                    selectedActivePriceMultipliers={activePriceMultipliers}
                    subscriptionLoaded={subscriptionLoaded}
                    moduleSettingValues={moduleSettingValues}
                    setModuleSettingValues={setModuleSettingValues}
                    change={handleCheckboxChange}
                    setShowLoading={setShowLoading}
                    disabled={disabled}
                  />
                  {item.firsttimePrice > 0 ? (
                    <FirstTimePrice firstTimePrice={item.firsttimePrice} />
                  ) : (
                    <></>
                  )}
                </div>
              );
            })}
          </div>
          <div>
            <label>MRO</label>
            {mroModules.map((item, index) => {
              return (
                <div key={index}>
                  <SubscriptionModule
                    module={item}
                    subscriptionType={subscriptionType}
                    editState={editState}
                    setSelectedModules={setSelectedModules}
                    concurrentUsers={concurrentUsers}
                    activeModules={activeModules}
                    selectedActivePriceMultipliers={activePriceMultipliers}
                    subscriptionLoaded={subscriptionLoaded}
                    moduleSettingValues={moduleSettingValues}
                    setModuleSettingValues={setModuleSettingValues}
                    change={handleCheckboxChange}
                    setShowLoading={setShowLoading}
                    disabled={disabled}
                  />
                  {item.firsttimePrice > 0 ? (
                    <FirstTimePrice firstTimePrice={item.firsttimePrice} />
                  ) : (
                    <></>
                  )}
                </div>
              );
            })}
          </div>
        </>
      )}
    </div>
  );
};

export default SAMAdminModuleTable;
