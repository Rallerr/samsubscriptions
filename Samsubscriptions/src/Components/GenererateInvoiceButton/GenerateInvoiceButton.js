import React from 'react';
import { FaFileInvoiceDollar } from 'react-icons/fa';
import InvoiceSVG from '../SvgIcons/InvoiceSVG';

import './GenerateInvoiceButton.css';

const GenerateInvoiceButton = ({ disabled, onClick, id }) => {
  return (
    <button
      title='Generate invoice'
      className='generate-invoice-button'
      disabled={disabled}
      id={id}
      onClick={(e) => onClick(e)}
    >
      <InvoiceSVG height='100%' width='100%' />
    </button>
  );
};

export default GenerateInvoiceButton;
