import React, { useState } from 'react';
import { AiOutlineSave } from 'react-icons/ai';
import { ImCancelCircle } from 'react-icons/im';
import './NewModuleSetting.css';

const NewModuleSetting = ({
  moduleSettings,
  setModuleSettings,
  setShowNewModuleSetting,
}) => {
  const [name, setName] = useState('');
  const [type, setType] = useState('number');

  const handleCancelOnClick = () => {
    setShowNewModuleSetting(false);
  };
  const handleSaveOnClick = () => {
    const moduleSetting = {
      name: name,
      type: type,
    };

    const settingExists = moduleSettings.find(
      (element) => element.name === name
    );

    if (settingExists === undefined) {
      setModuleSettings((prevArr) => [...prevArr, moduleSetting]);
    } else {
      alert('Setting already exists');
      return;
    }

    setShowNewModuleSetting(false);
  };

  return (
    <div className='modal' onClick={handleCancelOnClick}>
      <div
        className='new-setting-container'
        onClick={(e) => e.stopPropagation()}
      >
        <div className='new-setting-info'>
          <div className='new-setting-name-container'>
            <label htmlFor='newSettingName'>Name</label>
            <input
              type='text'
              value={name}
              onChange={(e) => setName(e.target.value)}
              className='inputfield'
              id='newSettingName'
            />
          </div>
          <div className='new-setting-type-container'>
            <label htmlFor='newSettingType'>Type</label>
            <select
              className='dropdown'
              onChange={(e) => setType(e.target.value)}
            >
              <option value={type}>Number</option>
              <option value='text'>Text</option>
            </select>
          </div>
        </div>
        <div className='new-setting-buttons'>
          <button
            className='save-new-modulesetting'
            onClick={handleSaveOnClick}
          >
            <AiOutlineSave size='1.7em' color='white' />
          </button>
          <button
            className='undo-new-modulesetting'
            onClick={handleCancelOnClick}
          >
            <ImCancelCircle size='1.5em' color='white' />
          </button>
        </div>
      </div>
    </div>
  );
};

export default NewModuleSetting;
