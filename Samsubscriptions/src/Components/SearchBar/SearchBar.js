import React from "react";
import './SearchBar.css'

const SearchBar = ({ value, onChange }) => {

    return (
        <input className="sidepanel-search" type="text" name="search" placeholder="Search..."
            onChange={onChange}
            value={value} />
    );
}

export default SearchBar;