import React, { useState } from 'react';
import SidePanelItem from '../SidePanelItem/SidePanelItem';
import SearchBar from '../SearchBar/SearchBar';
import './SidePanel.css';

const SidePanel = ({ menuItems, onItemClick }) => {
  const [searchValue, setSearchValue] = useState('');

  const generateMenuItem = () => {
    return menuItems !== undefined ? (
      menuItems
        .filter((item) => {
          if (searchValue === '') {
            return item;
          } else if (
            item.name.toLowerCase().includes(searchValue.toLowerCase())
          ) {
            return item;
          }
          return null;
        })
        .map((item, index) => {
          return (
            <SidePanelItem
              onClick={(e) => onItemClick(e.target.id)}
              title={item.name}
              dataId={item.id}
              key={index}
            />
          );
        })
    ) : (
      <></>
    );
  };
  return (
    <div className='sidepanel-container'>
      <SearchBar
        onChange={(e) => setSearchValue(e.target.value)}
        value={searchValue}
      />
      <div className='menuitems-container'>{generateMenuItem()}</div>
    </div>
  );
};

export default SidePanel;
