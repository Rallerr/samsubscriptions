const Role = {
  Admin: 'Admin',
  SAMAdmin: 'SAMAdmin',
  User: 'User',
};

export default Role;
