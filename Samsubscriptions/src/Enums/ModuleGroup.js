const ModuleGroup = {
  Installation: 'Installation',
  CAMO: 'CAMO',
  MRO: 'MRO',
};

export default ModuleGroup;
