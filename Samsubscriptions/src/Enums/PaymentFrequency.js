const PaymentFrequency = {
  Onetime: 'Onetime',
  Monthly: 'Monthly',
};

export default PaymentFrequency;
