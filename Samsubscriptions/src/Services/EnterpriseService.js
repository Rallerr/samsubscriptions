import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetEnterprises = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}enterprise/`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });
    return response.data;
  } catch (error) {
    console.log(error);
    return [];
  }
};

const GetEnterprisesWithSubscription = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}enterprise/subscription`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });
    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetEnterprisesFromHubspot = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}Hubspot/enterprise`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });
    console.log(response);
    const enterprises = [];
    response.data.results.map((item) => {
      return enterprises.push({
        id: item.id,
        name: item.properties.name,
      });
    });

    return enterprises;
  } catch (error) {
    console.log(error);
    return [];
  }
};

const GetEnterpriseByName = async (enterpriseName) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}enterprise/name/${enterpriseName}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );
    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetEnterpriseById = async (enterpriseId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}enterprise/${enterpriseId}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });
    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetEnterpriseByNo = async (enterpriseNo) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}enterprise/enterpriseno/${enterpriseNo}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetEnterpriseInfoByIdFromHubspot = async (enterpriseId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}Hubspot/enterprise/${enterpriseId}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    const enterpriseInfo = {
      companyId: response.data.companyId,
      name: response.data.properties.name.value,
      //invoiceEmail: response.data.properties.invoiceEmail.value,
      phone: response.data.properties.phone.value,
      address: response.data.properties.address.value,
      zip: response.data.properties.zip.value,
      city: response.data.properties.city.value,
      country: response.data.properties.country.value,
      //adminName: response.data.properties.adminName.value,
      //adminEmail: response.data.properties.adminEmail.value,
    };

    return enterpriseInfo;
  } catch (err) {
    console.log(err);
  }
};

const CreateEnterprise = async (enterprise) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(
      `https://localhost:44321/api/Enterprise`,
      enterprise,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );
    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export {
  GetEnterprises,
  GetEnterprisesWithSubscription,
  GetEnterprisesFromHubspot,
  GetEnterpriseByName,
  GetEnterpriseInfoByIdFromHubspot,
  GetEnterpriseByNo,
  GetEnterpriseById,
  CreateEnterprise,
};
