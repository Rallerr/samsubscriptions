import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const ValidateToken = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}Users/validate/${user.token}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });
    console.log(response);

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { ValidateToken };
