import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetAllModuleSettings = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}modulesetting`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetModuleSettingById = async (settingId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}moduleSetting/${settingId}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetModuleSettingsByModule = async (moduleId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}ModuleSetting/moduleId/${moduleId}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const CreateModuleSetting = async (moduleSetting) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(
      `${baseUrl}modulesetting/`,
      moduleSetting,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetActiveModuleSettingValue = async (moduleSettingId, activeModuleId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}moduleSettingValue/active/${moduleSettingId}/${activeModuleId}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetActiveModuleSettingValuesByActiveModuleId = async (activeModuleId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}modulesettingvalue/active/${activeModuleId}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const CreateModuleSettingValue = async (moduleSettingValue) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = axios.post(
      `${baseUrl}moduleSettingValue`,
      moduleSettingValue,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response;
  } catch (err) {}
};

const UpdateModuleSettingValue = async (moduleSettingValue) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.put(
      `${baseUrl}moduleSettingValue/${moduleSettingValue.Id}`,
      moduleSettingValue,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response;
  } catch (err) {
    console.log(err);
  }
};

const DeleteModuleSettingValue = async (moduleSettingValueId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = axios.delete(
      `${baseUrl}moduleSettingValue/${moduleSettingValueId}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response;
  } catch (err) {
    console.log(err);
  }
};

export {
  GetAllModuleSettings,
  GetModuleSettingById,
  GetModuleSettingsByModule,
  CreateModuleSetting,
  UpdateModuleSettingValue,
  GetActiveModuleSettingValue,
  GetActiveModuleSettingValuesByActiveModuleId,
  DeleteModuleSettingValue,
  CreateModuleSettingValue,
};
