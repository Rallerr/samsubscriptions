import axios from 'axios';
import { baseUrl } from '../../ApiUrl';

const DownloadEconomicsBookedInvoice = async (invoiceNumber) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}economicsBookedInvoice/download/${invoiceNumber}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetEconomicsBookedInvoices = async (customerNumber) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}economicsBookedInvoice/customerNumber/${customerNumber}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { DownloadEconomicsBookedInvoice, GetEconomicsBookedInvoices };
