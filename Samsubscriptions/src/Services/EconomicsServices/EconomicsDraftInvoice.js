import axios from 'axios';
import { baseUrl } from '../../ApiUrl';

const GenerateEconomicsDraftInvoice = async (enterpriseId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(
      `${baseUrl}economicsDraftInvoice/${enterpriseId}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { GenerateEconomicsDraftInvoice };
