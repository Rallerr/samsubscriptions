import axios from 'axios';
import { baseUrl } from '../../ApiUrl';

const CreateEconomicsProduct = async (product) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(`${baseUrl}economicsProduct/`, product, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetEconomicsProductById = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}economicsProduct/${id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const UpdateEconomicsProduct = async (product) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.put(
      `https://localhost:44321/api/EconomicsProduct/${product.productNumber}`,
      product,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export {
  CreateEconomicsProduct,
  GetEconomicsProductById,
  UpdateEconomicsProduct,
};
