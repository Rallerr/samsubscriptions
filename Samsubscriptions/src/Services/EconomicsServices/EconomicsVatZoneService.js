import axios from 'axios';
import { baseUrl } from '../../ApiUrl';

const GetEconomicsVatZones = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}economicsVatZone/`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { GetEconomicsVatZones };
