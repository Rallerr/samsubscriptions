import axios from 'axios';
import { baseUrl } from '../../ApiUrl';

const GetEconomicsCustomerById = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}economicsCustomer/id/${id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const CreateEconomicsCustomer = async (economicsCustomer) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(
      `${baseUrl}economicsCustomer/`,
      economicsCustomer,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const UpdateEconomicsCustomer = async (customer) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.put(
      `${baseUrl}EconomicsCustomer/${customer.customerNumber}
      `,
      customer,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export {
  GetEconomicsCustomerById,
  CreateEconomicsCustomer,
  UpdateEconomicsCustomer,
};
