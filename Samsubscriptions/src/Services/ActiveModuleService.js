import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetActiveModulesBySubscription = async (subscriptionId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}activeModule/subscriptionId/${subscriptionId}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { GetActiveModulesBySubscription };
