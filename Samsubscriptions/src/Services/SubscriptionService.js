import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetSubscriptionByEnterprise = async (enterpriseId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}subscription/enterprise/${enterpriseId}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const UpdateSubscription = async (subscription) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.put(
      `${baseUrl}subscription/${subscription.id}`,
      subscription,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    console.log(response);
    return response;
  } catch (err) {
    console.log(err);
  }
};

const CreateSubscription = async (subscription) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(`${baseUrl}subscription/`, subscription, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { GetSubscriptionByEnterprise, UpdateSubscription, CreateSubscription };
