import axios from 'axios';
import { baseUrl } from '../ApiUrl';

//Active concurrent users
const GetActiveConcurrentUsersBySubscriptionId = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}activeConcurrentUsers/subscriptionId/${id}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetActiveConcurrentUsersById = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}ActiveConcurrentUsers/${id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const CreateActiveConcurrentUsers = async (activeUsers) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(
      `${baseUrl}activeConcurrentUsers/`,
      activeUsers,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response;
  } catch (err) {
    console.log(err);
  }
};

const DeleteActiveConcurrentUsers = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.delete(
      `${baseUrl}activeConcurrentUsers/${id}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response;
  } catch (err) {
    console.log(err);
  }
};

const PutConcurrentUsers = async (concurrentUsers) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.put(
      `${baseUrl}concurrentUsers/${concurrentUsers.Id}`,
      concurrentUsers,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    console.log(response);

    return response;
  } catch (err) {
    console.log(err);
  }
};

//Inactive concurrent users

const CreateInactiveConcurrentUsers = async (inactiveCCUsers) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(
      `${baseUrl}inactiveConcurrentUsers/`,
      inactiveCCUsers,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export {
  GetActiveConcurrentUsersBySubscriptionId,
  GetActiveConcurrentUsersById,
  CreateActiveConcurrentUsers,
  DeleteActiveConcurrentUsers,
  PutConcurrentUsers,
  CreateInactiveConcurrentUsers,
};
