import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetRequiredModulesByModuleId = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}requiredModule/moduleId/${id}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const CreateRequiredModule = async (requiredModule) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(
      `${baseUrl}requiredModule/`,
      requiredModule,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { GetRequiredModulesByModuleId, CreateRequiredModule };
