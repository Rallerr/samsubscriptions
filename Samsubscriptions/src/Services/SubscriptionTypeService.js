import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetSubscriptionTypes = async () => {
  const user = JSON.parse(sessionStorage.getItem('user'));
  try {
    const response = await axios.get(`${baseUrl}subscriptiontype/`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
    if (err.toString().includes('401')) {
      return [{ status: 401 }];
    }
    return [{}];
  }
};

const GetSubscriptionTypeById = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}subscriptiontype/${id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { GetSubscriptionTypes, GetSubscriptionTypeById };
