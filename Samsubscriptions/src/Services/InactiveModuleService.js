import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetNotPaidInactiveModules = async (subscriptionId) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}inactiveModule/notpaid/${subscriptionId}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { GetNotPaidInactiveModules };
