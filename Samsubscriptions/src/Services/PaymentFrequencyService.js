import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetPaymentFrequencies = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}paymentFrequency`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetPaymentFrequencyById = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}paymentfrequency/${id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });
    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { GetPaymentFrequencyById, GetPaymentFrequencies };
