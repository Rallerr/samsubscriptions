import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetBillingPeriods = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}billingperiod/`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
    return [];
  }
};

export { GetBillingPeriods };
