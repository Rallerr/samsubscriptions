import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetPriceMultiplier = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}PriceMultiplier/`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetPriceMultiplierById = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));

    const response = await axios.get(`${baseUrl}priceMultiplier/${id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetActivePriceMultipliersBySubscription = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(
      `${baseUrl}activepricemultiplier/subscriptionId/${id}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const CreateActivePriceMultiplier = async (activePriceMultiplier) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(
      `${baseUrl}activePriceMultiplier/`,
      activePriceMultiplier,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const DeleteActivePriceMultiplier = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.delete(
      `${baseUrl}activePriceMultiplier/${id}`,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response;
  } catch (err) {
    console.log(err);
  }
};

const CreateInactivePriceMultiplier = async (inactivePriceMultiplier) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(
      `${baseUrl}inactivePriceMultiplier/`,
      inactivePriceMultiplier,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export {
  GetPriceMultiplier,
  GetPriceMultiplierById,
  GetActivePriceMultipliersBySubscription,
  CreateActivePriceMultiplier,
  CreateInactivePriceMultiplier,
  DeleteActivePriceMultiplier,
};
