import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetModules = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}module/`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {}
};

const GetModuleById = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}Module/${id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });
    console.log(response);
    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetSAMModules = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));

    const response = await axios.get(`${baseUrl}module/samModules/`, {
      headers: {
        Authentication: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {}
};

const CreateModule = async (module) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(`${baseUrl}module/`, module, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const CreateActiveModule = async (activeModule) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(`${baseUrl}activemodule`, activeModule, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const CreateInactiveModule = async (inactiveModule) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.post(
      `${baseUrl}inactiveModule/`,
      inactiveModule,
      {
        headers: {
          Authorization: `Bearer ${user.token}`,
        },
      }
    );

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const DeleteActiveModule = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.delete(`${baseUrl}activeModule/${id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });
    console.log(response);
    return response;
  } catch (err) {
    console.log(err);
  }
};

const UpdateModule = async (module) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.put(`${baseUrl}module/${module.id}`, module, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response;
  } catch (err) {
    console.log(err);
  }
};

export {
  GetModules,
  GetModuleById,
  GetSAMModules,
  CreateActiveModule,
  CreateInactiveModule,
  DeleteActiveModule,
  UpdateModule,
  CreateModule,
};
