import axios from 'axios';
import { baseUrl } from '../ApiUrl';

const GetCurrencies = async () => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}currency/`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });

    return response.data;
  } catch (err) {
    console.log(err);
  }
};

const GetCurrencyById = async (id) => {
  try {
    const user = JSON.parse(sessionStorage.getItem('user'));
    const response = await axios.get(`${baseUrl}currency/${id}`, {
      headers: {
        Authorization: `Bearer ${user.token}`,
      },
    });
    return response.data;
  } catch (err) {
    console.log(err);
  }
};

export { GetCurrencies, GetCurrencyById };
