import './App.css';
import TopMenu from './Components/TopMenu/TopMenu';
import LoginPage from './Pages/LoginPage/LoginPage';
import EnterprisePage from './Pages/EnterprisePage/EnterprisePage';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ProtectedRoute from './Components/ProtectedRoute/ProtectedRoute';
import SubscriptionPage from './Pages/SubscriptionPage/SubscriptionPage';
import { useEffect, useState } from 'react';
import ModulePage from './Pages/ModulePage/ModulePage';
import InvoicePage from './Pages/InvoicePage/InvoicePage';
import Role from './Enums/Role';
import SAMAdminSubscriptionPage from './Pages/SAMAdminSubscriptionPage/SAMAdminSubscriptionPage';
import SAMAdminTopMenu from './Components/TopMenu/SAMAdminTopMenu';
import SAMAdminInvoicePage from './Pages/SAMAdminInvoicePage/SAMAdminInvoicePage';

//Router https://www.youtube.com/watch?v=Law7wfdg_ls
function App() {
  const [authenticated, setAuthenticated] = useState(false);
  const [user, setUser] = useState({ role: '' });
  const [editState, setEditState] = useState();

  useEffect(() => {
    const userInfo = JSON.parse(sessionStorage.getItem('user'));

    if (userInfo !== null && userInfo !== undefined) {
      setUser(userInfo);
    }
  }, []);

  return (
    <div className='App'>
      <div className='main-grid-container'>
        <Router>
          <div className='navbar'>
            {user.role === Role.Admin ? (
              <TopMenu setEditState={setEditState} />
            ) : (
              <SAMAdminTopMenu />
            )}
          </div>
          <div className='main-content'>
            <Switch>
              <ProtectedRoute
                roles={[Role.Admin]}
                path='/'
                authenticated={authenticated}
                exact
                children={
                  <EnterprisePage
                    authenticated={authenticated}
                    setAuthenticated={setAuthenticated}
                  />
                }
              ></ProtectedRoute>
              <ProtectedRoute
                roles={[Role.Admin]}
                path='/enterprise'
                authenticated={authenticated}
                children={
                  <EnterprisePage
                    authenticated={authenticated}
                    setAuthenticated={setAuthenticated}
                  />
                }
              ></ProtectedRoute>
              <Route path='/login'>
                <LoginPage
                  user={user}
                  setUser={setUser}
                  authenticated={authenticated}
                  setAuthenticated={setAuthenticated}
                />
              </Route>
              <ProtectedRoute
                exact
                roles={[Role.Admin]}
                path='/subscription'
                authenticated={authenticated}
                children={
                  <SubscriptionPage
                    authenticated={authenticated}
                    setAuthenticated={setAuthenticated}
                    editState={editState}
                    setEditState={setEditState}
                  />
                }
              ></ProtectedRoute>
              <ProtectedRoute
                roles={[Role.Admin, Role.SAMAdmin]}
                path='/subscription/:enterpriseId'
                authenticated={authenticated}
                children={
                  <SAMAdminSubscriptionPage
                    setAuthenticated={setAuthenticated}
                    editState={editState}
                    setEditState={setEditState}
                  />
                }
              />
              <ProtectedRoute
                path='/module'
                exact
                roles={[Role.Admin]}
                authenticated={authenticated}
                children={
                  <ModulePage
                    authenticated={authenticated}
                    setAuthenticated={setAuthenticated}
                    editState={editState}
                    setEditState={setEditState}
                  />
                }
              />
              <ProtectedRoute
                path='/invoices'
                exact
                roles={[Role.Admin]}
                authenticated={authenticated}
                children={<InvoicePage setAuthenticated={setAuthenticated} />}
              />
              <ProtectedRoute
                roles={[Role.Admin, Role.SAMAdmin]}
                path='/invoices/:enterpriseId'
                authenticated={authenticated}
                children={
                  <SAMAdminInvoicePage setAuthenticated={setAuthenticated} />
                }
              />
            </Switch>
          </div>
        </Router>
      </div>
    </div>
  );
}

export default App;
