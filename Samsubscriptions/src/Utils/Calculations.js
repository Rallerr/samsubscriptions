import ModuleGroup from '../Enums/ModuleGroup';
import PaymentFrequency from '../Enums/PaymentFrequency';
import SubscriptionType from '../Enums/SubscriptionType';

const CalcModulePrice = (
  module,
  activeModule,
  subscriptionType,
  camoUsers,
  MROUsers,
  priceMultipliers,
  moduleSettings,
  moduleGroup,
  basePrice
) => {
  let totalModulePrice = parseFloat(basePrice);

  if (subscriptionType === SubscriptionType.ConcurrentUsers) {
    //Multiply moduleprice with amount of users

    switch (moduleGroup) {
      case ModuleGroup.Installation:
        const totalUsers = parseInt(camoUsers) + parseInt(MROUsers);
        totalModulePrice = totalModulePrice * totalUsers;
        break;
      case ModuleGroup.CAMO:
        totalModulePrice = totalModulePrice * parseInt(camoUsers);
        break;
      case ModuleGroup.MRO:
        totalModulePrice = totalModulePrice * parseInt(MROUsers);
        break;
      default:
        break;
    }

    //Add price multipliers to module price
    for (const multiplier of priceMultipliers) {
      multiplier.factor !== undefined
        ? (totalModulePrice = totalModulePrice * multiplier.factor)
        : (totalModulePrice = totalModulePrice * multiplier.value);
    }

    //Add settings to module price
    for (const setting of moduleSettings) {
      if (setting.moduleId === module.id) {
        if (!Number.isNaN(setting.value)) {
          totalModulePrice = totalModulePrice * parseFloat(setting.value);
        }
      }
    }
  }

  //console.log('price', module.name, totalModulePrice);

  return totalModulePrice === 0 || Number.isNaN(totalModulePrice)
    ? basePrice
    : totalModulePrice;
};

const CalculateStartUpPrice = (selectedModules, activeModules, discount) => {
  let totalStartupPrice = 0;
  selectedModules.map((item) => {
    const activeModule = activeModules.find(
      (module) => module.moduleId === item.id
    );

    if (item.dataset.paymentfrequency === PaymentFrequency.Onetime) {
      totalStartupPrice =
        totalStartupPrice + parseFloat(item.attributes.value.value);
    }

    if (activeModule === undefined) {
      totalStartupPrice =
        totalStartupPrice + parseFloat(item.dataset.firsttimeprice);
    } else if (activeModule.firsttimePayment === true) {
      totalStartupPrice =
        totalStartupPrice + parseFloat(item.dataset.firsttimeprice);
    }
    return '';
  });

  //Substract discount from total price
  if (discount > 0) {
    const discountFactor = 1 - parseFloat(discount) / 100;
    totalStartupPrice = totalStartupPrice * discountFactor;
  }

  return totalStartupPrice;
};

const CalculateMonthlyPrice = (selectedModules, discount) => {
  let totalMonthlyPrice = 0;

  selectedModules.map((item) => {
    if (item.dataset.paymentfrequency === PaymentFrequency.Monthly) {
      totalMonthlyPrice =
        parseFloat(totalMonthlyPrice) + parseFloat(item.attributes.value.value);
    }
    return '';
  });

  //Substract discount from total price
  if (discount > 0) {
    const discountFactor = 1 - parseFloat(discount) / 100;
    totalMonthlyPrice = totalMonthlyPrice * discountFactor;
  }

  return totalMonthlyPrice;
};

const CalculateNextMonthlyCCUsersBill = (startUpPrice, monthlyPrice) => {
  let firstYearPrice = 0;

  firstYearPrice = parseFloat(firstYearPrice) + parseFloat(startUpPrice);

  firstYearPrice = parseFloat(firstYearPrice) + parseFloat(monthlyPrice);

  return firstYearPrice;
};

//const CalculateNextBillPayment

export {
  CalcModulePrice,
  CalculateStartUpPrice,
  CalculateMonthlyPrice,
  CalculateNextMonthlyCCUsersBill,
};
