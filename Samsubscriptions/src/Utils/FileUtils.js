const downloadPDF = (base64, fileName) => {
  const linkSource = `data:application/pdf;base64,${base64}`;
  const downloadLink = document.createElement('a');
  downloadLink.href = linkSource;
  downloadLink.download = fileName + '.pdf';
  downloadLink.click();
};

export { downloadPDF };
