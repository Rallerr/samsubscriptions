import {
  CreateActiveConcurrentUsers,
  CreateInactiveConcurrentUsers,
  DeleteActiveConcurrentUsers,
  GetActiveConcurrentUsersBySubscriptionId,
} from '../Services/ConcurrentUsersService';
import {
  CreateActiveModule,
  CreateInactiveModule,
  DeleteActiveModule,
} from '../Services/ModuleService';
import { GetLastDayOfMonth } from './DateUtils';

const UpdateConcurrentUsers = async (concurrentUsers, subscriptionId) => {
  const prevActiveUsers = await GetActiveConcurrentUsersBySubscriptionId(
    subscriptionId
  );

  //Create new active and set prev users to inactive if users has changed
  if (
    prevActiveUsers.camo !== concurrentUsers.camo ||
    prevActiveUsers.mro !== concurrentUsers.mro
  ) {
    const newInactiveUsers = {
      enterpriseId: prevActiveUsers.enterpriseId,
      camo: prevActiveUsers.camo,
      mro: prevActiveUsers.mro,
      start:
        prevActiveUsers.lastPaid === null
          ? prevActiveUsers.start
          : prevActiveUsers.lastPaid,
      expiration: prevActiveUsers.nextPayment,
      paid: prevActiveUsers.lastPaid === null ? false : true,
      subscriptionId: subscriptionId,
    };

    await CreateInactiveConcurrentUsers(newInactiveUsers);

    await DeleteActiveConcurrentUsers(prevActiveUsers.id);

    const today = new Date();
    const lastDayOfMonth = new Date(
      today.getFullYear(),
      today.getMonth() + 1,
      0
    );

    const newActiveUsers = {
      enterpriseId: concurrentUsers.enterpriseId,
      camo: concurrentUsers.camo,
      mro: concurrentUsers.mro,
      start: new Date(),
      nextPayment: lastDayOfMonth,
      subscriptionId: subscriptionId,
    };
    await CreateActiveConcurrentUsers(newActiveUsers);
  }
};

const UpdateModules = async (modules, activeModules, subscription) => {
  modules.map(async (item) => {
    if (item.isChecked === false) {
      const activeModule = activeModules.find((m) => m.moduleId === item.id);

      if (activeModule !== undefined) {
        const newInactiveModule = {
          enterpriseId: activeModule.enterpriseId,
          moduleId: activeModule.moduleId,
          start:
            activeModule.lastPaid === null
              ? activeModule.start
              : activeModule.lastPaid,
          expiration: activeModule.nextPayment,
          firsttimePayment: activeModule.firsttimePayment,
          paid: activeModule.lastPaid === null ? false : true,
          subscriptionId: subscription.id,
        };

        await CreateInactiveModule(newInactiveModule);
        await DeleteActiveModule(activeModule.id);
      }
    }

    if (item.isChecked === true) {
      const isModuleActive = activeModules.find((m) => m.moduleId === item.id);

      if (isModuleActive === undefined) {
        const newActiveModule = {
          enterpriseId: subscription.enterpriseId,
          moduleId: item.id,
          start: new Date(),
          firsttimePayment: true,
          subscriptionId: subscription.id,
          NextPayment: GetLastDayOfMonth(),
        };

        await CreateActiveModule(newActiveModule);
      }
    }
  });
};

export { UpdateConcurrentUsers, UpdateModules };
