namespace WebApi.Entities
{
    public enum Role
    {
        Admin,
        SAMAdmin,
        SAMUser,
        User
    }
}