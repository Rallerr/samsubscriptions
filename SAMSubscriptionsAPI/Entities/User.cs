using System.Text.Json.Serialization;

namespace WebApi.Entities
{
    public class User
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public Role Role { get; set; }
        public string EnterpriseId { get; set; }

        [JsonIgnore]
        public string Password { get; set; }
    }
}