﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class InactiveConcurrentUsers
    {
        public string Id { get; set; }
        public string EnterpriseId { get; set; }
        public int? CAMO { get; set; }
        public int? MRO { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? Expiration { get; set; }
        public bool? Paid{ get; set; }
        public string SubscriptionId { get; set; }
    }
}
