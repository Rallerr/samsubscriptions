﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class Module
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double? Price { get; set; }
        public string GroupId { get; set; }
        public string PaymentFrequencyId { get; set; }
        public bool SAMModule { get; set; }
        public double? FirsttimePrice { get; set; }
        public string EconomicsProductId { get; set; }
    }
}
