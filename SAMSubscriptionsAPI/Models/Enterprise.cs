﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class Enterprise
    {

        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Street { get; set; }
        public string StreetNo { get; set; }
        public int? Zip { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string EnterpriseNo { get; set; }
        public int EconomicsCustomerNumber { get; set; }

    }
}
