﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class InactivePriceMultiplier
    {
        public string Id{ get; set; }
        public string SubscriptionId { get; set; }
        public string PriceMultiplierId{ get; set; }
        public DateTime? Start { get; set; }
        public DateTime? Expiration { get; set; }
        public bool? Paid { get; set; }
    }
}
