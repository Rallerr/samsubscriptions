﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class AircraftCount
    {

        public string Id { get; set; }
        public string EnterpriseId { get; set; }
        public int? NumberOfAircrafts { get; set; }
    }
}
