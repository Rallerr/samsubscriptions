﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class InactiveModule
    {
        public string Id { get; set; }
        public string EnterpriseId { get; set; }
        public string ModuleId { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? Expiration { get; set; }
        public bool? FirsttimePayment { get; set; }
        public bool? Paid { get; set; }
        public string SubscriptionId { get; set; }
    }
}
