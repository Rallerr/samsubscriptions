﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class PriceMultiplier
    {

        public string Id { get; set; }
        public string Title { get; set; }
        public double Factor { get; set; }
        public string Group { get; set; }
        public string Type { get; set; }

    }
}
