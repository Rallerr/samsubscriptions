﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class ModuleSetting
    {
        public string Id { get; set; }
        public string ModuleId { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }

    }
}
