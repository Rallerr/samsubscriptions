﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class ActivePriceMultiplier
    {

        public string Id { get; set; }
        public string SubscriptionId { get; set; }
        public string PriceMultiplierId { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? LastPaid { get; set; }
        public DateTime? NextPayment { get; set; }

    }
}
