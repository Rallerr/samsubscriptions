﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class ActiveConcurrentUsers
    {

        public string Id { get; set; }
        public string EnterpriseId { get; set; }
        public int? CAMO { get; set; }
        public int? MRO { get; set; }
        public DateTime? Start { get; set; }
        public DateTime? LastPaid { get; set; }
        public string SubscriptionId { get; set; }
        public DateTime? NextPayment { get; set; }

    }
}
