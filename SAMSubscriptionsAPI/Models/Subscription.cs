﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class Subscription
    {
        public string Id { get; set; }
        public string EnterpriseId { get; set; }
        public string BillingPeriodId { get; set; }
        public string CurrencyId { get; set; }
        public string AircraftCountId { get; set; }
        public string SubscriptionTypeId { get; set; }
        public double Discount { get; set; }
    }
}
