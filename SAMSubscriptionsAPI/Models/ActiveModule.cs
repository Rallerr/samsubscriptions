﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class ActiveModule
    {
        public string Id { get; set; }
        public string EnterpriseId { get; set; }
        public string ModuleId { get; set; }
        public DateTime? Start { get; set; }
        public bool? FirsttimePayment { get; set; }
        public string SubscriptionId { get; set; }
        public DateTime? LastPaid { get; set; }
        public DateTime? NextPayment { get; set; }
    }
}
