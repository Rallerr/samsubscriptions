﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class Currency
    {
        public string Id { get; set; }
        public string CurrencyName { get; set; }
        public double Rate { get; set; }
    }
}
