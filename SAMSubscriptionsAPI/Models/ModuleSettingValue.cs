﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class ModuleSettingValue
    {
        public string Id { get; set; }
        public string ModuleSettingId { get; set; }
        public string Value { get; set; }
        public string InactiveModuleId { get; set; }
        public string ActiveModuleId { get; set; }
    }
}
