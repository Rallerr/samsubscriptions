﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class PaymentFrequency
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
