﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace SAM.Economic.Integration.Client.Models
{
    public class CustomerModel
    {
        public class PaymentTerms
        {
            public int paymentTermsNumber { get; set; }
        }

        public class CustomerGroup
        {
            public int customerGroupNumber { get; set; }
        }

        public class VatZone
        {
            public int vatZoneNumber { get; set; }
        }

        public class Layout
        {
            public int layoutNumber { get; set; }
        }

        public class Templates
        {
            public string invoice { get; set; }
            public string invoiceLine { get; set; }
        }

        public class Totals
        {
            public string booked { get; set; }
            public string drafts { get; set; }
        }

        public class Invoices
        {
            public string booked { get; set; }
            public string drafts { get; set; }
        }

        public class Customer
        {
            public int customerNumber { get; set; }
        }

        public class Attention
        {
            public Customer customer { get; set; }
            public int customerContactNumber { get; set; }
        }

        public class CustomerObject
        {
            public string mobilePhone;
            public string address { get; set; }
            //public Attention attention { get; set; }
            public double balance { get; set; }
            public string city { get; set; }
            public string contacts { get; set; }
            public string corporateIdentificationNumber { get; set; }
            public string country { get; set; }
            public string currency { get; set; }
            public CustomerGroup customerGroup { get; set; }
            public Layout layout { get; set; }
            public int customerNumber { get; set; }
            public string deliveryLocations { get; set; }
            public double dueAmount { get; set; }
            public string email { get; set; }
            public Invoices invoices { get; set; }
            public DateTime lastUpdated { get; set; }
            public string name { get; set; }
            public PaymentTerms paymentTerms { get; set; }
            public string self { get; set; }
            public string telephoneAndFaxNumber { get; set; }
            public Templates templates { get; set; }
            public Totals totals { get; set; }
            public VatZone vatZone { get; set; }
            public string website { get; set; }
            public string zip { get; set; }
        }

        public class Pagination
        {
            public string firstPage { get; set; }
            public string lastPage { get; set; }
            public int maxPageSizeAllowed { get; set; }
            public int pageSize { get; set; }
            public int results { get; set; }
            public int resultsWithoutFilter { get; set; }
            public int skipPages { get; set; }
        }

        public class Create
        {
            public string description { get; set; }
            public string href { get; set; }
            public string httpMethod { get; set; }
        }

        public class MetaData
        {
            public Create create { get; set; }
        }

        public class RootObject
        {
            public List<CustomerObject> collection { get; set; }
            public MetaData metaData { get; set; }
            public Pagination pagination { get; set; }
            public string self { get; set; }
        }
    }
}