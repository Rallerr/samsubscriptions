﻿namespace SAM.Economic.Integration.Client.Models
{
    public class JournalGet
    {
        public class Rootobject
        {
            public Collection[] collection { get; set; }
            public Pagination pagination { get; set; }
            public string self { get; set; }
        }

        public class Pagination
        {
            public int maxPageSizeAllowed { get; set; }
            public int skipPages { get; set; }
            public int pageSize { get; set; }
            public int results { get; set; }
            public int resultsWithoutFilter { get; set; }
            public string firstPage { get; set; }
            public string lastPage { get; set; }
        }

        public class Collection
        {
            public string name { get; set; }
            public Settings settings { get; set; }
            public string vouchers { get; set; }
            public string entries { get; set; }
            public Templates templates { get; set; }
            public int journalNumber { get; set; }
            public string self { get; set; }
        }

        public class Settings
        {
            public Vouchernumbers voucherNumbers { get; set; }
            public string entryTypeRestrictedTo { get; set; }
            public Contraaccounts contraAccounts { get; set; }
        }

        public class Vouchernumbers
        {
            public int minimumVoucherNumber { get; set; }
        }

        public class Contraaccounts
        {
            public Customerpayments customerPayments { get; set; }
        }

        public class Customerpayments
        {
            public int accountNumber { get; set; }
            public string self { get; set; }
        }

        public class Templates
        {
            public string financeVoucher { get; set; }
            public string manualCustomerInvoice { get; set; }
            public string self { get; set; }
        }
    }
}