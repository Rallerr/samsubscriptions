﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.Economics
{
    public class Layout
    {
        public int layoutNumber { get; set; }
        public string name { get; set; }
        public bool deleted { get; set; }


        public class Pagination
        {
            public string firstPage { get; set; }
            public string lastPage { get; set; }
            public int maxPageSizeAllowed { get; set; }
            public int pageSize { get; set; }
            public int results { get; set; }
            public int resultsWithoutFilter { get; set; }
            public int skipPages { get; set; }
        }

        public class Create
        {
            public string description { get; set; }
            public string href { get; set; }
            public string httpMethod { get; set; }
        }

        public class MetaData
        {
            public Create create { get; set; }
        }

        public class RootObject
        {
            public List<Layout> collection { get; set; }
            public MetaData metaData { get; set; }
            public Pagination pagination { get; set; }
            public string self { get; set; }
        }
    }
}
