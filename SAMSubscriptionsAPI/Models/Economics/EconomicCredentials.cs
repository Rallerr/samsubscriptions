﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.Models.Economics
{
    public class EconomicCredentials
    {

        public string Secret { get; set; }
        public string PartnerSecret { get; set; }
    }
}
