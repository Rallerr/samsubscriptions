﻿#region

using System.Collections.Generic;

#endregion

namespace SAM.Economic.Integration.Client.Models
{
    internal class FullInvoice
    {
        public class PaymentTerms
        {
            public int daysOfCredit { get; set; }
            public string name { get; set; }
            public int paymentTermsNumber { get; set; }
            public string paymentTermsType { get; set; }
        }

        public class Customer
        {
            public int customerNumber { get; set; }
        }

        public class VatZone
        {
            public bool enabledForCustomer { get; set; }
            public bool enabledForSupplier { get; set; }
            public string name { get; set; }
            public int vatZoneNumber { get; set; }
        }

        public class Recipient
        {
            public string address { get; set; }
            public string city { get; set; }
            public string name { get; set; }
            public VatZone vatZone { get; set; }
            public string zip { get; set; }
        }

        public class Delivery
        {
            public string address { get; set; }
            public string city { get; set; }
            public string country { get; set; }
            public string deliveryDate { get; set; }
            public string zip { get; set; }
        }

        public class References
        {
            public string other { get; set; }
        }

        public class Layout
        {
            public int layoutNumber { get; set; }
        }

        public class Unit
        {
            public string name { get; set; }
            public int unitNumber { get; set; }
        }

        public class Product
        {
            public string productNumber { get; set; }
        }

        public class Line
        {
            public double discountPercentage { get; set; }
            public int lineNumber { get; set; }
            public double marginInBaseCurrency { get; set; }
            public double marginPercentage { get; set; }
            public Product product { get; set; }
            public double quantity { get; set; }
            public int sortKey { get; set; }
            public double totalNetAmount { get; set; }
            public Unit unit { get; set; }
            public double unitCostPrice { get; set; }
            public double unitNetPrice { get; set; }
        }

        public class FullInvoiceObject
        {
            public double costPriceInBaseCurrency { get; set; }
            public string currency { get; set; }
            public Customer customer { get; set; }
            public string date { get; set; }
            public Delivery delivery { get; set; }
            public double exchangeRate { get; set; }
            public double grossAmount { get; set; }
            public Layout layout { get; set; }
            public List<Line> lines { get; set; }
            public double marginInBaseCurrency { get; set; }
            public double marginPercentage { get; set; }
            public double netAmount { get; set; }
            public double netAmountInBaseCurrency { get; set; }
            public PaymentTerms paymentTerms { get; set; }
            public Recipient recipient { get; set; }
            public References references { get; set; }
            public double roundingAmount { get; set; }
            public double vatAmount { get; set; }
        }
    }
}