﻿namespace SAM.Economic.Integration.Client.Models
{
    public class DraftInvoice
    {
        public class Rootobject
        {
            public Collection[] Collection { get; set; }
            public Pagination pagination { get; set; }
            public string self { get; set; }
        }

        public class Pagination
        {
            public string firstPage { get; set; }
            public string lastPage { get; set; }
            public int maxPageSizeAllowed { get; set; }
            public int pageSize { get; set; }
            public int results { get; set; }
            public int resultsWithoutFilter { get; set; }
            public int skipPages { get; set; }
        }

        public class Collection
        {
            public float costPriceInBaseCurrency { get; set; }
            public string currency { get; set; }
            public Customer customer { get; set; }
            public string date { get; set; }
            public int draftInvoiceNumber { get; set; }
            public string dueDate { get; set; }
            public float exchangeRate { get; set; }
            public float grossAmount { get; set; }
            public float grossAmountInBaseCurrency { get; set; }
            public Layout layout { get; set; }
            public float marginInBaseCurrency { get; set; }
            public float marginPercentage { get; set; }
            public float netAmount { get; set; }
            public float netAmountInBaseCurrency { get; set; }
            public Paymentterms paymentTerms { get; set; }
            public Pdf pdf { get; set; }
            public Recipient recipient { get; set; }
            public float roundingAmount { get; set; }
            public string self { get; set; }
            public Soap soap { get; set; }
            public Templates templates { get; set; }
            public float vatAmount { get; set; }
        }

        public class Soap
        {
            public Currentinvoicehandle currentInvoiceHandle { get; set; }
        }

        public class Currentinvoicehandle
        {
            public int id { get; set; }
        }

        public class Templates
        {
            public string bookingInstructions { get; set; }
            public string self { get; set; }
        }

        public class Paymentterms
        {
            public int daysOfCredit { get; set; }
            public string name { get; set; }
            public int paymentTermsNumber { get; set; }
            public string paymentTermsType { get; set; }
            public string self { get; set; }
        }

        public class Customer
        {
            public int customerNumber { get; set; }
            public string self { get; set; }
        }

        public class Recipient
        {
            public string address { get; set; }
            public string city { get; set; }
            public string name { get; set; }
            public Vatzone vatZone { get; set; }
            public string zip { get; set; }
        }

        public class Vatzone
        {
            public bool enabledForCustomer { get; set; }
            public bool enabledForSupplier { get; set; }
            public string name { get; set; }
            public string self { get; set; }
            public int vatZoneNumber { get; set; }
        }

        public class Layout
        {
            public int layoutNumber { get; set; }
            public string self { get; set; }
        }

        public class Pdf
        {
            public string download { get; set; }
        }
    }
}