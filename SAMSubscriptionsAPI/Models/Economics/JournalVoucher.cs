﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Globalization;

namespace SAM.Economic.Integration.Client.Models
{
    public class JournalVoucher
    {
        /// <summary>
        /// A schema for creating a journal entry.
        /// </summary>
        public partial class Coordinate
        {
            /// <summary>
            /// The accounting year the voucher belongs to. Together with the voucherNumber it is a
            /// unique identifier for the voucher.
            /// </summary>
            [JsonProperty("accountingYear")]
            public AccountingYear AccountingYear { get; set; }

            /// <summary>
            /// Entries, separated by type, for the journal.
            /// </summary>
            [JsonProperty("entries", NullValueHandling = NullValueHandling.Ignore)]
            public Entries Entries { get; set; }

            /// <summary>
            /// The journal the voucher belongs to.
            /// </summary>
            [JsonProperty("journal", NullValueHandling = NullValueHandling.Ignore)]
            public CoordinateJournal Journal { get; set; }

            /// <summary>
            /// Journal voucher number must be between 1-999999999. Together with the accountingYear it
            /// is a unique identifier for the voucher.
            /// </summary>
            [JsonProperty("voucherNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxValueCheckConverter))]
            public int? VoucherNumber { get; set; }
        }

        /// <summary>
        /// The accounting year the voucher belongs to. Together with the voucherNumber it is a
        /// unique identifier for the voucher.
        /// </summary>
        public partial class AccountingYear
        {
            /// <summary>
            /// A unique reference to the accounting year resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// A unique identifier of the accounting year.
            /// </summary>
            [JsonProperty("year", NullValueHandling = NullValueHandling.Ignore)]
            public string Year { get; set; }
        }

        /// <summary>
        /// Entries, separated by type, for the journal.
        /// </summary>
        public partial class Entries
        {
            /// <summary>
            /// An array containing customer payments.
            /// </summary>
            [JsonProperty("customerPayments", NullValueHandling = NullValueHandling.Ignore)]
            public CustomerPayment[] CustomerPayments { get; set; }

            /// <summary>
            /// An array containing finance vouchers.
            /// </summary>
            [JsonProperty("financeVouchers", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucher[] FinanceVouchers { get; set; }

            /// <summary>
            /// An array containing finance vouchers.
            /// </summary>
            [JsonProperty("manualCustomerInvoices", NullValueHandling = NullValueHandling.Ignore)]
            public ManualCustomerInvoice[] ManualCustomerInvoices { get; set; }

            /// <summary>
            /// An array containing finance vouchers.
            /// </summary>
            [JsonProperty("supplierInvoices", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoice[] SupplierInvoices { get; set; }

            /// <summary>
            /// An array containing finance vouchers.
            /// </summary>
            [JsonProperty("supplierPayments", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierPayment[] SupplierPayments { get; set; }
        }

        /// <summary>
        /// Customer Payment entry.
        /// </summary>
        public partial class CustomerPayment
        {
            /// <summary>
            /// Defining whether it is a debit or credit can be done by prefixing the amount with the '-'
            /// (minus) sign.
            /// </summary>
            [JsonProperty("amount")]
            public double Amount { get; set; }

            /// <summary>
            /// The amount in base currency.
            /// </summary>
            [JsonProperty("amountBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? AmountBaseCurrency { get; set; }

            /// <summary>
            /// The account used for the funds. Either 'customer' or 'contraAccount' is required.
            /// </summary>
            [JsonProperty("contraAccount", NullValueHandling = NullValueHandling.Ignore)]
            public CustomerPaymentContraAccount ContraAccount { get; set; }

            /// <summary>
            /// The account for VAT.
            /// </summary>
            [JsonProperty("contraVatAccount", NullValueHandling = NullValueHandling.Ignore)]
            public CustomerPaymentContraVatAccount ContraVatAccount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry on the contra account.
            /// </summary>
            [JsonProperty("contraVatAmount", NullValueHandling = NullValueHandling.Ignore)]
            public double? ContraVatAmount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry on the contra account in base currency.
            /// </summary>
            [JsonProperty("contraVatAmountInBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? ContraVatAmountInBaseCurrency { get; set; }

            /// <summary>
            /// The currency for the entry.
            /// </summary>
            [JsonProperty("currency", NullValueHandling = NullValueHandling.Ignore)]
            public CustomerPaymentCurrency Currency { get; set; }

            /// <summary>
            /// The customer this entry is based on. Either 'customer' or 'contraAccount' is required.
            /// </summary>
            [JsonProperty("customer", NullValueHandling = NullValueHandling.Ignore)]
            public CustomerPaymentCustomer Customer { get; set; }

            /// <summary>
            /// Customer invoice number.
            /// </summary>
            [JsonProperty("customerInvoice", NullValueHandling = NullValueHandling.Ignore)]
            public long? CustomerInvoice { get; set; }

            /// <summary>
            /// Entry date. Format according to ISO-8601 (YYYY-MM-DD).
            /// </summary>
            [JsonProperty("date")]
            public string Date { get; set; }

            /// <summary>
            /// A departmental distribution defines which departments this entry is distributed between.
            /// This requires the departments module to be enabled.
            /// </summary>
            [JsonProperty("departmentalDistribution", NullValueHandling = NullValueHandling.Ignore)]
            public CustomerPaymentDepartmentalDistribution DepartmentalDistribution { get; set; }

            /// <summary>
            /// Employee that this entry is related to. Requires Project module
            /// </summary>
            [JsonProperty("employee", NullValueHandling = NullValueHandling.Ignore)]
            public CustomerPaymentEmployee Employee { get; set; }

            /// <summary>
            /// Type of the journal entry. This is automatically set to customerPayment.
            /// </summary>
            [JsonProperty("entryType", NullValueHandling = NullValueHandling.Ignore)]
            public CustomerPaymentEntryType? EntryType { get; set; }

            /// <summary>
            /// The exchange rate between the base currency and the invoice currency.
            /// </summary>
            [JsonProperty("exchangeRate", NullValueHandling = NullValueHandling.Ignore)]
            public double? ExchangeRate { get; set; }

            /// <summary>
            /// The journal the voucher belongs to.
            /// </summary>
            [JsonProperty("journal", NullValueHandling = NullValueHandling.Ignore)]
            public CustomerPaymentJournal Journal { get; set; }

            /// <summary>
            /// Remaining amount to be paid.
            /// </summary>
            [JsonProperty("remainder", NullValueHandling = NullValueHandling.Ignore)]
            public double? Remainder { get; set; }

            /// <summary>
            /// Remaining amount to be paid in default currency.
            /// </summary>
            [JsonProperty("remainderInDefaultCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? RemainderInDefaultCurrency { get; set; }

            /// <summary>
            /// Entry description.
            /// </summary>
            [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxLengthCheckConverter))]
            public string Text { get; set; }

            /// <summary>
            /// Voucher that the entry belongs to.
            /// </summary>
            [JsonProperty("voucher", NullValueHandling = NullValueHandling.Ignore)]
            public CustomerPaymentVoucher Voucher { get; set; }
        }

        /// <summary>
        /// The account used for the funds. Either 'customer' or 'contraAccount' is required.
        /// </summary>
        public partial class CustomerPaymentContraAccount
        {
            /// <summary>
            /// The account number.
            /// </summary>
            [JsonProperty("accountNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? AccountNumber { get; set; }

            /// <summary>
            /// A unique reference to the account resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The account for VAT.
        /// </summary>
        public partial class CustomerPaymentContraVatAccount
        {
            /// <summary>
            /// A unique link reference to the contraVatAccount item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The unique identifier of the contra vat account.
            /// </summary>
            [JsonProperty("vatCode", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxLengthCheckConverter))]
            public string VatCode { get; set; }
        }

        /// <summary>
        /// The currency for the entry.
        /// </summary>
        public partial class CustomerPaymentCurrency
        {
            /// <summary>
            /// The ISO 4217 3-letter currency code of the entry.
            /// </summary>
            [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
            public string Code { get; set; }

            /// <summary>
            /// A unique link reference to the currency item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The customer this entry is based on. Either 'customer' or 'contraAccount' is required.
        /// </summary>
        public partial class CustomerPaymentCustomer
        {
            /// <summary>
            /// The customer number is a positive unique numerical identifier with a maximum of 9 digits.
            /// </summary>
            [JsonProperty("customerNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? CustomerNumber { get; set; }

            /// <summary>
            /// A unique reference to the customer resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// A departmental distribution defines which departments this entry is distributed between.
        /// This requires the departments module to be enabled.
        /// </summary>
        public partial class CustomerPaymentDepartmentalDistribution
        {
            /// <summary>
            /// A unique identifier of the departmental distribution.
            /// </summary>
            [JsonProperty("departmentalDistributionNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? DepartmentalDistributionNumber { get; set; }

            /// <summary>
            /// Type of the distribution
            /// </summary>
            [JsonProperty("distributionType", NullValueHandling = NullValueHandling.Ignore)]
            public string DistributionType { get; set; }

            /// <summary>
            /// A unique reference to the departmental distribution resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// Employee that this entry is related to. Requires Project module
        /// </summary>
        public partial class CustomerPaymentEmployee
        {
            /// <summary>
            /// A unique numerical identifier with a maximum of 9 digits.
            /// </summary>
            [JsonProperty("employeeNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? EmployeeNumber { get; set; }

            /// <summary>
            /// A unique link reference to the employee item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The journal the voucher belongs to.
        /// </summary>
        public partial class CustomerPaymentJournal
        {
            /// <summary>
            /// Journal number that must be at least 1.
            /// </summary>
            [JsonProperty("journalNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxValueCheckConverter))]
            public double? JournalNumber { get; set; }

            /// <summary>
            /// A unique reference to the journal resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// Voucher that the entry belongs to.
        /// </summary>
        public partial class CustomerPaymentVoucher
        {
            /// <summary>
            /// A unique link reference to the voucher item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// Journal voucher number must be between 1-999999999.
            /// </summary>
            [JsonProperty("voucherNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxValueCheckConverter))]
            public int? VoucherNumber { get; set; }
        }

        /// <summary>
        /// Finance Voucher entry.
        /// </summary>
        public partial class FinanceVoucher
        {
            /// <summary>
            /// The account used by the entry.
            /// </summary>
            [JsonProperty("account", NullValueHandling = NullValueHandling.Ignore)]
            public Account Account { get; set; }

            [JsonProperty("amount")]
            public double Amount { get; set; }

            /// <summary>
            /// The amount in base currency.
            /// </summary>
            [JsonProperty("amountBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? AmountBaseCurrency { get; set; }

            /// <summary>
            /// The account used for the funds.
            /// </summary>
            [JsonProperty("contraAccount", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherContraAccount ContraAccount { get; set; }

            /// <summary>
            /// The account for VAT.
            /// </summary>
            [JsonProperty("contraVatAccount", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherContraVatAccount ContraVatAccount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry on the contra account.
            /// </summary>
            [JsonProperty("contraVatAmount", NullValueHandling = NullValueHandling.Ignore)]
            public double? ContraVatAmount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry on the contra account in base currency.
            /// </summary>
            [JsonProperty("contraVatAmountInBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? ContraVatAmountInBaseCurrency { get; set; }

            /// <summary>
            /// The currency for the entry.
            /// </summary>
            [JsonProperty("currency", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherCurrency Currency { get; set; }

            /// <summary>
            /// Entry date. Format according to ISO-8601 (YYYY-MM-DD).
            /// </summary>
            [JsonProperty("date")]
            public string Date { get; set; }

            /// <summary>
            /// A departmental distribution defines which departments this entry is distributed between.
            /// This requires the departments module to be enabled.
            /// </summary>
            [JsonProperty("departmentalDistribution", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherDepartmentalDistribution DepartmentalDistribution { get; set; }

            /// <summary>
            /// Employee that this entry is related to. Requires Project module
            /// </summary>
            [JsonProperty("employee", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherEmployee Employee { get; set; }

            /// <summary>
            /// Type of the journal entry. This is automatically set to financeVoucher.
            /// </summary>
            [JsonProperty("entryType", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherEntryType? EntryType { get; set; }

            /// <summary>
            /// The exchange rate between the base currency and the invoice currency.
            /// </summary>
            [JsonProperty("exchangeRate", NullValueHandling = NullValueHandling.Ignore)]
            public double? ExchangeRate { get; set; }

            /// <summary>
            /// The journal the voucher belongs to.
            /// </summary>
            [JsonProperty("journal", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherJournal Journal { get; set; }

            /// <summary>
            /// The project the entry is connected to.
            /// </summary>
            [JsonProperty("project", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherProject Project { get; set; }

            /// <summary>
            /// Requires dimension module.
            /// </summary>
            [JsonProperty("quantity1", NullValueHandling = NullValueHandling.Ignore)]
            public double? Quantity1 { get; set; }

            /// <summary>
            /// Requires dimension module.
            /// </summary>
            [JsonProperty("quantity2", NullValueHandling = NullValueHandling.Ignore)]
            public double? Quantity2 { get; set; }

            /// <summary>
            /// Remaining amount to be paid.
            /// </summary>
            [JsonProperty("remainder", NullValueHandling = NullValueHandling.Ignore)]
            public double? Remainder { get; set; }

            /// <summary>
            /// Remaining amount to be paid in default currency.
            /// </summary>
            [JsonProperty("remainderInDefaultCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? RemainderInDefaultCurrency { get; set; }

            /// <summary>
            /// Entry description.
            /// </summary>
            [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxLengthCheckConverter))]
            public string Text { get; set; }

            /// <summary>
            /// The first unit of measure applied to the entry. Requires dimension module.
            /// </summary>
            [JsonProperty("unit1", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherUnit1 Unit1 { get; set; }

            /// <summary>
            /// The second unit of measure applied to the entry. Requires dimension module.
            /// </summary>
            [JsonProperty("unit2", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherUnit2 Unit2 { get; set; }

            /// <summary>
            /// The account for VAT.
            /// </summary>
            [JsonProperty("vatAccount", NullValueHandling = NullValueHandling.Ignore)]
            public VatAccount VatAccount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry. Used if the entry has a VAT account.
            /// </summary>
            [JsonProperty("vatAmount", NullValueHandling = NullValueHandling.Ignore)]
            public double? VatAmount { get; set; }

            /// <summary>
            /// The amount of VAT on the voucher in base currency.
            /// </summary>
            [JsonProperty("vatAmountBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? VatAmountBaseCurrency { get; set; }

            /// <summary>
            /// Voucher that the entry belongs to.
            /// </summary>
            [JsonProperty("voucher", NullValueHandling = NullValueHandling.Ignore)]
            public FinanceVoucherVoucher Voucher { get; set; }
        }

        /// <summary>
        /// The account used by the entry.
        /// </summary>
        public partial class Account
        {
            /// <summary>
            /// The account number.
            /// </summary>
            [JsonProperty("accountNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? AccountNumber { get; set; }

            /// <summary>
            /// A unique reference to the account resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The account used for the funds.
        /// </summary>
        public partial class FinanceVoucherContraAccount
        {
            /// <summary>
            /// The account number.
            /// </summary>
            [JsonProperty("accountNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? AccountNumber { get; set; }

            /// <summary>
            /// A unique reference to the account resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The account for VAT.
        /// </summary>
        public partial class FinanceVoucherContraVatAccount
        {
            /// <summary>
            /// A unique link reference to the contraVatAccount item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The unique identifier of the contra vat account.
            /// </summary>
            [JsonProperty("vatCode", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxLengthCheckConverter))]
            public string VatCode { get; set; }
        }

        /// <summary>
        /// The currency for the entry.
        /// </summary>
        public partial class FinanceVoucherCurrency
        {
            /// <summary>
            /// The ISO 4217 3-letter currency code of the entry.
            /// </summary>
            [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
            public string Code { get; set; }

            /// <summary>
            /// A unique link reference to the currency item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// A departmental distribution defines which departments this entry is distributed between.
        /// This requires the departments module to be enabled.
        /// </summary>
        public partial class FinanceVoucherDepartmentalDistribution
        {
            /// <summary>
            /// A unique identifier of the departmental distribution.
            /// </summary>
            [JsonProperty("departmentalDistributionNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? DepartmentalDistributionNumber { get; set; }

            /// <summary>
            /// Type of the distribution
            /// </summary>
            [JsonProperty("distributionType", NullValueHandling = NullValueHandling.Ignore)]
            public string DistributionType { get; set; }

            /// <summary>
            /// A unique reference to the departmental distribution resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// Employee that this entry is related to. Requires Project module
        /// </summary>
        public partial class FinanceVoucherEmployee
        {
            /// <summary>
            /// A unique numerical identifier with a maximum of 9 digits.
            /// </summary>
            [JsonProperty("employeeNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? EmployeeNumber { get; set; }

            /// <summary>
            /// A unique link reference to the employee item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The journal the voucher belongs to.
        /// </summary>
        public partial class FinanceVoucherJournal
        {
            /// <summary>
            /// Journal number that must be at least 1.
            /// </summary>
            [JsonProperty("journalNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxValueCheckConverter))]
            public double? JournalNumber { get; set; }

            /// <summary>
            /// A unique reference to the journal resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The project the entry is connected to.
        /// </summary>
        public partial class FinanceVoucherProject
        {
            /// <summary>
            /// A unique identifier of the project.
            /// </summary>
            [JsonProperty("projectNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? ProjectNumber { get; set; }

            /// <summary>
            /// A unique reference to the project resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The first unit of measure applied to the entry. Requires dimension module.
        /// </summary>
        public partial class FinanceVoucherUnit1
        {
            /// <summary>
            /// A unique reference to the unit resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The unique identifier of the unit.
            /// </summary>
            [JsonProperty("unitNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? UnitNumber { get; set; }
        }

        /// <summary>
        /// The second unit of measure applied to the entry. Requires dimension module.
        /// </summary>
        public partial class FinanceVoucherUnit2
        {
            /// <summary>
            /// A unique reference to the unit resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The unique identifier of the unit.
            /// </summary>
            [JsonProperty("unitNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? UnitNumber { get; set; }
        }

        /// <summary>
        /// The account for VAT.
        /// </summary>
        public partial class VatAccount
        {
            /// <summary>
            /// A unique link reference to the vatAccount item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The unique identifier of the vat account.
            /// </summary>
            [JsonProperty("vatCode", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxLengthCheckConverter))]
            public string VatCode { get; set; }
        }

        /// <summary>
        /// Voucher that the entry belongs to.
        /// </summary>
        public partial class FinanceVoucherVoucher
        {
            /// <summary>
            /// A unique link reference to the voucher item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// Journal voucher number must be between 1-999999999.
            /// </summary>
            [JsonProperty("voucherNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxValueCheckConverter))]
            public int? VoucherNumber { get; set; }
        }

        /// <summary>
        /// Manual Customer Invoice entry.
        /// </summary>
        public partial class ManualCustomerInvoice
        {
            [JsonProperty("amount")]
            public double Amount { get; set; }

            /// <summary>
            /// The amount in base currency.
            /// </summary>
            [JsonProperty("amountBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? AmountBaseCurrency { get; set; }

            /// <summary>
            /// The account used for the funds. Either 'customer' or 'contraAccount' is required.
            /// </summary>
            [JsonProperty("contraAccount", NullValueHandling = NullValueHandling.Ignore)]
            public ManualCustomerInvoiceContraAccount ContraAccount { get; set; }

            /// <summary>
            /// The account for VAT.
            /// </summary>
            [JsonProperty("contraVatAccount", NullValueHandling = NullValueHandling.Ignore)]
            public ManualCustomerInvoiceContraVatAccount ContraVatAccount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry on the contra account.
            /// </summary>
            [JsonProperty("contraVatAmount", NullValueHandling = NullValueHandling.Ignore)]
            public double? ContraVatAmount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry on the contra account in base currency.
            /// </summary>
            [JsonProperty("contraVatAmountInBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? ContraVatAmountInBaseCurrency { get; set; }

            /// <summary>
            /// The currency for the entry.
            /// </summary>
            [JsonProperty("currency", NullValueHandling = NullValueHandling.Ignore)]
            public ManualCustomerInvoiceCurrency Currency { get; set; }

            /// <summary>
            /// The customer this entry is based on. Either 'customer' or 'contraAccount' is required.
            /// </summary>
            [JsonProperty("customer", NullValueHandling = NullValueHandling.Ignore)]
            public ManualCustomerInvoiceCustomer Customer { get; set; }

            /// <summary>
            /// Customer invoice number.
            /// </summary>
            [JsonProperty("customerInvoice", NullValueHandling = NullValueHandling.Ignore)]
            public long? CustomerInvoice { get; set; }

            /// <summary>
            /// Entry date. Format according to ISO-8601 (YYYY-MM-DD).
            /// </summary>
            [JsonProperty("date")]
            public string Date { get; set; }

            /// <summary>
            /// A departmental distribution defines which departments this entry is distributed between.
            /// This requires the departments module to be enabled.
            /// </summary>
            [JsonProperty("departmentalDistribution", NullValueHandling = NullValueHandling.Ignore)]
            public ManualCustomerInvoiceDepartmentalDistribution DepartmentalDistribution { get; set; }

            /// <summary>
            /// The date the entry is due for payment. Format according to ISO-8601 (YYYY-MM-DD).
            /// </summary>
            [JsonProperty("dueDate", NullValueHandling = NullValueHandling.Ignore)]
            public string DueDate { get; set; }

            /// <summary>
            /// Employee that this entry is related to. Requires Project module
            /// </summary>
            [JsonProperty("employee", NullValueHandling = NullValueHandling.Ignore)]
            public ManualCustomerInvoiceEmployee Employee { get; set; }

            /// <summary>
            /// Type of the journal entry. This is automatically set to manualCustomerInvoice.
            /// </summary>
            [JsonProperty("entryType", NullValueHandling = NullValueHandling.Ignore)]
            public ManualCustomerInvoiceEntryType? EntryType { get; set; }

            /// <summary>
            /// The exchange rate between the base currency and the invoice currency.
            /// </summary>
            [JsonProperty("exchangeRate", NullValueHandling = NullValueHandling.Ignore)]
            public double? ExchangeRate { get; set; }

            /// <summary>
            /// The journal the voucher belongs to.
            /// </summary>
            [JsonProperty("journal", NullValueHandling = NullValueHandling.Ignore)]
            public ManualCustomerInvoiceJournal Journal { get; set; }

            /// <summary>
            /// Remaining amount to be paid.
            /// </summary>
            [JsonProperty("remainder", NullValueHandling = NullValueHandling.Ignore)]
            public double? Remainder { get; set; }

            /// <summary>
            /// Remaining amount to be paid in default currency.
            /// </summary>
            [JsonProperty("remainderInDefaultCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? RemainderInDefaultCurrency { get; set; }

            /// <summary>
            /// Entry description.
            /// </summary>
            [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxLengthCheckConverter))]
            public string Text { get; set; }

            /// <summary>
            /// Voucher that the entry belongs to.
            /// </summary>
            [JsonProperty("voucher", NullValueHandling = NullValueHandling.Ignore)]
            public ManualCustomerInvoiceVoucher Voucher { get; set; }
        }

        /// <summary>
        /// The account used for the funds. Either 'customer' or 'contraAccount' is required.
        /// </summary>
        public partial class ManualCustomerInvoiceContraAccount
        {
            /// <summary>
            /// The account number.
            /// </summary>
            [JsonProperty("accountNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? AccountNumber { get; set; }

            /// <summary>
            /// A unique reference to the account resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The account for VAT.
        /// </summary>
        public partial class ManualCustomerInvoiceContraVatAccount
        {
            /// <summary>
            /// A unique link reference to the contraVatAccount item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The unique identifier of the contra vat account.
            /// </summary>
            [JsonProperty("vatCode", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxLengthCheckConverter))]
            public string VatCode { get; set; }
        }

        /// <summary>
        /// The currency for the entry.
        /// </summary>
        public partial class ManualCustomerInvoiceCurrency
        {
            /// <summary>
            /// The ISO 4217 3-letter currency code of the entry.
            /// </summary>
            [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
            public string Code { get; set; }

            /// <summary>
            /// A unique link reference to the currency item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The customer this entry is based on. Either 'customer' or 'contraAccount' is required.
        /// </summary>
        public partial class ManualCustomerInvoiceCustomer
        {
            /// <summary>
            /// The customer number is a positive unique numerical identifier with a maximum of 9 digits.
            /// </summary>
            [JsonProperty("customerNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? CustomerNumber { get; set; }

            /// <summary>
            /// A unique reference to the customer resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// A departmental distribution defines which departments this entry is distributed between.
        /// This requires the departments module to be enabled.
        /// </summary>
        public partial class ManualCustomerInvoiceDepartmentalDistribution
        {
            /// <summary>
            /// A unique identifier of the departmental distribution.
            /// </summary>
            [JsonProperty("departmentalDistributionNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? DepartmentalDistributionNumber { get; set; }

            /// <summary>
            /// Type of the distribution
            /// </summary>
            [JsonProperty("distributionType", NullValueHandling = NullValueHandling.Ignore)]
            public string DistributionType { get; set; }

            /// <summary>
            /// A unique reference to the departmental distribution resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// Employee that this entry is related to. Requires Project module
        /// </summary>
        public partial class ManualCustomerInvoiceEmployee
        {
            /// <summary>
            /// A unique numerical identifier with a maximum of 9 digits.
            /// </summary>
            [JsonProperty("employeeNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? EmployeeNumber { get; set; }

            /// <summary>
            /// A unique link reference to the employee item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The journal the voucher belongs to.
        /// </summary>
        public partial class ManualCustomerInvoiceJournal
        {
            /// <summary>
            /// Journal number that must be at least 1.
            /// </summary>
            [JsonProperty("journalNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxValueCheckConverter))]
            public double? JournalNumber { get; set; }

            /// <summary>
            /// A unique reference to the journal resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// Voucher that the entry belongs to.
        /// </summary>
        public partial class ManualCustomerInvoiceVoucher
        {
            /// <summary>
            /// A unique link reference to the voucher item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// Journal voucher number must be between 1-999999999.
            /// </summary>
            [JsonProperty("voucherNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxValueCheckConverter))]
            public int? VoucherNumber { get; set; }
        }

        /// <summary>
        /// Supplier Invoice entry.
        /// </summary>
        public partial class SupplierInvoice
        {
            [JsonProperty("amount")]
            public double Amount { get; set; }

            /// <summary>
            /// The amount in base currency.
            /// </summary>
            [JsonProperty("amountBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? AmountBaseCurrency { get; set; }

            /// <summary>
            /// The account used for the funds. Either 'supplier' or 'contraAccount' is required.
            /// </summary>
            [JsonProperty("contraAccount", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceContraAccount ContraAccount { get; set; }

            /// <summary>
            /// The account for VAT.
            /// </summary>
            [JsonProperty("contraVatAccount", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceContraVatAccount ContraVatAccount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry on the contra account.
            /// </summary>
            [JsonProperty("contraVatAmount", NullValueHandling = NullValueHandling.Ignore)]
            public double? ContraVatAmount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry on the contra account in base currency.
            /// </summary>
            [JsonProperty("contraVatAmountInBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? ContraVatAmountInBaseCurrency { get; set; }

            /// <summary>
            /// The currency for the entry.
            /// </summary>
            [JsonProperty("currency", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceCurrency Currency { get; set; }

            /// <summary>
            /// Entry date. Format according to ISO-8601 (YYYY-MM-DD).
            /// </summary>
            [JsonProperty("date")]
            public string Date { get; set; }

            /// <summary>
            /// A departmental distribution defines which departments this entry is distributed between.
            /// This requires the departments module to be enabled.
            /// </summary>
            [JsonProperty("departmentalDistribution", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceDepartmentalDistribution DepartmentalDistribution { get; set; }

            /// <summary>
            /// The date the entry is due for payment. Format according to ISO-8601 (YYYY-MM-DD).
            /// </summary>
            [JsonProperty("dueDate", NullValueHandling = NullValueHandling.Ignore)]
            public string DueDate { get; set; }

            /// <summary>
            /// Employee that this entry is related to. Requires Project module
            /// </summary>
            [JsonProperty("employee", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceEmployee Employee { get; set; }

            /// <summary>
            /// Type of the journal entry. This is automatically set to supplierInvoice.
            /// </summary>
            [JsonProperty("entryType", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceEntryType? EntryType { get; set; }

            /// <summary>
            /// The exchange rate between the base currency and the invoice currency.
            /// </summary>
            [JsonProperty("exchangeRate", NullValueHandling = NullValueHandling.Ignore)]
            public double? ExchangeRate { get; set; }

            /// <summary>
            /// The journal the voucher belongs to.
            /// </summary>
            [JsonProperty("journal", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceJournal Journal { get; set; }

            /// <summary>
            /// Payment details associated with the supplier invoice. Each payment detail contains two
            /// keys that are required. For the +71 type, fiSupplierNo and ocrLine is expected. For the
            /// +73 type, fiSupplierNo and message is expected. For the +04 type, giroAccount and ocrLine
            /// is expected. For the bank transfer type, accountNo and message is expected. For the IBAN
            /// type, ibanSwift and message is expected. For the +75 type, fiSupplierNo and
            /// ocrLineMessage is expected. Please refer to the schema for validation rules details.
            /// </summary>
            [JsonProperty("paymentDetails", NullValueHandling = NullValueHandling.Ignore)]
            public PaymentDetails PaymentDetails { get; set; }

            /// <summary>
            /// The project the entry is connected to.
            /// </summary>
            [JsonProperty("project", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceProject Project { get; set; }

            /// <summary>
            /// Requires dimension module.
            /// </summary>
            [JsonProperty("quantity1", NullValueHandling = NullValueHandling.Ignore)]
            public double? Quantity1 { get; set; }

            /// <summary>
            /// Requires dimension module.
            /// </summary>
            [JsonProperty("quantity2", NullValueHandling = NullValueHandling.Ignore)]
            public double? Quantity2 { get; set; }

            /// <summary>
            /// Remaining amount to be paid.
            /// </summary>
            [JsonProperty("remainder", NullValueHandling = NullValueHandling.Ignore)]
            public double? Remainder { get; set; }

            /// <summary>
            /// Remaining amount to be paid in default currency.
            /// </summary>
            [JsonProperty("remainderInDefaultCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? RemainderInDefaultCurrency { get; set; }

            /// <summary>
            /// The supplier is the vendor from whom you buy your goods. Either 'supplier' or
            /// 'contraAccount' is required.
            /// </summary>
            [JsonProperty("supplier", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceSupplier Supplier { get; set; }

            /// <summary>
            /// The unique identifier of the supplier invoice gotten from the supplier.
            /// </summary>
            [JsonProperty("supplierInvoiceNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(IndecentMinMaxLengthCheckConverter))]
            public string SupplierInvoiceNumber { get; set; }

            /// <summary>
            /// Entry description.
            /// </summary>
            [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxLengthCheckConverter))]
            public string Text { get; set; }

            /// <summary>
            /// The first unit of measure applied to the entry. Requires dimension module.
            /// </summary>
            [JsonProperty("unit1", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceUnit1 Unit1 { get; set; }

            /// <summary>
            /// The second unit of measure applied to the entry. Requires dimension module.
            /// </summary>
            [JsonProperty("unit2", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceUnit2 Unit2 { get; set; }

            /// <summary>
            /// Voucher that the entry belongs to.
            /// </summary>
            [JsonProperty("voucher", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierInvoiceVoucher Voucher { get; set; }
        }

        /// <summary>
        /// The account used for the funds. Either 'supplier' or 'contraAccount' is required.
        /// </summary>
        public partial class SupplierInvoiceContraAccount
        {
            /// <summary>
            /// The account number.
            /// </summary>
            [JsonProperty("accountNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? AccountNumber { get; set; }

            /// <summary>
            /// A unique reference to the account resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The account for VAT.
        /// </summary>
        public partial class SupplierInvoiceContraVatAccount
        {
            /// <summary>
            /// A unique link reference to the contraVatAccount item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The unique identifier of the contra vat account.
            /// </summary>
            [JsonProperty("vatCode", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxLengthCheckConverter))]
            public string VatCode { get; set; }
        }

        /// <summary>
        /// The currency for the entry.
        /// </summary>
        public partial class SupplierInvoiceCurrency
        {
            /// <summary>
            /// The ISO 4217 3-letter currency code of the entry.
            /// </summary>
            [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
            public string Code { get; set; }

            /// <summary>
            /// A unique link reference to the currency item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// A departmental distribution defines which departments this entry is distributed between.
        /// This requires the departments module to be enabled.
        /// </summary>
        public partial class SupplierInvoiceDepartmentalDistribution
        {
            /// <summary>
            /// A unique identifier of the departmental distribution.
            /// </summary>
            [JsonProperty("departmentalDistributionNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? DepartmentalDistributionNumber { get; set; }

            /// <summary>
            /// Type of the distribution
            /// </summary>
            [JsonProperty("distributionType", NullValueHandling = NullValueHandling.Ignore)]
            public string DistributionType { get; set; }

            /// <summary>
            /// A unique reference to the departmental distribution resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// Employee that this entry is related to. Requires Project module
        /// </summary>
        public partial class SupplierInvoiceEmployee
        {
            /// <summary>
            /// A unique numerical identifier with a maximum of 9 digits.
            /// </summary>
            [JsonProperty("employeeNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? EmployeeNumber { get; set; }

            /// <summary>
            /// A unique link reference to the employee item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The journal the voucher belongs to.
        /// </summary>
        public partial class SupplierInvoiceJournal
        {
            /// <summary>
            /// Journal number that must be at least 1.
            /// </summary>
            [JsonProperty("journalNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxValueCheckConverter))]
            public double? JournalNumber { get; set; }

            /// <summary>
            /// A unique reference to the journal resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// Payment details associated with the supplier invoice. Each payment detail contains two
        /// keys that are required. For the +71 type, fiSupplierNo and ocrLine is expected. For the
        /// +73 type, fiSupplierNo and message is expected. For the +04 type, giroAccount and ocrLine
        /// is expected. For the bank transfer type, accountNo and message is expected. For the IBAN
        /// type, ibanSwift and message is expected. For the +75 type, fiSupplierNo and
        /// ocrLineMessage is expected. Please refer to the schema for validation rules details.
        /// </summary>
        public partial class PaymentDetails
        {
            /// <summary>
            /// A specific payment type on the entry.
            /// </summary>
            [JsonProperty("paymentType", NullValueHandling = NullValueHandling.Ignore)]
            public PaymentType PaymentType { get; set; }
        }

        /// <summary>
        /// A specific payment type on the entry.
        /// </summary>
        public partial class PaymentType
        {
            /// <summary>
            /// The payment type number is a positive unique numerical identifier.
            /// </summary>
            [JsonProperty("paymentTypeNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? PaymentTypeNumber { get; set; }

            /// <summary>
            /// A unique link reference to the payment type item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            [JsonProperty("fiSupplierNo", NullValueHandling = NullValueHandling.Ignore)]
            public string FiSupplierNo { get; set; }

            [JsonProperty("ocrLine", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(StickyMinMaxLengthCheckConverter))]
            public string OcrLine { get; set; }

            [JsonProperty("message", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(TentacledMinMaxLengthCheckConverter))]
            public string Message { get; set; }

            [JsonProperty("giroAccount", NullValueHandling = NullValueHandling.Ignore)]
            public string GiroAccount { get; set; }

            [JsonProperty("accountNo", NullValueHandling = NullValueHandling.Ignore)]
            public string AccountNo { get; set; }

            [JsonProperty("ibanSwift", NullValueHandling = NullValueHandling.Ignore)]
            public string IbanSwift { get; set; }

            [JsonProperty("ocrLineMessage", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(IndigoMinMaxLengthCheckConverter))]
            public string OcrLineMessage { get; set; }
        }

        /// <summary>
        /// The project the entry is connected to.
        /// </summary>
        public partial class SupplierInvoiceProject
        {
            /// <summary>
            /// A unique identifier of the project.
            /// </summary>
            [JsonProperty("projectNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? ProjectNumber { get; set; }

            /// <summary>
            /// A unique reference to the project resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The supplier is the vendor from whom you buy your goods. Either 'supplier' or
        /// 'contraAccount' is required.
        /// </summary>
        public partial class SupplierInvoiceSupplier
        {
            /// <summary>
            /// A unique self reference of the supplier.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The supplier number is a positive unique numerical identifier with a maximum of 9 digits.
            /// </summary>
            [JsonProperty("supplierNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? SupplierNumber { get; set; }
        }

        /// <summary>
        /// The first unit of measure applied to the entry. Requires dimension module.
        /// </summary>
        public partial class SupplierInvoiceUnit1
        {
            /// <summary>
            /// A unique reference to the unit resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The unique identifier of the unit.
            /// </summary>
            [JsonProperty("unitNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? UnitNumber { get; set; }
        }

        /// <summary>
        /// The second unit of measure applied to the entry. Requires dimension module.
        /// </summary>
        public partial class SupplierInvoiceUnit2
        {
            /// <summary>
            /// A unique reference to the unit resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The unique identifier of the unit.
            /// </summary>
            [JsonProperty("unitNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? UnitNumber { get; set; }
        }

        /// <summary>
        /// Voucher that the entry belongs to.
        /// </summary>
        public partial class SupplierInvoiceVoucher
        {
            /// <summary>
            /// A unique link reference to the voucher item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// Journal voucher number must be between 1-999999999.
            /// </summary>
            [JsonProperty("voucherNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxValueCheckConverter))]
            public int? VoucherNumber { get; set; }
        }

        /// <summary>
        /// Supplier Payment entry.
        /// </summary>
        public partial class SupplierPayment
        {
            [JsonProperty("amount")]
            public double Amount { get; set; }

            /// <summary>
            /// The amount in base currency.
            /// </summary>
            [JsonProperty("amountBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? AmountBaseCurrency { get; set; }

            /// <summary>
            /// The account used for the funds. Either 'supplier' or 'contraAccount' is required.
            /// </summary>
            [JsonProperty("contraAccount", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierPaymentContraAccount ContraAccount { get; set; }

            /// <summary>
            /// The account for VAT.
            /// </summary>
            [JsonProperty("contraVatAccount", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierPaymentContraVatAccount ContraVatAccount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry on the contra account.
            /// </summary>
            [JsonProperty("contraVatAmount", NullValueHandling = NullValueHandling.Ignore)]
            public double? ContraVatAmount { get; set; }

            /// <summary>
            /// The amount of VAT on the entry on the contra account in base currency.
            /// </summary>
            [JsonProperty("contraVatAmountInBaseCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? ContraVatAmountInBaseCurrency { get; set; }

            /// <summary>
            /// The currency for the entry.
            /// </summary>
            [JsonProperty("currency", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierPaymentCurrency Currency { get; set; }

            /// <summary>
            /// Entry date. Format according to ISO-8601 (YYYY-MM-DD).
            /// </summary>
            [JsonProperty("date")]
            public string Date { get; set; }

            /// <summary>
            /// A departmental distribution defines which departments this entry is distributed between.
            /// This requires the departments module to be enabled.
            /// </summary>
            [JsonProperty("departmentalDistribution", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierPaymentDepartmentalDistribution DepartmentalDistribution { get; set; }

            /// <summary>
            /// Employee that this entry is related to. Requires Project module
            /// </summary>
            [JsonProperty("employee", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierPaymentEmployee Employee { get; set; }

            /// <summary>
            /// Type of the journal entry. This is automatically set to supplierPayment.
            /// </summary>
            [JsonProperty("entryType", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierPaymentEntryType? EntryType { get; set; }

            /// <summary>
            /// The exchange rate between the base currency and the invoice currency.
            /// </summary>
            [JsonProperty("exchangeRate", NullValueHandling = NullValueHandling.Ignore)]
            public double? ExchangeRate { get; set; }

            /// <summary>
            /// The journal the voucher belongs to.
            /// </summary>
            [JsonProperty("journal", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierPaymentJournal Journal { get; set; }

            /// <summary>
            /// Remaining amount to be paid.
            /// </summary>
            [JsonProperty("remainder", NullValueHandling = NullValueHandling.Ignore)]
            public double? Remainder { get; set; }

            /// <summary>
            /// Remaining amount to be paid in default currency.
            /// </summary>
            [JsonProperty("remainderInDefaultCurrency", NullValueHandling = NullValueHandling.Ignore)]
            public double? RemainderInDefaultCurrency { get; set; }

            /// <summary>
            /// The supplier is the vendor from whom you buy your goods. Either 'supplier' or
            /// 'contraAccount' is required.
            /// </summary>
            [JsonProperty("supplier", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierPaymentSupplier Supplier { get; set; }

            /// <summary>
            /// The unique identifier of the supplier invoice gotten from the supplier.
            /// </summary>
            [JsonProperty("supplierInvoiceNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(IndecentMinMaxLengthCheckConverter))]
            public string SupplierInvoiceNumber { get; set; }

            /// <summary>
            /// Entry description.
            /// </summary>
            [JsonProperty("text", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxLengthCheckConverter))]
            public string Text { get; set; }

            /// <summary>
            /// Voucher that the entry belongs to.
            /// </summary>
            [JsonProperty("voucher", NullValueHandling = NullValueHandling.Ignore)]
            public SupplierPaymentVoucher Voucher { get; set; }
        }

        /// <summary>
        /// The account used for the funds. Either 'supplier' or 'contraAccount' is required.
        /// </summary>
        public partial class SupplierPaymentContraAccount
        {
            /// <summary>
            /// The account number.
            /// </summary>
            [JsonProperty("accountNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? AccountNumber { get; set; }

            /// <summary>
            /// A unique reference to the account resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The account for VAT.
        /// </summary>
        public partial class SupplierPaymentContraVatAccount
        {
            /// <summary>
            /// A unique link reference to the contraVatAccount item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The unique identifier of the contra vat account.
            /// </summary>
            [JsonProperty("vatCode", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxLengthCheckConverter))]
            public string VatCode { get; set; }
        }

        /// <summary>
        /// The currency for the entry.
        /// </summary>
        public partial class SupplierPaymentCurrency
        {
            /// <summary>
            /// The ISO 4217 3-letter currency code of the entry.
            /// </summary>
            [JsonProperty("code", NullValueHandling = NullValueHandling.Ignore)]
            public string Code { get; set; }

            /// <summary>
            /// A unique link reference to the currency item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// A departmental distribution defines which departments this entry is distributed between.
        /// This requires the departments module to be enabled.
        /// </summary>
        public partial class SupplierPaymentDepartmentalDistribution
        {
            /// <summary>
            /// A unique identifier of the departmental distribution.
            /// </summary>
            [JsonProperty("departmentalDistributionNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? DepartmentalDistributionNumber { get; set; }

            /// <summary>
            /// Type of the distribution
            /// </summary>
            [JsonProperty("distributionType", NullValueHandling = NullValueHandling.Ignore)]
            public string DistributionType { get; set; }

            /// <summary>
            /// A unique reference to the departmental distribution resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// Employee that this entry is related to. Requires Project module
        /// </summary>
        public partial class SupplierPaymentEmployee
        {
            /// <summary>
            /// A unique numerical identifier with a maximum of 9 digits.
            /// </summary>
            [JsonProperty("employeeNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? EmployeeNumber { get; set; }

            /// <summary>
            /// A unique link reference to the employee item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The journal the voucher belongs to.
        /// </summary>
        public partial class SupplierPaymentJournal
        {
            /// <summary>
            /// Journal number that must be at least 1.
            /// </summary>
            [JsonProperty("journalNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxValueCheckConverter))]
            public double? JournalNumber { get; set; }

            /// <summary>
            /// A unique reference to the journal resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// The supplier is the vendor from whom you buy your goods. Either 'supplier' or
        /// 'contraAccount' is required.
        /// </summary>
        public partial class SupplierPaymentSupplier
        {
            /// <summary>
            /// A unique self reference of the supplier.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// The supplier number is a positive unique numerical identifier with a maximum of 9 digits.
            /// </summary>
            [JsonProperty("supplierNumber", NullValueHandling = NullValueHandling.Ignore)]
            public long? SupplierNumber { get; set; }
        }

        /// <summary>
        /// Voucher that the entry belongs to.
        /// </summary>
        public partial class SupplierPaymentVoucher
        {
            /// <summary>
            /// A unique link reference to the voucher item.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }

            /// <summary>
            /// Journal voucher number must be between 1-999999999.
            /// </summary>
            [JsonProperty("voucherNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(FluffyMinMaxValueCheckConverter))]
            public int? VoucherNumber { get; set; }
        }

        /// <summary>
        /// The journal the voucher belongs to.
        /// </summary>
        public partial class CoordinateJournal
        {
            /// <summary>
            /// Journal number that must be at least 1.
            /// </summary>
            [JsonProperty("journalNumber", NullValueHandling = NullValueHandling.Ignore)]
            [JsonConverter(typeof(PurpleMinMaxValueCheckConverter))]
            public int? JournalNumber { get; set; }

            /// <summary>
            /// A unique reference to the journal resource.
            /// </summary>
            [JsonProperty("self", NullValueHandling = NullValueHandling.Ignore)]
            public Uri Self { get; set; }
        }

        /// <summary>
        /// Type of the journal entry. This is automatically set to customerPayment.
        /// </summary>
        public enum CustomerPaymentEntryType { CustomerPayment };

        /// <summary>
        /// Type of the journal entry. This is automatically set to financeVoucher.
        /// </summary>
        public enum FinanceVoucherEntryType { FinanceVoucher };

        /// <summary>
        /// Type of the journal entry. This is automatically set to manualCustomerInvoice.
        /// </summary>
        public enum ManualCustomerInvoiceEntryType { ManualCustomerInvoice };

        /// <summary>
        /// Type of the journal entry. This is automatically set to supplierInvoice.
        /// </summary>
        public enum SupplierInvoiceEntryType { SupplierInvoice };

        /// <summary>
        /// Type of the journal entry. This is automatically set to supplierPayment.
        /// </summary>
        public enum SupplierPaymentEntryType { SupplierPayment };

        internal static class Converter
        {
            public static readonly JsonSerializerSettings Settings = new JsonSerializerSettings
            {
                MetadataPropertyHandling = MetadataPropertyHandling.Ignore,
                DateParseHandling = DateParseHandling.None,
                Converters =
            {
                CustomerPaymentEntryTypeConverter.Singleton,
                FinanceVoucherEntryTypeConverter.Singleton,
                ManualCustomerInvoiceEntryTypeConverter.Singleton,
                SupplierInvoiceEntryTypeConverter.Singleton,
                SupplierPaymentEntryTypeConverter.Singleton,
                new IsoDateTimeConverter { DateTimeStyles = DateTimeStyles.AssumeUniversal }
            },
            };
        }

        internal class PurpleMinMaxLengthCheckConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(string);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                var value = serializer.Deserialize<string>(reader);
                if (value.Length <= 5)
                {
                    return value;
                }
                throw new Exception("Cannot unmarshal type string");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (string)untypedValue;
                if (value.Length <= 5)
                {
                    serializer.Serialize(writer, value);
                    return;
                }
                throw new Exception("Cannot marshal type string");
            }

            public static readonly PurpleMinMaxLengthCheckConverter Singleton = new PurpleMinMaxLengthCheckConverter();
        }

        internal class CustomerPaymentEntryTypeConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(CustomerPaymentEntryType) || t == typeof(CustomerPaymentEntryType?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "customerPayment")
                {
                    return CustomerPaymentEntryType.CustomerPayment;
                }
                throw new Exception("Cannot unmarshal type CustomerPaymentEntryType");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (CustomerPaymentEntryType)untypedValue;
                if (value == CustomerPaymentEntryType.CustomerPayment)
                {
                    serializer.Serialize(writer, "customerPayment");
                    return;
                }
                throw new Exception("Cannot marshal type CustomerPaymentEntryType");
            }

            public static readonly CustomerPaymentEntryTypeConverter Singleton = new CustomerPaymentEntryTypeConverter();
        }

        internal class PurpleMinMaxValueCheckConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(double) || t == typeof(double?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<double>(reader);
                if (value >= 1)
                {
                    return value;
                }
                throw new Exception("Cannot unmarshal type double");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (int)untypedValue;
                if (value >= 1)
                {
                    serializer.Serialize(writer, value);
                    return;
                }
                throw new Exception("Cannot marshal type int");
            }

            public static readonly PurpleMinMaxValueCheckConverter Singleton = new PurpleMinMaxValueCheckConverter();
        }

        internal class FluffyMinMaxLengthCheckConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(string);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                var value = serializer.Deserialize<string>(reader);
                if (value.Length <= 250)
                {
                    return value;
                }
                throw new Exception("Cannot unmarshal type string");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (string)untypedValue;
                if (value.Length <= 250)
                {
                    serializer.Serialize(writer, value);
                    return;
                }
                throw new Exception("Cannot marshal type string");
            }

            public static readonly FluffyMinMaxLengthCheckConverter Singleton = new FluffyMinMaxLengthCheckConverter();
        }

        internal class FluffyMinMaxValueCheckConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(double) || t == typeof(double?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<double>(reader);
                if (value >= 1 && value <= 999999999)
                {
                    return value;
                }
                throw new Exception("Cannot unmarshal type double");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (int)untypedValue;
                if (value >= 1 && value <= 999999999)
                {
                    serializer.Serialize(writer, value);
                    return;
                }
                throw new Exception("Cannot marshal type double");
            }

            public static readonly FluffyMinMaxValueCheckConverter Singleton = new FluffyMinMaxValueCheckConverter();
        }

        internal class FinanceVoucherEntryTypeConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(FinanceVoucherEntryType) || t == typeof(FinanceVoucherEntryType?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "financeVoucher")
                {
                    return FinanceVoucherEntryType.FinanceVoucher;
                }
                throw new Exception("Cannot unmarshal type FinanceVoucherEntryType");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (FinanceVoucherEntryType)untypedValue;
                if (value == FinanceVoucherEntryType.FinanceVoucher)
                {
                    serializer.Serialize(writer, "financeVoucher");
                    return;
                }
                throw new Exception("Cannot marshal type FinanceVoucherEntryType");
            }

            public static readonly FinanceVoucherEntryTypeConverter Singleton = new FinanceVoucherEntryTypeConverter();
        }

        internal class ManualCustomerInvoiceEntryTypeConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(ManualCustomerInvoiceEntryType) || t == typeof(ManualCustomerInvoiceEntryType?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "manualCustomerInvoice")
                {
                    return ManualCustomerInvoiceEntryType.ManualCustomerInvoice;
                }
                throw new Exception("Cannot unmarshal type ManualCustomerInvoiceEntryType");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (ManualCustomerInvoiceEntryType)untypedValue;
                if (value == ManualCustomerInvoiceEntryType.ManualCustomerInvoice)
                {
                    serializer.Serialize(writer, "manualCustomerInvoice");
                    return;
                }
                throw new Exception("Cannot marshal type ManualCustomerInvoiceEntryType");
            }

            public static readonly ManualCustomerInvoiceEntryTypeConverter Singleton = new ManualCustomerInvoiceEntryTypeConverter();
        }

        internal class SupplierInvoiceEntryTypeConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(SupplierInvoiceEntryType) || t == typeof(SupplierInvoiceEntryType?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "supplierInvoice")
                {
                    return SupplierInvoiceEntryType.SupplierInvoice;
                }
                throw new Exception("Cannot unmarshal type SupplierInvoiceEntryType");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (SupplierInvoiceEntryType)untypedValue;
                if (value == SupplierInvoiceEntryType.SupplierInvoice)
                {
                    serializer.Serialize(writer, "supplierInvoice");
                    return;
                }
                throw new Exception("Cannot marshal type SupplierInvoiceEntryType");
            }

            public static readonly SupplierInvoiceEntryTypeConverter Singleton = new SupplierInvoiceEntryTypeConverter();
        }

        internal class TentacledMinMaxLengthCheckConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(string);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                var value = serializer.Deserialize<string>(reader);
                if (value.Length >= 5 && value.Length <= 100)
                {
                    return value;
                }
                throw new Exception("Cannot unmarshal type string");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (string)untypedValue;
                if (value.Length >= 5 && value.Length <= 100)
                {
                    serializer.Serialize(writer, value);
                    return;
                }
                throw new Exception("Cannot marshal type string");
            }

            public static readonly TentacledMinMaxLengthCheckConverter Singleton = new TentacledMinMaxLengthCheckConverter();
        }

        internal class StickyMinMaxLengthCheckConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(string);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                var value = serializer.Deserialize<string>(reader);
                if (value.Length >= 15 && value.Length <= 16)
                {
                    return value;
                }
                throw new Exception("Cannot unmarshal type string");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (string)untypedValue;
                if (value.Length >= 15 && value.Length <= 16)
                {
                    serializer.Serialize(writer, value);
                    return;
                }
                throw new Exception("Cannot marshal type string");
            }

            public static readonly StickyMinMaxLengthCheckConverter Singleton = new StickyMinMaxLengthCheckConverter();
        }

        internal class IndigoMinMaxLengthCheckConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(string);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                var value = serializer.Deserialize<string>(reader);
                if (value.Length >= 16 && value.Length <= 157)
                {
                    return value;
                }
                throw new Exception("Cannot unmarshal type string");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (string)untypedValue;
                if (value.Length >= 16 && value.Length <= 157)
                {
                    serializer.Serialize(writer, value);
                    return;
                }
                throw new Exception("Cannot marshal type string");
            }

            public static readonly IndigoMinMaxLengthCheckConverter Singleton = new IndigoMinMaxLengthCheckConverter();
        }

        internal class IndecentMinMaxLengthCheckConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(string);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                var value = serializer.Deserialize<string>(reader);
                if (value.Length <= 30)
                {
                    return value;
                }
                throw new Exception("Cannot unmarshal type string");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                var value = (string)untypedValue;
                if (value.Length <= 30)
                {
                    serializer.Serialize(writer, value);
                    return;
                }
                throw new Exception("Cannot marshal type string");
            }

            public static readonly IndecentMinMaxLengthCheckConverter Singleton = new IndecentMinMaxLengthCheckConverter();
        }

        internal class SupplierPaymentEntryTypeConverter : JsonConverter
        {
            public override bool CanConvert(Type t) => t == typeof(SupplierPaymentEntryType) || t == typeof(SupplierPaymentEntryType?);

            public override object ReadJson(JsonReader reader, Type t, object existingValue, JsonSerializer serializer)
            {
                if (reader.TokenType == JsonToken.Null) return null;
                var value = serializer.Deserialize<string>(reader);
                if (value == "supplierPayment")
                {
                    return SupplierPaymentEntryType.SupplierPayment;
                }
                throw new Exception("Cannot unmarshal type SupplierPaymentEntryType");
            }

            public override void WriteJson(JsonWriter writer, object untypedValue, JsonSerializer serializer)
            {
                if (untypedValue == null)
                {
                    serializer.Serialize(writer, null);
                    return;
                }
                var value = (SupplierPaymentEntryType)untypedValue;
                if (value == SupplierPaymentEntryType.SupplierPayment)
                {
                    serializer.Serialize(writer, "supplierPayment");
                    return;
                }
                throw new Exception("Cannot marshal type SupplierPaymentEntryType");
            }

            public static readonly SupplierPaymentEntryTypeConverter Singleton = new SupplierPaymentEntryTypeConverter();
        }
    }
}