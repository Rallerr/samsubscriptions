﻿namespace SAM.Economic.Integration.Client.Models
{
    public class PaymentTerms
    {
        public class Rootobject
        {
            public Collection[] collection { get; set; }
            public Pagination pagination { get; set; }
            public string self { get; set; }
        }

        public class Pagination
        {
            public int maxPageSizeAllowed { get; set; }
            public int skipPages { get; set; }
            public int pageSize { get; set; }
            public int results { get; set; }
            public int resultsWithoutFilter { get; set; }
            public string firstPage { get; set; }
            public string lastPage { get; set; }
        }

        public class Collection
        {
            public int paymentTermsNumber { get; set; }
            public int daysOfCredit { get; set; }
            public string name { get; set; }
            public string paymentTermsType { get; set; }
            public string self { get; set; }
        }
    }
}