﻿namespace SAM.Economic.Integration.Client.Models
{
    public class VatZones
    {
        public class Rootobject
        {
            public Collection[] collection { get; set; }
            public Pagination pagination { get; set; }
            public string self { get; set; }
        }

        public class Pagination
        {
            public int maxPageSizeAllowed { get; set; }
            public int skipPages { get; set; }
            public int pageSize { get; set; }
            public int results { get; set; }
            public int resultsWithoutFilter { get; set; }
            public string firstPage { get; set; }
            public string lastPage { get; set; }
        }

        public class Collection
        {
            public string name { get; set; }
            public int vatZoneNumber { get; set; }
            public bool enabledForCustomer { get; set; }
            public bool enabledForSupplier { get; set; }
            public string self { get; set; }
        }
    }
}