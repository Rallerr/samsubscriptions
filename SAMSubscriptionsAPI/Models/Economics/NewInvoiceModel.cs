﻿namespace SAM.Economic.Integration.Client.Models
{
    public class NewInvoiceModel
    {
        public class DraftInvoice
        {
            public string currency { get; set; }
            public Customer customer { get; set; }
            public string date { get; set; }
            public string dueDate { get; set; }
            public Layout layout { get; set; }
            public Line[] Lines { get; set; }
            public PaymentTerms paymentTerms { get; set; }
            public Recipient recipient { get; set; }
        }

        public class Line
        {
            public string Description { get; set; }
            public double DiscountPercentage { get; set; }
            public Product Product { get; set; }
            public float Quantity { get; set; }
            public double TotalNetAmount { get; set; }
            public Unit Unit { get; set; }
            public double UnitCostPrice { get; set; }
            public double UnitNetPrice { get; set; }
        }

        public class Product
        {
            public int ProductNumber { get; set; }
        }

        public class PaymentTerms
        {
            public int paymentTermsNumber { get; set; }
        }

        public class Customer
        {
            public int customerNumber { get; set; }
        }

        public class Recipient
        {
            public string address { get; set; }
            public string city { get; set; }
            public string name { get; set; }
            public Vatzone vatZone { get; set; }
            public string zip { get; set; }
        }

        public class Unit
        {
            public int unitNumber { get; set; }
        }

        public class Vatzone
        {
            public int vatZoneNumber { get; set; }
        }

        public class Layout
        {
            public int layoutNumber { get; set; }
        }
    }
}