﻿namespace SAM.Economic.Integration.Client.Models
{
    public class JournalPostback
    {
        public class Rootobject
        {
            public Class1[] Property1 { get; set; }
        }

        public class Class1
        {
            public Accountingyear accountingYear { get; set; }
            public Journal journal { get; set; }
            public Entries entries { get; set; }
            public int voucherNumber { get; set; }
            public string attachment { get; set; }
            public string self { get; set; }
        }

        public class Accountingyear
        {
            public string year { get; set; }
            public string self { get; set; }
        }

        public class Journal
        {
            public int journalNumber { get; set; }
            public string self { get; set; }
        }

        public class Entries
        {
            public Financevoucher[] financeVouchers { get; set; }
            public Customerpayment[] customerPayments { get; set; }
        }

        public class Financevoucher
        {
            public Account account { get; set; }
            public string text { get; set; }
            public Journal1 journal { get; set; }
            public decimal amount { get; set; }
            public Currency currency { get; set; }
            public string date { get; set; }
            public decimal exchangeRate { get; set; }
            public string entryType { get; set; }
            public Voucher voucher { get; set; }
            public decimal amountDefaultCurrency { get; set; }
            public decimal remainder { get; set; }
            public decimal remainderDefaultCurrency { get; set; }
            public int journalEntryNumber { get; set; }
            public Metadata metaData { get; set; }
            public string self { get; set; }
        }

        public class Account
        {
            public int accountNumber { get; set; }
            public string self { get; set; }
        }

        public class Journal1
        {
            public int journalNumber { get; set; }
            public string self { get; set; }
        }

        public class Currency
        {
            public string code { get; set; }
            public string self { get; set; }
        }

        public class Voucher
        {
            public Accountingyear1 accountingYear { get; set; }
            public int voucherNumber { get; set; }
            public string attachment { get; set; }
            public string self { get; set; }
        }

        public class Accountingyear1
        {
            public string year { get; set; }
            public string self { get; set; }
        }

        public class Metadata
        {
            public Delete delete { get; set; }
        }

        public class Delete
        {
            public string description { get; set; }
            public string href { get; set; }
            public string httpMethod { get; set; }
        }

        public class Customerpayment
        {
            public Customer customer { get; set; }
            public string text { get; set; }
            public Journal2 journal { get; set; }
            public decimal amount { get; set; }
            public Currency1 currency { get; set; }
            public string date { get; set; }
            public decimal exchangeRate { get; set; }
            public string entryType { get; set; }
            public Voucher1 voucher { get; set; }
            public decimal amountDefaultCurrency { get; set; }
            public decimal remainder { get; set; }
            public decimal remainderDefaultCurrency { get; set; }
            public int journalEntryNumber { get; set; }
            public Metadata1 metaData { get; set; }
            public string self { get; set; }
        }

        public class Customer
        {
            public int customerNumber { get; set; }
            public string self { get; set; }
        }

        public class Journal2
        {
            public int journalNumber { get; set; }
            public string self { get; set; }
        }

        public class Currency1
        {
            public string code { get; set; }
            public string self { get; set; }
        }

        public class Voucher1
        {
            public Accountingyear2 accountingYear { get; set; }
            public int voucherNumber { get; set; }
            public string attachment { get; set; }
            public string self { get; set; }
        }

        public class Accountingyear2
        {
            public string year { get; set; }
            public string self { get; set; }
        }

        public class Metadata1
        {
            public Delete1 delete { get; set; }
        }

        public class Delete1
        {
            public string description { get; set; }
            public string href { get; set; }
            public string httpMethod { get; set; }
        }
    }
}