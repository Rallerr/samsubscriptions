using WebApi.Entities;

namespace WebApi.Models.Users
{
    public class AuthenticateResponse
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public Role Role { get; set; }
        public string Token { get; set; }
        public string EnterpriseId { get; set; }

        public AuthenticateResponse(User user, string token)
        {
            Id = user.Id;
            Username = user.Username;
            Role = user.Role;
            Token = token;
            EnterpriseId = user.EnterpriseId;
        }
    }
}