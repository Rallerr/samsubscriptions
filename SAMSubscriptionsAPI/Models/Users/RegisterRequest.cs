﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Entities;

namespace WebApi.Models.Users
{
    public class RegisterRequest
    {
        public string Id { get; set; }
        public string Username { get; set; }
        public Role Role { get; set; }
        public string Password { get; set; }
        public string EnterpriseId { get; set; }
    }
}
