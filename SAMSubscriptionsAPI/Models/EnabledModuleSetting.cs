﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SAMSubscriptionsAPI.Models
{
    public class EnabledModuleSetting
    {
        public string Id { get; set; }
        public string ModuleId { get; set; }
        public string SettingId { get; set; }
    }
}
