﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ModuleSettingController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public ModuleSettingController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<ModuleSettingController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ModuleSetting>>> Get()
        {
            var moduleSettings = await _context.ModuleSetting.ToListAsync();

            if(moduleSettings == null)
            {
                return NotFound();
            }

            return moduleSettings;
        }

        // GET: api/<ModuleSettingController>
        [HttpGet]
        [Route("moduleId/{moduleId}")]
        public async Task<ActionResult<IEnumerable<ModuleSetting>>> GetByModuleID(string moduleId)
        {
            var moduleSettings = await _context.ModuleSetting.Where(ms => ms.ModuleId == moduleId).ToListAsync();

            if (moduleSettings == null)
            {
                return NotFound();
            }

            return moduleSettings;
        }

        // GET api/<ModuleSettingController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ModuleSetting>> Get(string id)
        {
            var moduleSetting = await _context.ModuleSetting.FindAsync(id);

            if (moduleSetting == null)
            {
                return NotFound();
            }

            return moduleSetting;
        }

        // POST api/<ModuleSettingController>
        [HttpPost]
        public async Task<ActionResult<ModuleSetting>> Post([FromBody] ModuleSetting value)
        {
            var moduleSettingExists = await _context.ModuleSetting
                .FirstOrDefaultAsync(m => m.ModuleId == value.ModuleId && m.Name == value.Name);

            if (moduleSettingExists != null)
            {
                return StatusCode(403, "Already exists");
            }

            value.Id = Guid.NewGuid().ToString();
            var res = await _context.ModuleSetting.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<ModuleSettingController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] ModuleSetting value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<ModuleSettingController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var moduleSetting = await _context.ModuleSetting.FindAsync(id);
            if (moduleSetting == null)
            {
                return NotFound();
            }

            _context.ModuleSetting.Remove(moduleSetting);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
