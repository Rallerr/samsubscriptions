﻿using Microsoft.AspNetCore.Mvc;
using SAM.Economic.Integration.Client.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;
using WebApi.Models.Economics;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EconomicsProductGroupController : ControllerBase
    {

        RestFunctions restFunctions = new RestFunctions();

        // GET: api/<EconomicsProductGroupController>
        [HttpGet]
        public ActionResult<IEnumerable<ProductGroup>> Get()
        {
            var productGroups = restFunctions.GetProductGroups();

            if (productGroups == null)
            {
                return NotFound();
            }

            return Ok(productGroups);
        }

        // GET api/<EconomicsProductGroupController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<EconomicsProductGroupController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<EconomicsProductGroupController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EconomicsProductGroupController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
