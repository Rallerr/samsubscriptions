﻿using Microsoft.AspNetCore.Mvc;
using SAM.Economic.Integration.Client.Logic;
using SAM.Economic.Integration.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EconomicsBookedInvoiceController : ControllerBase
    {

        RestFunctions restFunctions = new RestFunctions();
        // GET: api/<EconomicsBookedInvoiceController>
        [HttpGet]
        [Route("customerNumber/{customerNumber}")]
        public ActionResult<IEnumerable<BookedInvoice>> GetByCustomerNumber(int customerNumber)
        {
            var invoices = restFunctions.GetInvoices(customerNumber);

            if (invoices == null)
            {
                return NotFound();
            }

            return Ok(invoices.Collection);
        }

        // GET api/<EconomicsBookedInvoiceController>/5
        [HttpGet("download/{invoiceNumber}")]
        public ActionResult<byte[]> Get(int invoiceNumber)
        {
            var fileArray = restFunctions.GetInvoicePDF($"https://restapi.e-conomic.com/invoices/booked/{invoiceNumber}/pdf");

            if (fileArray == null)
            {
                return NotFound();
            }

            return Ok(fileArray);
        }
    }
}
