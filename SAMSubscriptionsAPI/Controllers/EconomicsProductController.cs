﻿using Microsoft.AspNetCore.Mvc;
using SAM.Economic.Integration.Client.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;
using WebApi.Models.Economics;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EconomicsProductController : ControllerBase
    {
        RestFunctions restFunctions = new RestFunctions();

        // GET: api/<EconomicsProductController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Product>>> Get()
        {
            var products = await restFunctions.GetProducts();

            if (products == null)
            {
                return NotFound();
            }
            return products;
        }

        // GET api/<EconomicsProductController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> Get(string id)
        {
            var product = await restFunctions.GetProduct(id);

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        // POST api/<EconomicsProductController>
        [HttpPost]
        public async Task<ActionResult<Product>> Post([FromBody] Product value)
        {

            var products = await restFunctions.GetProducts();

            var product = products.Where(p => p.name == value.name).FirstOrDefault();

            if (product != null)
            {
                return BadRequest("Product already exists");
            }

            value.productNumber = await restFunctions.GetProductNumber();

            var createdProduct = await restFunctions.CreateProduct(value);
            return createdProduct;
        }

        // PUT api/<EconomicsProductController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<Product>> Put(string id, [FromBody] Product product)
        {
            var updatedProduct = await restFunctions.UpdateProductAsync(id, product);

            if (updatedProduct == null)
            {
                return NotFound();
            }

            return Ok(updatedProduct);

        }

        // DELETE api/<EconomicsProductController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
