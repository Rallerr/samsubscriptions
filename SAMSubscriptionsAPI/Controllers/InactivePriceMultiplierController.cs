﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InactivePriceMultiplierController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public InactivePriceMultiplierController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<InactivePriceMultiplierController>/subscritionId/{subscriptionId}
        [HttpGet]
        [Route("subscriptionId/{subscriptionId}")]
        public async Task<ActionResult<IEnumerable<InactivePriceMultiplier>>> GetBySubscriptionId(string subscriptionId)
        {
            var inactivePriceMultipliers = await _context.InactivePriceMultiplier.Where(m => m.SubscriptionId == subscriptionId).ToListAsync();

            if (inactivePriceMultipliers == null)
            {
                return NotFound();
            }
            return inactivePriceMultipliers;
        }

        // GET api/<InactivePriceMultiplierController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<InactivePriceMultiplier>> Get(string id)
        {
            var inactivePriceMultiplier = await _context.InactivePriceMultiplier.FindAsync(id);

            if (inactivePriceMultiplier == null)
            {
                return NotFound();
            }

            return inactivePriceMultiplier;
        }

        // POST api/<InactivePriceMultiplierController>
        [HttpPost]
        public async Task<ActionResult<InactivePriceMultiplier>> Post([FromBody] InactivePriceMultiplier value)
        {
            var priceMultiplier = await _context.PriceMultiplier.Where(m => m.Id == value.PriceMultiplierId).FirstOrDefaultAsync();

            if (priceMultiplier == null)
            {
                return BadRequest();
            }


            value.Id = Guid.NewGuid().ToString();
            var res = await _context.InactivePriceMultiplier.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<InactivePriceMultiplierController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] InactivePriceMultiplier value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<InactivePriceMultiplierController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var inactivePriceMultiplier = await _context.InactivePriceMultiplier.FindAsync(id);
            if (inactivePriceMultiplier == null)
            {
                return NotFound();
            }

            _context.InactivePriceMultiplier.Remove(inactivePriceMultiplier);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
