﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SubscriptionTypeController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public SubscriptionTypeController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<SubscriptionTypeController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SubscriptionType>>> Get()
        {
            var subscriptionTypes = await _context.SubscriptionType.ToListAsync();

            if (subscriptionTypes == null)
            {
                return NotFound();
            }

            return subscriptionTypes;
        }

        // GET api/<SubscriptionTypeController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<SubscriptionType>> Get(string id)
        {
            var subscriptionType = await _context.SubscriptionType.FindAsync(id);

            if (subscriptionType == null)
            {
                return NotFound();
            }

            return subscriptionType;
        }

        // POST api/<SubscriptionTypeController>
        [HttpPost]
        public async Task<ActionResult<SubscriptionType>> Post([FromBody] SubscriptionType value)
        {
            value.Id = Guid.NewGuid().ToString();
            var res = await _context.SubscriptionType.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<SubscriptionTypeController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] SubscriptionType value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<SubscriptionTypeController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var subscriptionType = await _context.SubscriptionType.FindAsync(id);
            if (subscriptionType == null)
            {
                return NotFound();
            }

            _context.SubscriptionType.Remove(subscriptionType);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
