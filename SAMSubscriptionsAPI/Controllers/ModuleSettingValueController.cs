﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ModuleSettingValueController : ControllerBase
    {
        private readonly SAMSubscriptionsDbContext _context;
        public ModuleSettingValueController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<ModuleSettingValueController>
        [HttpGet]
        [Route("active/{moduleSettingId}/{activeModuleId}")]
        public async Task<ActionResult<ModuleSettingValue>> GetActiveSettingValues(string moduleSettingId, string activeModuleId)
        {
            var activeModuleSettingValues = await _context.ModuleSettingValue
                .Where(ms => ms.ModuleSettingId == moduleSettingId && ms.ActiveModuleId == activeModuleId)
                .FirstOrDefaultAsync();

            if (activeModuleSettingValues == null)
            {
                return NotFound();
            }

            return activeModuleSettingValues;
        }

        [HttpGet]
        [Route("active/{activeModuleId}")]
        public async Task<ActionResult<IEnumerable<ModuleSettingValue>>> GetActiveSettingValues(string activeModuleId)
        {
            var activeModuleSettingValues = await _context.ModuleSettingValue.Where(ms => ms.ActiveModuleId == activeModuleId)
                .ToListAsync();

            if (activeModuleSettingValues == null)
            {
                return NotFound();
            }

            return activeModuleSettingValues;
        }

        // GET: api/<ModuleSettingValueController>
        [HttpGet]
        [Route("inactive/{moduleSettingId}/{inactiveModuleId}")]
        public async Task<ActionResult<ModuleSettingValue>> GetInactiveSettingValues(string moduleSettingId, string inactiveModuleId)
        {
            var inactiveModuleSettingValues = await _context.ModuleSettingValue
                .Where(ms => ms.ModuleSettingId == moduleSettingId && ms.ActiveModuleId == inactiveModuleId)
                .FirstOrDefaultAsync();

            if (inactiveModuleSettingValues == null)
            {
                return NotFound();
            }

            return inactiveModuleSettingValues;
        }

        [HttpGet]
        [Route("inactive/{inactiveModuleId}")]
        public async Task<ActionResult<IEnumerable<ModuleSettingValue>>> GetInactiveSettingValues(string inactiveModuleId)
        {
            var inactiveModuleSettingValues = await _context.ModuleSettingValue.Where(ms => ms.InactiveModuleId == inactiveModuleId)
                .ToListAsync();

            if (inactiveModuleSettingValues == null)
            {
                return NotFound();
            }

            return inactiveModuleSettingValues;
        }


        // POST api/<ModuleSettingValueController>
        [HttpPost]
        public async Task<ActionResult<ModuleSettingValue>> Post([FromBody] ModuleSettingValue value)
        {
            var settingValueExists = await _context.ModuleSettingValue
                .Where(s => s.ModuleSettingId == value.ModuleSettingId && s.ActiveModuleId == value.ActiveModuleId).FirstOrDefaultAsync();

            value.Id = Guid.NewGuid().ToString();
            var res = await _context.ModuleSettingValue.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<ModuleSettingValueController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] ModuleSettingValue value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<ModuleSettingValueController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var moduleSettingValue = await _context.ModuleSettingValue.FindAsync(id);
            if (moduleSettingValue == null)
            {
                return NotFound();
            }

            _context.ModuleSettingValue.Remove(moduleSettingValue);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
