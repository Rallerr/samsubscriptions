﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PriceMultiplierController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public PriceMultiplierController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<PriceMultiplierController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PriceMultiplier>>> Get()
        {
            var pricemultipliers = await _context.PriceMultiplier.ToListAsync();
            
            if (pricemultipliers == null)
            {
                return NotFound();
            }

            return pricemultipliers;
        }

        // GET api/<PriceMultiplierController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PriceMultiplier>> Get(string id)
        {
            var pricemultiplier = await _context.PriceMultiplier.FindAsync(id);

            if (pricemultiplier == null)
            {
                return NotFound();
            }

            return pricemultiplier;
        }

        // POST api/<PriceMultiplierController>
        [HttpPost]
        public async Task<ActionResult<PriceMultiplier>> Post([FromBody] PriceMultiplier value)
        {
            value.Id = Guid.NewGuid().ToString();
            var res = await _context.PriceMultiplier.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<PriceMultiplierController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] PriceMultiplier value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<PriceMultiplierController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var priceMultiplier = await _context.PriceMultiplier.FindAsync(id);
            if (priceMultiplier == null)
            {
                return NotFound();
            }

            _context.PriceMultiplier.Remove(priceMultiplier);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
