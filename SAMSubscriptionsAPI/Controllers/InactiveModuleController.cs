﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InactiveModuleController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public InactiveModuleController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<ActiveModuleController>/enterpriseId/{enterpriseId}
        [HttpGet]
        [Route("enterpriseId/{enterpriseId}")]
        public async Task<ActionResult<IEnumerable<InactiveModule>>> GetByEnterpriseId(string enterpriseId)
        {
            var inactiveModules = await _context.InactiveModule.Where(m => m.EnterpriseId == enterpriseId).ToListAsync();

            if (inactiveModules== null)
            {
                return NotFound();
            }
            return inactiveModules;
        }

        // GET: api/<ActiveModuleController>/subscritionId/{subscriptionId}
        [HttpGet]
        [Route("subscriptionId/{subscriptionId}")]
        public async Task<ActionResult<IEnumerable<InactiveModule>>> GetBySubscriptionId(string subscriptionId)
        {
            var inactiveModules = await _context.InactiveModule.Where(m => m.SubscriptionId == subscriptionId).ToListAsync();

            if (inactiveModules == null)
            {
                return NotFound();
            }
            return inactiveModules;
        }

        [HttpGet]
        [Route("notpaid/{subscriptionId}")]
        public async Task<ActionResult<IEnumerable<InactiveModule>>> GetByNotPaid(string subscriptionId)
        {
            var notPaidInactiveModules = await _context.InactiveModule.Where(m => m.SubscriptionId == subscriptionId && m.Paid == false).ToListAsync();

            if (notPaidInactiveModules == null)
            {
                return NotFound();
            }
            return notPaidInactiveModules;
        }

        // GET api/<InactiveModuleController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<InactiveModule>> Get(string id)
        {
            var inactiveModule = await _context.InactiveModule.FindAsync(id);

            if (inactiveModule == null)
            {
                return NotFound();
            }

            return inactiveModule;
        }

        // POST api/<InactiveModuleController>
        [HttpPost]
        public async Task<ActionResult<InactiveModule>> Post([FromBody] InactiveModule value)
        {
            var moduleExists = await _context.Module.Where(m => m.Id == value.ModuleId).FirstOrDefaultAsync();

            if (moduleExists == null)
            {
                return BadRequest();
            }

            value.Id = Guid.NewGuid().ToString();
            var res = await _context.InactiveModule.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<InactiveModuleController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] InactiveModule value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<InactiveModuleController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var inactiveModule = await _context.InactiveModule.FindAsync(id);
            if (inactiveModule == null)
            {
                return NotFound();
            }

            _context.InactiveModule.Remove(inactiveModule);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
