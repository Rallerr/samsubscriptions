﻿using Microsoft.AspNetCore.Mvc;
using SAM.Economic.Integration.Client.Logic;
using SAM.Economic.Integration.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EconomicsPaymentTermsController : ControllerBase
    {
        RestFunctions restFunctions = new RestFunctions();
        // GET: api/<EconomicsPaymentTermsController>
        [HttpGet]
        public ActionResult<IEnumerable<PaymentTerms.Collection[]>> Get()
        {
            var paymentTerms = restFunctions.GetPaymentTerms();

            if (paymentTerms == null)
            {
                return NotFound();
            }

            return Ok(paymentTerms);
        }

        // GET api/<EconomicsPaymentTermsController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<EconomicsPaymentTermsController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<EconomicsPaymentTermsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EconomicsPaymentTermsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
