﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ActiveModuleController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public ActiveModuleController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<ActiveModuleController>/enterpriseId/{enterpriseId}
        [HttpGet]
        [Route("enterpriseId/{enterpriseId}")]
        public async Task<ActionResult<IEnumerable<ActiveModule>>> GetByEnterpriseId(string enterpriseId)
        {
            var activeModules = await _context.ActiveModule.Where(m => m.EnterpriseId == enterpriseId).ToListAsync();

            if (activeModules == null)
            {
                return NotFound();
            }
            return activeModules;
        }

        // GET: api/<ActiveModuleController>/subscritionId/{subscriptionId}
        [HttpGet]
        [Route("subscriptionId/{subscriptionId}")]
        public async Task<ActionResult<IEnumerable<ActiveModule>>> GetBySubscriptionId(string subscriptionId)
        {
            var activeModules = await _context.ActiveModule.Where(m => m.SubscriptionId == subscriptionId).ToListAsync();

            if (activeModules == null)
            {
                return NotFound();
            }
            return activeModules;
        }

        // GET api/<ActiveModuleController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActiveModule>> Get(string id)
        {
            var activeModule = await _context.ActiveModule.FindAsync(id);

            if (activeModule == null)
            {
                return NotFound();
            }

            return activeModule;
        }

        // POST api/<ActiveModuleController>
        [HttpPost]
        public async Task<ActionResult<ActiveModule>> Post([FromBody] ActiveModule value)
        {
            var moduleExists = await _context.Module.Where(m => m.Id == value.ModuleId).FirstOrDefaultAsync();

            if (moduleExists == null)
            {
                return BadRequest();
            }

            var activeModuleExists = await _context.ActiveModule
                .Where(m => m.ModuleId == value.ModuleId && m.SubscriptionId == value.SubscriptionId)
                .FirstOrDefaultAsync();

            if (activeModuleExists != null)
            {
                return StatusCode(403, "Already exists");
            }


            value.Id = Guid.NewGuid().ToString();
            var res = await _context.ActiveModule.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<ActiveModuleController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] ActiveModule value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<ActiveModuleController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var activeModule = await _context.ActiveModule.FindAsync(id);
            if (activeModule == null)
            {
                return NotFound();
            }

            _context.ActiveModule.Remove(activeModule);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
