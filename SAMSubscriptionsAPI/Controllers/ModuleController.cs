﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;
using WebApi.Entities;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ModuleController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public ModuleController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<ModuleController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Module>>> Get()
        {
            var modules = await _context.Module.ToListAsync();

            if (modules == null)
            {
                return NotFound();
            }

            return modules;
        }

        [HttpGet]
        [Route("samModules/")]
        public async Task<ActionResult<IEnumerable<Module>>> GetSAMModules()
        {
            var modules = await _context.Module.Where(m => m.SAMModule == true).ToListAsync();

            if (modules == null)
            {
                return NotFound();
            }

            return modules;
        }

        // GET: api/<ModuleController>
        [HttpPost]
        [Route("samModules/moduleIdList/")]
        public async Task<ActionResult<IEnumerable<Module>>> GetSAMModulesByModuleIdList([FromBody] List<string> moduleIds)
        {
            List<Module> modules = new List<Module>();

            foreach (var id in moduleIds)
            {
                modules.Add(await _context.Module.Where(m => m.Id == id && m.SAMModule == true).FirstOrDefaultAsync());
            }

            if (modules == null)
            {
                return NotFound();
            }

            return modules;
        }

        // GET api/<ModuleController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Module>> Get(string id)
        {
            var module = await _context.Module.FindAsync(id);

            if (module == null)
            {
                return NotFound();
            }

            return module;
        }

        // GET api/<ModuleController>/5
        [HttpGet]
        [Route("name/{moduleName}")]
        public async Task<ActionResult<Module>> GetByName(string moduleName)
        {
            var module = await _context.Module.FirstOrDefaultAsync(m => m.Name == moduleName);

            if (module == null)
            {
                return NotFound();
            }

            return module;
        }

        // POST api/<ModuleController>
        [HttpPost]
        public async Task<ActionResult<Module>> Post([FromBody] Module value)
        {
            var moduleExists = await _context.Module.FirstOrDefaultAsync(m => m.Name == value.Name);

            if (moduleExists != null)
            {
                return StatusCode(403, "Already exists");
            }

            value.Id = Guid.NewGuid().ToString();
            var res = await _context.Module.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<ModuleController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] Module value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<ModuleController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var module = await _context.Module.FindAsync(id);
            if (module == null)
            {
                return NotFound();
            }

            _context.Module.Remove(module);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
