﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class BillingPeriodController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public BillingPeriodController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<BillingPeriodController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BillingPeriod>>> Get()
        {
            var billingPeriods = await _context.BillingPeriod.ToListAsync();

            if (billingPeriods == null)
            {
                return NotFound();
            }

            return billingPeriods;
        }

        // GET api/<BillingPeriodController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<BillingPeriod>> Get(string id)
        {
            var billingPeriod = await _context.BillingPeriod.FindAsync(id);

            if (billingPeriod == null)
            {
                return NotFound();
            }

            return billingPeriod;

        }

        // POST api/<BillingPeriodController>
        [HttpPost]
        public async Task<ActionResult<BillingPeriod>> Post([FromBody] BillingPeriod value)
        {
            value.Id = Guid.NewGuid().ToString();
            var res = await _context.BillingPeriod.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<BillingPeriodController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] BillingPeriod value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<BillingPeriodController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var billingPeriod = await _context.BillingPeriod.FindAsync(id);
            if (billingPeriod == null)
            {
                return NotFound();
            }

            _context.BillingPeriod.Remove(billingPeriod);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
