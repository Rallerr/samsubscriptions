﻿using Microsoft.AspNetCore.Mvc;
using SAM.Economic.Integration.Client.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models.Economics;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EconomicsLayoutController : ControllerBase
    {

        RestFunctions restFunctions = new RestFunctions();

        // GET: api/<EconomicsLayoutController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Layout>>> Get()
        {
            var layouts = await restFunctions.GetLayoutsAsync();

            if (layouts == null)
            {
                return NotFound();
            }

            return Ok(layouts);
        }

        // GET api/<EconomicsLayoutController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<EconomicsLayoutController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<EconomicsLayoutController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EconomicsLayoutController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
