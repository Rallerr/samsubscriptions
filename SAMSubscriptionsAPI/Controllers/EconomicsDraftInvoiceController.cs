﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using SAM.Economic.Integration.Client.Logic;
using SAM.Economic.Integration.Client.Models;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;
using WebApi.EconomicsLogic;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EconomicsDraftInvoiceController : ControllerBase
    {
        RestFunctions restFunctions = new RestFunctions();

        private readonly SAMSubscriptionsDbContext _context;
        public EconomicsDraftInvoiceController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<EconomicsController>
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return null;
        }

        // GET api/<EconomicsController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "";
        }

        // POST api/<EconomicsController>
        [HttpPost("{enterpriseId}")]
        public async Task<DraftInvoice.Collection> PostInvoiceDraftAsync(string enterpriseId)
        {
            var enterprise = await _context.Enterprise.Where(e => e.Id == enterpriseId).FirstOrDefaultAsync();
            var subscription = await _context.Subscription.Where(s => s.EnterpriseId == enterpriseId).FirstOrDefaultAsync();
            var subscriptionType = await _context.SubscriptionType.Where(t => t.Id == subscription.SubscriptionTypeId).FirstOrDefaultAsync();

            var economicsCustomer = await restFunctions.GetCustomer(enterprise.EconomicsCustomerNumber);

            var activeModules = await _context.ActiveModule.Where(m => m.EnterpriseId == enterpriseId).ToListAsync();

            var activePriceMultipliers = await _context.ActivePriceMultiplier.Where(m => m.SubscriptionId == subscription.Id).ToListAsync();

            var concurrentUsers = await _context.ActiveConcurrentUsers.Where(u => u.SubscriptionId == subscription.Id).FirstOrDefaultAsync();

            var multiplierList = new List<PriceMultiplier>();

            List<NewInvoiceModel.Line> invoiceLines = new List<NewInvoiceModel.Line>();

            foreach (var item in activePriceMultipliers)
            {
                var multiplier = await _context.PriceMultiplier.Where(m => m.Id == item.PriceMultiplierId).FirstOrDefaultAsync();
                multiplierList.Add(multiplier);
            }

            var camoProduct = await restFunctions.GetProductByName("CAMO users");

            invoiceLines.Add(new NewInvoiceModel.Line
            {
                Product = new NewInvoiceModel.Product
                {
                    ProductNumber = int.Parse(camoProduct.productNumber)
                },
                Unit = new NewInvoiceModel.Unit
                {
                    unitNumber = camoProduct.unit.unitNumber
                },
                UnitCostPrice = 0,
                DiscountPercentage = subscription.Discount,
                TotalNetAmount = 0,
                UnitNetPrice = 0,
                Description = camoProduct.description,
                Quantity = (int)concurrentUsers.CAMO
            });

            var mroProduct = await restFunctions.GetProductByName("MRO users");

            invoiceLines.Add(new NewInvoiceModel.Line
            {
                Product = new NewInvoiceModel.Product
                {
                    ProductNumber = int.Parse(mroProduct.productNumber)
                },
                Unit = new NewInvoiceModel.Unit
                {
                    unitNumber = mroProduct.unit.unitNumber
                },
                UnitCostPrice = 0,
                DiscountPercentage = subscription.Discount,
                TotalNetAmount = 0,
                UnitNetPrice = 0,
                Description = mroProduct.description,
                Quantity = (int)concurrentUsers.MRO
            });

            foreach (var item in activeModules)
            {
                var module = await _context.Module.Where(m => m.Id == item.ModuleId).FirstOrDefaultAsync();

                if (module.EconomicsProductId == null)
                {
                    continue;
                }

                var settingValues = await _context.ModuleSettingValue.Where(v => v.ActiveModuleId == item.Id).ToListAsync();

                var group = await _context.ModuleGroup.Where(g => g.Id == module.GroupId).FirstOrDefaultAsync();

                var modulePrice = DraftInvoicelogic.CalculateModulePrice(module.Price, subscriptionType.Type, group.Name, settingValues, multiplierList, concurrentUsers.CAMO, concurrentUsers.MRO);

                var economicsProduct = await restFunctions.GetProduct(module.EconomicsProductId);

                var settingValueQuantity = 0;
                var settingValueName = "";

                foreach (var value in settingValues)
                {
                    settingValueQuantity += int.Parse(value.Value);
                    settingValueName = _context.ModuleSetting.Where(s => s.Id == value.ModuleSettingId).FirstOrDefault().Name;
                }

                invoiceLines.Add(new NewInvoiceModel.Line
                {

                    Product = new NewInvoiceModel.Product
                    {
                        ProductNumber = int.Parse(module.EconomicsProductId)
                    },
                    Unit = new NewInvoiceModel.Unit
                    {
                        unitNumber = economicsProduct.unit.unitNumber
                    },
                    UnitCostPrice = (double)modulePrice,
                    DiscountPercentage = subscription.Discount,
                    TotalNetAmount = (double)modulePrice,
                    UnitNetPrice = (double)modulePrice,
                    Description = settingValueQuantity != 0 ? module.Name + " " + settingValueQuantity + " " + settingValueName : module.Name,
                    Quantity = 1 //settingValueQuantity != 0 ? settingValueQuantity : 1
                });

                if (module.FirsttimePrice > 0 && item.FirsttimePayment == true)
                {
                    invoiceLines.Add(new NewInvoiceModel.Line
                    {
                        Product = new NewInvoiceModel.Product
                        {
                            ProductNumber = int.Parse(module.EconomicsProductId)
                        },
                        Unit = new NewInvoiceModel.Unit
                        {
                            unitNumber = 1
                        },
                        UnitCostPrice = (double)module.FirsttimePrice,
                        DiscountPercentage = subscription.Discount,
                        TotalNetAmount = (double)module.FirsttimePrice,
                        UnitNetPrice = (double)module.FirsttimePrice,
                        Description = "Startup price",
                        Quantity = 1
                    });
                }

                await DraftInvoicelogic.UpdateActiveModule(item, _context);
                var paymentFrequency = await _context.PaymentFrequency.Where(f => f.Id == module.PaymentFrequencyId).FirstOrDefaultAsync();
                if (paymentFrequency.Name == "Onetime")
                {
                    await DraftInvoicelogic.SetActiveModuleInactive(item, _context, settingValues);
                }

            }
            await DraftInvoicelogic.UpdateActiveConcurrentUsers(concurrentUsers, _context);
            foreach (var item in activePriceMultipliers)
            {
                await DraftInvoicelogic.UpdateActivePriceMultiplier(item, _context);
            }


            var newInvoice = new NewInvoiceModel.DraftInvoice
            {
                currency = economicsCustomer.currency,
                date = DateTime.Now.ToString("yyyy-MM-dd"),
                dueDate = DateTime.Now.AddDays(10).ToString("yyyy-MM-dd"),
                customer = new NewInvoiceModel.Customer { customerNumber = economicsCustomer.customerNumber },
                layout = new NewInvoiceModel.Layout { layoutNumber = economicsCustomer.layout.layoutNumber },
                paymentTerms = new NewInvoiceModel.PaymentTerms { paymentTermsNumber = economicsCustomer.paymentTerms.paymentTermsNumber },
                recipient = new NewInvoiceModel.Recipient
                {
                    address = economicsCustomer.address,
                    city = economicsCustomer.city,
                    name = economicsCustomer.name,
                    zip = economicsCustomer.zip,
                    vatZone = new NewInvoiceModel.Vatzone
                    {
                        vatZoneNumber = economicsCustomer.vatZone.vatZoneNumber
                    }
                },
                Lines = invoiceLines.ToArray()
            };

            var createdDraftInvoice = await restFunctions.CreateDraftInvoice(newInvoice);

            return createdDraftInvoice;
        }

        // PUT api/<EconomicsController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EconomicsController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
