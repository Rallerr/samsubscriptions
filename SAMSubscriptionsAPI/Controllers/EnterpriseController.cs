﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EnterpriseController : ControllerBase
    {
        private readonly SAMSubscriptionsDbContext _context;
        public EnterpriseController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<EnterpriseController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Enterprise>>> Get()
        {
            var enterprises = await _context.Enterprise.ToListAsync();

            if (enterprises == null)
            {
                return NotFound();
            }

            return enterprises;
        }

        // GET: api/<EnterpriseController>
        [HttpGet]
        [Route("subscription/")]
        public async Task<ActionResult<IEnumerable<Enterprise>>> GetWithSubscription()
        {
           var enterprises = await _context.Enterprise.Where(e => _context.Subscription.Any(l => e.Id == l.EnterpriseId)).ToListAsync();


            if (enterprises == null)
            {
                return NotFound();
            }

            return enterprises;
        }

        [HttpGet]
        [Route("NoSubscription")]
        public async Task<ActionResult<IEnumerable<Enterprise>>> GetWithoutSubscription()
        {
            var enterprises = await _context.Enterprise.Where(e => _context.Subscription.Any(l => e.Id != l.EnterpriseId)).ToListAsync();

            if (enterprises == null)
            {
                return NotFound();
            }

            return enterprises;
        }


        // GET api/<EnterpriseController>/5
        [HttpGet("{id}")]
        public async Task<Enterprise> GetById(string id)
        {
            var enterprise = await _context.Enterprise.FindAsync(id);
            return enterprise;
        }

        // GET api/<EnterpriseController>/name/
        [HttpGet]
        [Route("/api/[controller]/name/{name}")]
        public async Task<ActionResult<Enterprise>> GetByName(string name)
        {
            var enterprise = await _context.Enterprise.FirstOrDefaultAsync(e => e.Name == name);

            if (enterprise == null)
            {
                return NotFound();
            }

            return enterprise;
        }

        // GET api/<EnterpriseController>/enterpriseno/
        [HttpGet]
        [Route("/api/[controller]/enterpriseNo/{enterpriseno}")]
        public async Task<ActionResult<Enterprise>> GetByEnterpriseNo(string enterpriseNo)
        {
            var enterprise = await _context.Enterprise.FirstOrDefaultAsync(e => e.EnterpriseNo.Equals(enterpriseNo));

            if (enterprise == null)
            {
                return NotFound();
            }

            return enterprise;
        }

        // POST api/<EnterpriseController>
        [HttpPost]
        public async Task<ActionResult<Enterprise>> Post([FromBody] Enterprise value)
        {
            try
            {
                var enterpriseExists = _context.Enterprise.FirstOrDefault(e => e.Name == value.Name);

                if (enterpriseExists != null)
                {
                    return StatusCode(403, "Already exists");
                }
            }
            catch (Exception)
            {

            }


            /*if (prevEnterpriseNo == null)
            {
                prevEnterpriseNo = 10000;
            }

            value.EnterpriseNo = prevEnterpriseNo + 1;*/
            value.Id = Guid.NewGuid().ToString();
            var res = await _context.Enterprise.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;

        }

        // PUT api/<EnterpriseController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(string id, [FromBody] Enterprise value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<EnterpriseController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(string id)
        {
            var enterprise = await _context.Enterprise.FindAsync(id);
            if (enterprise == null)
            {
                return NotFound();
            }

            _context.Enterprise.Remove(enterprise);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
