﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PaymentFrequencyController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public PaymentFrequencyController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<PaymentFrequencyController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PaymentFrequency>>> Get()
        {
            var frequencies = await _context.PaymentFrequency.ToListAsync();

            if(frequencies == null)
            {
                return NotFound();
            }

            return frequencies;

        }

        // GET api/<PaymentFrequencyController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<PaymentFrequency>> Get(string id)
        {
            var frequency = await _context.PaymentFrequency.FindAsync(id);

            if (frequency == null)
            {
                return NotFound();
            }

            return frequency;
        }

        // POST api/<PaymentFrequencyController>
        [HttpPost]
        public async Task<ActionResult<PaymentFrequency>> Post([FromBody] PaymentFrequency value)
        {
            value.Id = Guid.NewGuid().ToString();
            var res = await _context.PaymentFrequency.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;

        }

        // PUT api/<PaymentFrequencyController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] PaymentFrequency value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<PaymentFrequencyController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var paymentFrequency = await _context.PaymentFrequency.FindAsync(id);
            if (paymentFrequency == null)
            {
                return NotFound();
            }

            _context.PaymentFrequency.Remove(paymentFrequency);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
