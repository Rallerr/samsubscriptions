﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class RequiredModuleController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public RequiredModuleController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<RequiredModuleController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<RequiredModule>>> Get()
        {
            var reqModules = await _context.RequiredModule.ToListAsync();

            if (reqModules == null)
            {
                return NotFound();
            }

            return reqModules;
        }

        // GET: api/<RequiredModuleController>
        [HttpGet]
        [Route("moduleId/{moduleId}")]
        public async Task<ActionResult<IEnumerable<RequiredModule>>> GetByModuleId(string moduleId)
        {
            var reqModules = await _context.RequiredModule.Where(m => m.ModuleId == moduleId).ToListAsync();

            if (reqModules == null)
            {
                return NotFound();
            }

            return reqModules;
        }

        // GET api/<RequiredModuleController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<RequiredModule>> Get(string id)
        {
            var reqModule = await _context.RequiredModule.FindAsync(id);

            if (reqModule == null)
            {
                return NotFound();
            }

            return reqModule;
        }

        // POST api/<RequiredModuleController>
        [HttpPost]
        public async Task<ActionResult<RequiredModule>> Post([FromBody] RequiredModule value)
        {
            var moduleExists = await _context.Module.Where(m => m.Id == value.ModuleId).FirstOrDefaultAsync();

            if (moduleExists == null)
            {
                return BadRequest();
            }

            var reqModuleExists = await _context.Module.Where(m => m.Id == value.RequiredModuleId).FirstOrDefaultAsync();

            if (reqModuleExists == null)
            {
                return BadRequest();
            }

            value.Id = Guid.NewGuid().ToString();
            var res = await _context.RequiredModule.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<RequiredModuleController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] RequiredModule value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<RequiredModuleController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var reqModule = await _context.RequiredModule.FindAsync(id);
            if (reqModule == null)
            {
                return NotFound();
            }

            _context.RequiredModule.Remove(reqModule);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
