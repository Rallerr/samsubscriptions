﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public CurrencyController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }


        // GET: api/<CurrencyController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Currency>>> Get()
        {
            var currencies = await _context.Currency.ToListAsync();

            if (currencies == null)
            {
                return NotFound();
            }

            return currencies;
        }

        // GET api/<CurrencyController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Currency>> Get(string id)
        {
            var currency = await _context.Currency.FindAsync(id);

            if (currency == null)
            {
                return NotFound();
            }

            return currency;
        }

        // POST api/<CurrencyController>
        [HttpPost]
        public async Task<ActionResult<Currency>> Post([FromBody] Currency value)
        {
            value.Id = Guid.NewGuid().ToString();
            var res = await _context.Currency.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<CurrencyController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] Currency value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<CurrencyController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var currency = await _context.Currency.FindAsync(id);
            if (currency == null)
            {
                return NotFound();
            }

            _context.Currency.Remove(currency);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
