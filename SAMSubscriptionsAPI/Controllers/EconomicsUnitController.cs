﻿using Microsoft.AspNetCore.Mvc;
using SAM.Economic.Integration.Client.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Models.Economics;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EconomicsUnitController : ControllerBase
    {

        RestFunctions restFunctions = new RestFunctions();
        // GET: api/<EconomicsUnitController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Unit>>> Get()
        {
            var units = await restFunctions.GetUnitsAsync();

            if (units == null)
            {
                return NotFound();
            }

            return units;
        }

        // GET api/<EconomicsUnitController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<EconomicsUnitController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<EconomicsUnitController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EconomicsUnitController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
