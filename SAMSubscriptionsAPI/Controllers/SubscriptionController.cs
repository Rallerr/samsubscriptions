﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class SubscriptionController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public SubscriptionController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<SubscriptionController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Subscription>>> Get()
        {
            var subscriptions = await _context.Subscription.ToListAsync();

            if(subscriptions == null)
            {
                return NotFound();
            }

            return subscriptions;
        }



        // GET: api/<SubscriptionController>
        [HttpGet]
        [Route("enterprise/{enterpriseId}")]
        public async Task<ActionResult<Subscription>> GetByEnterpriseId(string enterpriseId)
        {
            var subscription = await _context.Subscription.Where(s => s.EnterpriseId == enterpriseId).FirstOrDefaultAsync();

            if (subscription == null)
            {
                return NotFound();
            }

            return subscription;
        }

        // GET api/<SubscriptionController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Subscription>> Get(string id)
        {
            var subscription = await _context.Subscription.FindAsync(id);

            if (subscription == null)
            {
                return NotFound();
            }

            return subscription;
        }

        // POST api/<SubscriptionController>
        [HttpPost]
        public async Task<ActionResult<Subscription>> Post([FromBody] Subscription value)
        {
            var subscriptionExists = await _context.Subscription.
                FirstOrDefaultAsync(s => s.EnterpriseId == value.EnterpriseId);

            if (subscriptionExists != null)
            {
                return StatusCode(403, "Already exists");
            }

            value.Id = Guid.NewGuid().ToString();
            var res = await _context.Subscription.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<SubscriptionController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] Subscription value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<SubscriptionController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var subscription = await _context.Subscription.FindAsync(id);
            if (subscription == null)
            {
                return NotFound();
            }

            _context.Subscription.Remove(subscription);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
