﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ActivePriceMultiplierController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public ActivePriceMultiplierController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<ActivePriceMultiplierController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActivePriceMultiplier>>> Get()
        {
            var activePriceMultipliers = await _context.ActivePriceMultiplier.ToListAsync();
            
            if (activePriceMultipliers == null)
            {
                return NotFound();
            }

            return activePriceMultipliers;
        }

        // GET: api/<ActivePriceMultiplierController/SubscriptionId>
        [HttpGet]
        [Route("subscriptionId/{subscriptionId}")]
        public async Task<ActionResult<IEnumerable<ActivePriceMultiplier>>> GetBySubscriptionId(string subscriptionId)
        {
            var activePriceMultipliers = await _context.ActivePriceMultiplier.Where(pm => pm.SubscriptionId == subscriptionId).ToListAsync();

            if (activePriceMultipliers == null)
            {
                return NotFound();
            }

            return activePriceMultipliers;
        }

        // GET api/<ActivePriceMultiplierController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActivePriceMultiplier>> Get(string id)
        {
            var activePriceMultiplier = await _context.ActivePriceMultiplier.FindAsync(id);

            if (activePriceMultiplier == null)
            {
                return NotFound();
            }

            return activePriceMultiplier;
        }

        // POST api/<ActivePriceMultiplierController>
        [HttpPost]
        public async Task<ActionResult<ActivePriceMultiplier>> Post([FromBody] ActivePriceMultiplier value)
        {

            var priceMultiplier = await _context.PriceMultiplier.FindAsync(value.PriceMultiplierId);

            if (priceMultiplier == null)
            {
                return BadRequest();
            }

            var activeMultiplierExists = await _context.ActivePriceMultiplier
                .Where(m => m.SubscriptionId == value.SubscriptionId && m.PriceMultiplierId == value.PriceMultiplierId)
                .FirstOrDefaultAsync();

            if (activeMultiplierExists != null)
            {
                return StatusCode(403, "Already exists");
            }

            value.Id = Guid.NewGuid().ToString();
            var res = await _context.ActivePriceMultiplier.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<ActivePriceMultiplierController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] ActivePriceMultiplier value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            var priceMultiplier = await _context.PriceMultiplier.FindAsync(value.PriceMultiplierId);

            if (priceMultiplier == null)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<ActivePriceMultiplierController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var activePriceMultiplier = await _context.ActivePriceMultiplier.FindAsync(id);
            if (activePriceMultiplier == null)
            {
                return NotFound();
            }

            _context.ActivePriceMultiplier.Remove(activePriceMultiplier);
            await _context.SaveChangesAsync();

            return NoContent();
        }

    }
}
