﻿using Microsoft.AspNetCore.Mvc;
using SAM.Economic.Integration.Client.Logic;
using SAM.Economic.Integration.Client.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EconomicsVatZoneController : ControllerBase
    {

        RestFunctions restFunctions = new RestFunctions();
        // GET: api/<EconomicsVatZoneController>
        [HttpGet]
        public ActionResult<IEnumerable<VatZones.Collection[]>> Get()
        {
            var vatZones = restFunctions.GetVatZone();

            if (vatZones == null)
            {
                return NotFound(); 
            }

            return Ok(vatZones);
        }

        // GET api/<EconomicsVatZoneController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<EconomicsVatZoneController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<EconomicsVatZoneController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EconomicsVatZoneController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
