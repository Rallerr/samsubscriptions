﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class InactiveConcurrentUsersController : ControllerBase
    {
        private readonly SAMSubscriptionsDbContext _context;
        public InactiveConcurrentUsersController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<InactiveConcurrentUsersController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<InactiveConcurrentUsers>>> Get()
        {
            var concurrentUsers = await _context.InactiveConcurrentUsers.ToListAsync();

            if (concurrentUsers == null)
            {
                return NotFound();
            }

            return concurrentUsers;
        }

        // GET api/<InactiveConcurrentUsersController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<InactiveConcurrentUsers>> Get(string id)
        {
            var concurrentUsers = await _context.InactiveConcurrentUsers.FindAsync(id);

            if (concurrentUsers == null)
            {
                return NotFound();
            }

            return concurrentUsers;
        }


        //comment
        [HttpGet]
        [Route("notpaid/{subscriptionId}")]
        public async Task<ActionResult<InactiveConcurrentUsers>> GetNotPaidUsers(string subscriptionId)
        {
            var concurrentUsers = await _context.InactiveConcurrentUsers
                .Where(item => item.SubscriptionId == subscriptionId && item.Paid == false)
                .FirstOrDefaultAsync();

            if (concurrentUsers == null)
            {
                return NotFound();
            }

            return concurrentUsers;
        }

        // GET api/<InactiveConcurrentUsersController>/enterpriseId/
        [HttpGet]
        [Route("enterpriseId/{id}")]
        public async Task<ActionResult<IEnumerable<InactiveConcurrentUsers>>> GetByEnterpriseId(string id)
        {
            var concurrentUsers = await _context.InactiveConcurrentUsers.Where(u => u.EnterpriseId == id).ToListAsync();

            if (concurrentUsers == null)
            {
                return NotFound();
            }

            return concurrentUsers;
        }

        [HttpGet]
        [Route("subscriptionId/{id}")]
        public async Task<ActionResult<IEnumerable<InactiveConcurrentUsers>>> GetBySubscriptionId(string id)
        {
            var concurrentUsers = await _context.InactiveConcurrentUsers.Where(u => u.SubscriptionId == id).ToListAsync();

            if (concurrentUsers == null)
            {
                return NotFound();
            }

            return concurrentUsers;
        }

        // POST api/<InactiveConcurrentUsersController>
        [HttpPost]
        public async Task<ActionResult<InactiveConcurrentUsers>> Post([FromBody] InactiveConcurrentUsers value)
        {

            value.Id = Guid.NewGuid().ToString();
            var res = await _context.InactiveConcurrentUsers.AddAsync(value);
            await _context.SaveChangesAsync();
            return res.Entity;
        }

        // PUT api/<InactiveConcurrentUsersController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] InactiveConcurrentUsers value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<InactiveConcurrentUsersController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var concurrentUsers = await _context.InactiveConcurrentUsers.FindAsync(id);
            if (concurrentUsers == null)
            {
                return NotFound();
            }

            _context.InactiveConcurrentUsers.Remove(concurrentUsers);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
