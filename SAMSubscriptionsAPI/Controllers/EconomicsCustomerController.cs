﻿using Microsoft.AspNetCore.Mvc;
using SAM.Economic.Integration.Client.Logic;
using SAM.Economic.Integration.Client.Models;
using SAMSubscriptionsAPI.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;
using SAM.Economic.Integration.Client.Models;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EconomicsCustomerController : ControllerBase
    {

        RestFunctions restFunctions = new RestFunctions();

        private readonly SAMSubscriptionsDbContext _context;
        public EconomicsCustomerController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<EconomicsCustomerController>
        [HttpGet]
        public IEnumerable<CustomerModel.CustomerObject> GetCustomers()
        {
            var customers = restFunctions.Customers();
            return customers;
        }

        // GET api/<EconomicsCustomerController>/5
        [HttpGet]
        [Route("id/{id}")]
        public async Task<ActionResult<CustomerModel.CustomerObject>> GetCustomerById(int id)
        {
            var customer = await restFunctions.GetCustomer(id);

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }

        // GET api/<EconomicsCustomerController>/5
        [HttpGet]
        [Route("name/{name}")]
        public ActionResult<CustomerModel.CustomerObject> GetCustomerByName(string name)
        {
            var customers = restFunctions.Customers();

            var customer = customers.Where(c => c.name == name).FirstOrDefault();

            if (customer == null)
            {
                return NotFound();
            }

            return Ok(customer);
        }


        // POST api/<EconomicsCustomerController>
        [HttpPost]
        public async Task<ActionResult<CustomerModel.CustomerObject>> PostCustomer([FromBody] CustomerModel.CustomerObject value)
        {
            var customers = restFunctions.Customers();

            var customer = customers.Where(c => c.name == value.name).FirstOrDefault();

            if (customer != null)
            {
                return BadRequest("The customer name already exists");
            }

            value.customerNumber = (int)restFunctions.GetCustomerNumber();

            var createdCustomer = await restFunctions.CreateCustomer(value);

            return Ok(createdCustomer);
        }

        // PUT api/<EconomicsCustomerController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult<CustomerModel.CustomerObject>> Put(int id, [FromBody] CustomerModel.CustomerObject value)
        {
            var updatedCustomer = await restFunctions.UpdateCustomer(id, value);

            if (updatedCustomer == null)
            {
                return BadRequest();
            }

            return Ok(updatedCustomer);
        }

        // DELETE api/<EconomicsCustomerController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
