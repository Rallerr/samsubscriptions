using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Models.Users;
using WebApi.Services;

namespace WebApi.Controllers
{
    [Authorize]
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [AllowAnonymous]
        [HttpPost("[action]")]
        public IActionResult Authenticate(AuthenticateRequest model)
        {
            var response = _userService.Authenticate(model);
            return Ok(response);
        }

        [Authorize(Role.Admin)]
        [HttpGet]
        public IActionResult GetAll()
        {
            var users = _userService.GetAll();
            return Ok(users);
        }

        [HttpGet("{id:int}")]
        public IActionResult GetById(string id)
        {
            // only admins can access other user records
            var currentUser = (User)HttpContext.Items["User"];
            if (!id.Equals(currentUser.Id) && currentUser.Role != Role.Admin)
                return Unauthorized(new { message = "Unauthorized" });

            var user =  _userService.GetById(id);
            return Ok(user);
        }

        [Authorize(Role.Admin)]
        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> RegisterAsync(RegisterRequest user)
        {
            var newUser = new User {
                Username = user.Username,
                Role = user.Role,
                Password = user.Password,
                EnterpriseId = user.EnterpriseId
            };

            var createdUser = await _userService.RegisterAsync(newUser);
            return Ok(createdUser); ;
        }

        [AllowAnonymous]
        [HttpGet]
        [Route("validate/{token}")]
        public bool ValidateToken(string token)
        {
            var isTokenActive = _userService.ValidateToken(token);

            return isTokenActive;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("token")]
        public string GetToken(AuthenticateRequest model)
        {
            var token = _userService.GetToken(model);

            return token;
        } 
    }
}
