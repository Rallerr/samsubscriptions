﻿using Microsoft.AspNetCore.Mvc;
using SAM.Economic.Integration.Client.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace WebApi.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class EconomicsCustomerGroupController : ControllerBase
    {

        RestFunctions restFunctions = new RestFunctions();

        // GET: api/<EconomicsCustomerGroupController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<string>>> Get()
        {
            var customerGroups = await restFunctions.GetCustomerGroups();

            if (customerGroups == null)
            {
                return NotFound();
            }

            return Ok(customerGroups);
        }

        // GET api/<EconomicsCustomerGroupController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<EconomicsCustomerGroupController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<EconomicsCustomerGroupController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<EconomicsCustomerGroupController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
