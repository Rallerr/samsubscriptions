﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;
//using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [ApiController]
    public class HubspotController : ControllerBase
    {
        

        // GET: api/<HubspotController>
        [HttpGet]
        [Route("api/[controller]/enterprise")]
        public async Task<string> Get()
        {
            var apikey = Environment.GetEnvironmentVariable("HUBSPOT_API_KEY");
            var path = $"https://api.hubapi.com/crm/v3/objects/companies/search?hapikey={apikey}";
            var client = new RestClient(path);
            var request = new RestRequest(Method.POST);
            request.AddHeader("accept", "application/json");
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"filterGroups\":[{\"filters\":[{\"propertyName\": \"software\",\"operator\": \"EQ\",\"value\": \"SAM-CAMO\"},{\"propertyName\": \"lifecyclestage\", \"operator\": \"EQ\",\"value\": \"customer\"}]}],\"limit\": \"100\"}", ParameterType.RequestBody);
            IRestResponse response = await client.ExecuteAsync(request);

            return response.Content;
        }

        // GET api/<HubspotController>/5
        [HttpGet]
        [Route("/api/[controller]/enterprise/{id}")]
        public async Task<string> Get(string id)
        {
            var apikey = Environment.GetEnvironmentVariable("HUBSPOT_API_KEY");
            var path = $"https://api.hubapi.com/companies/v2/companies/{id}?hapikey={apikey}";

            var client = new RestClient(path);
            var request = new RestRequest(Method.GET);
            request.AddHeader("accept", "application/json");
            request.AddHeader("content-type", "application/json");
            IRestResponse response = await client.ExecuteAsync(request);

            return response.Content;
        }
    }
}
