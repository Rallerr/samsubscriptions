﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Contexts
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ActiveConcurrentUsersController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public ActiveConcurrentUsersController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<ConcurrentUsersController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActiveConcurrentUsers>>> Get()
        {
            var concurrentUsers = await _context.ActiveConcurrentUsers.ToListAsync();

            if (concurrentUsers == null)
            {
                return NotFound();
            }

            return concurrentUsers;
        }

        // GET api/<ConcurrentUsersController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActiveConcurrentUsers>> Get(string id)
        {
            var concurrentUsers = await _context.ActiveConcurrentUsers.FindAsync(id);

            if (concurrentUsers == null)
            {
                return NotFound();
            }

            return concurrentUsers;
        }

        // GET api/<ConcurrentUsersController>/enterpriseId/
        [HttpGet]
        [AllowAnonymous]
        [Route("enterpriseId/{id}")]
        public async Task<ActionResult<ActiveConcurrentUsers>> GetByEnterpriseId(string id)
        {
            var concurrentUsers = await _context.ActiveConcurrentUsers.FirstOrDefaultAsync(ccu => ccu.EnterpriseId == id);

            if (concurrentUsers == null)
            {
                return NotFound();
            }

            return concurrentUsers;
        }

        [HttpGet]
        [Route("subscriptionId/{id}")]
        public async Task<ActionResult<ActiveConcurrentUsers>> GetBySubscriptionId(string id)
        {
            var concurrentUsers = await _context.ActiveConcurrentUsers.FirstOrDefaultAsync(u => u.SubscriptionId == id);

            if (concurrentUsers == null)
            {
                return NotFound();
            }

            return concurrentUsers;
        }

        // POST api/<ConcurrentUsersController>
        [HttpPost]
        public async Task<ActionResult<ActiveConcurrentUsers>> Post([FromBody] ActiveConcurrentUsers value)
        {
            var activeUsersExists = await _context.ActiveConcurrentUsers
                .Where(u => u.SubscriptionId == value.SubscriptionId || u.EnterpriseId == value.EnterpriseId)
                .FirstOrDefaultAsync();

            if (activeUsersExists != null) {
                return StatusCode(403, "Already exists");
            }

            value.Id = Guid.NewGuid().ToString();
            var res = await _context.ActiveConcurrentUsers.AddAsync(value);
            await _context.SaveChangesAsync();
            return res.Entity;
        }

        // PUT api/<ConcurrentUsersController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] ActiveConcurrentUsers value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<ConcurrentUsersController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var concurrentUsers = await _context.ActiveConcurrentUsers.FindAsync(id);
            if (concurrentUsers == null)
            {
                return NotFound();
            }

            _context.ActiveConcurrentUsers.Remove(concurrentUsers);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
