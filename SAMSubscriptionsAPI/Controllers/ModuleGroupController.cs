﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class ModuleGroupController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public ModuleGroupController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<ModuleGroupController>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ModuleGroup>>> Get()
        {
            var moduleGroups = await _context.ModuleGroup.ToListAsync();

            if (moduleGroups == null)
            {
                return NotFound();
            }

            return moduleGroups;
        }

        // GET api/<ModuleGroupController>/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ModuleGroup>> Get(string id)
        {
            var moduleGroup = await _context.ModuleGroup.FindAsync(id);

            if (moduleGroup == null)
            {
                return NotFound();
            }

            return moduleGroup;
        }

        // POST api/<ModuleGroupController>
        [HttpPost]
        public async Task<ActionResult<ModuleGroup>> Post([FromBody] ModuleGroup value)
        {
            var moduleGroupExists = await _context.Module.FirstOrDefaultAsync(m => m.Name == value.Name);

            if (moduleGroupExists != null)
            {
                return StatusCode(403, "Already exists");
            }

            value.Id = Guid.NewGuid().ToString();
            var res = await _context.ModuleGroup.AddAsync(value);
            await _context.SaveChangesAsync();

            return res.Entity;
        }

        // PUT api/<ModuleGroupController>/5
        [HttpPut("{id}")]
        public async Task<ActionResult> Put(string id, [FromBody] ModuleGroup value)
        {
            if (id != value.Id)
            {
                return BadRequest();
            }

            _context.Entry(value).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                return NotFound();
            }

            return NoContent();
        }

        // DELETE api/<ModuleGroupController>/5
        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(string id)
        {
            var moduleGroup = await _context.ModuleGroup.FindAsync(id);
            if (moduleGroup == null)
            {
                return NotFound();
            }

            _context.ModuleGroup.Remove(moduleGroup);
            await _context.SaveChangesAsync();

            return NoContent();
        }
    }
}
