﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Authorization;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace SAMSubscriptionsAPI.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class PriceController : ControllerBase
    {

        private readonly SAMSubscriptionsDbContext _context;
        public PriceController(SAMSubscriptionsDbContext context)
        {
            _context = context;
        }

        // GET: api/<PriceController>
        [HttpGet]
        [Route("nextBill/ConcurrentUsers/Monthly/{suscriptionId}")]
        public async Task<ActionResult<double>> Get(string subscriptionId)
        {
            var totalPrice = 0.0;

            var subscription = await _context.Subscription
                .Where(item => item.Id == subscriptionId)
                .FirstOrDefaultAsync();
            var activeModules = await _context.ActiveModule
                .Where(item => item.SubscriptionId == subscriptionId)
                .ToListAsync();
            var activePriceMultipliers = await _context.ActivePriceMultiplier
                .Where(item => item.SubscriptionId == subscriptionId)
                .ToListAsync();
            var activeConcurrentUsers = await _context.ActiveConcurrentUsers
                .Where(item => item.SubscriptionId == subscriptionId)
                .ToListAsync();

            



            return null;
        }

        // GET api/<PriceController>/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }

        // POST api/<PriceController>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/<PriceController>/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/<PriceController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
