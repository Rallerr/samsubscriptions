﻿using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebApi.Entities;

namespace SAMSubscriptionsAPI.Contexts
{
    public class SAMSubscriptionsDbContext : DbContext
    {
        public SAMSubscriptionsDbContext(DbContextOptions<SAMSubscriptionsDbContext> options) : base(options)
        {
        }

        //test comment
        public DbSet<Enterprise> Enterprise { get; set; }
        public DbSet<ActiveConcurrentUsers> ActiveConcurrentUsers { get; set; }
        public DbSet<InactiveConcurrentUsers> InactiveConcurrentUsers { get; set; }
        public DbSet<Currency> Currency { get; set; }
        public DbSet<PriceMultiplier> PriceMultiplier { get; set; }
        public DbSet<ActivePriceMultiplier> ActivePriceMultiplier { get; set; }
        public DbSet<InactivePriceMultiplier> InactivePriceMultiplier { get; set; }
        public DbSet<SubscriptionType> SubscriptionType { get; set; }
        public DbSet<BillingPeriod> BillingPeriod { get; set; }
        public DbSet<Module> Module { get; set; }
        public DbSet<RequiredModule> RequiredModule { get; set; }
        public DbSet<ActiveModule> ActiveModule { get; set; }
        public DbSet<ModuleGroup> ModuleGroup { get; set; }
        public DbSet<InactiveModule> InactiveModule { get; set; }
        public DbSet<ModuleSetting> ModuleSetting { get; set; }
        //public DbSet<EnabledModuleSetting> EnabledModuleSetting { get; set; }
        public DbSet<ModuleSettingValue> ModuleSettingValue { get; set; }
        public DbSet<Subscription> Subscription { get; set; }
        public DbSet<PaymentFrequency> PaymentFrequency { get; set; }
        public DbSet<User> User { get; set; }
    }
}
