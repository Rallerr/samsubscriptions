﻿#region

using System.Configuration;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using WebApi.Models.Economics;

#endregion

namespace SAM.Economic.Integration.Client.Logic
{
    public class CreateClient
    {
        public static WebClient EconomicHttpClient(EconomicCredentials economicCredentials = null)
        {
            var client = new WebClient();

            string secret = "";
            string partnerSecret = "";

            if (economicCredentials != null)
            {
                secret = economicCredentials.Secret;
                partnerSecret = economicCredentials.PartnerSecret;
            }
            else
            {
                secret = ConfigurationManager.AppSettings["EconomicsAppSecretToken"];
                partnerSecret = ConfigurationManager.AppSettings["EconomicsAgreementGrantTokens"];
            }

            Debug.WriteLine("SECRET SECRET SECRET " + secret);
            client.Headers.Add("X-AppSecretToken", secret);
            client.Headers.Add("X-AgreementGrantToken", partnerSecret);
            return client;
        }

        public static HttpClient EconomicAsyncClient(EconomicCredentials economicCredentials = null)
        {
            var client = new HttpClient();
            string secret = "";
            string partnerSecret = "";

            if (economicCredentials != null)
            {
                secret = economicCredentials.Secret;
                partnerSecret = economicCredentials.PartnerSecret;
            }
            else
            {
                secret = ConfigurationManager.AppSettings["EconomicsAppSecretToken"];
                partnerSecret = ConfigurationManager.AppSettings["EconomicsAgreementGrantTokens"];
            }
            client.DefaultRequestHeaders.Add("X-AppSecretToken", secret);
            client.DefaultRequestHeaders.Add("X-AgreementGrantToken", partnerSecret);
            return client;
        }
    }
}