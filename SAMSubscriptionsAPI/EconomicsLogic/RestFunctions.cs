﻿#region

using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SAM.Economic.Integration.Client.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using WebApi.Models.Economics;

#endregion

namespace SAM.Economic.Integration.Client.Logic
{
    public class RestFunctions
    {
        private const string BaseURL = " https://restapi.e-conomic.com/";
        private const string CustomersUri = "https://restapi.e-conomic.com/customers";

        public async Task<CustomerModel.CustomerObject> CreateCustomer(CustomerModel.CustomerObject customer)
        {
            var client = CreateClient.EconomicAsyncClient();

            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new NullToEmptyStringResolver()
            };

            var json = JsonConvert.SerializeObject(customer, settings);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(CustomersUri, content);

            client.Dispose();
            if (response.StatusCode != HttpStatusCode.Created)
            {
                var errorstring = await response.Content.ReadAsStringAsync();
                Debug.WriteLine("ERROR ERROR ERROR " + errorstring);
                return null;
            }

            var jsonString = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<CustomerModel.CustomerObject>(jsonString);
    }

        /// <summary>
        ///     Required:  currency, customer, date, layout, paymentTerms, recipient, recipient.name, recipient.vatZone
        ///     Example: https://restdocs.e-conomic.com/#post-invoices-drafts
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        public async Task<DraftInvoice.Collection> CreateDraftInvoice(NewInvoiceModel.DraftInvoice invoice)
        {
            const string invoiceURI = BaseURL + "invoices/drafts";

            var json = JsonConvert.SerializeObject(invoice);
            var client = CreateClient.EconomicAsyncClient();
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

            var response = await client.PostAsync(invoiceURI, content);

            client.Dispose();
            if (response.StatusCode != HttpStatusCode.Created)
            {
                var errorstring = await response.Content.ReadAsStringAsync();
                Debug.WriteLine("ERROR ERROR ERROR " + errorstring);
                return null;
            }

            var jsonString = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<DraftInvoice.Collection>(jsonString);
        }

        public async Task<Product> CreateProduct(Product product)
        {
            var client = CreateClient.EconomicAsyncClient();
            var json = JsonConvert.SerializeObject(product);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(BaseURL + "products/", content);
            var jsonResponse = await response.Content.ReadAsStringAsync();
            Debug.WriteLine("RESPONS RESPONS RESPONS " + jsonResponse);

            client.Dispose();

            if (response.StatusCode != HttpStatusCode.Created)
            {
                var errorstring = await response.Content.ReadAsStringAsync();
                return null;
            }

            return JsonConvert.DeserializeObject<Product>(await response.Content.ReadAsStringAsync());

        }

        public async Task<Product> UpdateProductAsync(string id, Product product)
        {
            var client = CreateClient.EconomicAsyncClient();
            var json = JsonConvert.SerializeObject(product);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PutAsync(BaseURL + "products/" + id, content);
            var jsonResponse = await response.Content.ReadAsStringAsync();
            

            client.Dispose();

            if (response.StatusCode != HttpStatusCode.OK)
            {
                var errorstring = await response.Content.ReadAsStringAsync();
                Debug.WriteLine("ERROR ERROR ERROR" + errorstring);
                return null;
            }

            return JsonConvert.DeserializeObject<Product>(await response.Content.ReadAsStringAsync());
        }

        public async Task<List<Product>> GetProducts()
        {
            var client = CreateClient.EconomicAsyncClient();

            var response = await client.GetAsync(BaseURL + "products/");

            if (response.StatusCode != HttpStatusCode.OK)
            {
                var errorstring = await response.Content.ReadAsStringAsync();
                Debug.WriteLine("ERROR ERROR ERROR " + errorstring);

                return null;
            }

            var products = JsonConvert.DeserializeObject<Product.RootObject>(await response.Content.ReadAsStringAsync());

            return products.collection;
        }

        public async Task<Product> GetProduct(string id)
        {
            var client = CreateClient.EconomicAsyncClient();

            var response = await client.GetAsync($"{BaseURL}/products/{id}");

            if (response.StatusCode != HttpStatusCode.OK)
            {
                var errorString = await response.Content.ReadAsStringAsync();
                return null;
            }

            var product = JsonConvert.DeserializeObject<Product>(await response.Content.ReadAsStringAsync());
            return product;
        }

        public async Task<Product> GetProductByName(string name)
        {
            var products = await GetProducts();

            var product = products.Where(p => p.name == name).FirstOrDefault();

            return product;
        }

        public async Task<List<Layout>> GetLayoutsAsync ()
        {
            var client = CreateClient.EconomicAsyncClient();

            var response = await client.GetAsync(BaseURL + "layouts/");

            if (response.StatusCode != HttpStatusCode.OK)
            {
                var errorstring = await response.Content.ReadAsStringAsync();
                Debug.WriteLine("ERROR ERROR ERROR " + errorstring);

                return null;
            }

            var layouts = JsonConvert.DeserializeObject<Layout.RootObject>(await response.Content.ReadAsStringAsync());

            return layouts.collection;
        }

        public List<CustomerModel.CustomerObject> Customers()
        {
            var client = CreateClient.EconomicHttpClient();
            var response = client.DownloadString(CustomersUri);
            var customers = JsonConvert.DeserializeObject<CustomerModel.RootObject>(response);

            client.Dispose();
            return customers.collection;
        }

        public async Task<CustomerModel.CustomerObject> UpdateCustomer(int id, CustomerModel.CustomerObject customer)
        {
            var client = CreateClient.EconomicAsyncClient();

            var settings = new JsonSerializerSettings()
            {
                ContractResolver = new NullToEmptyStringResolver()
            };

            var json = JsonConvert.SerializeObject(customer, settings);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PutAsync(CustomersUri + "/" + id, content);

            client.Dispose();
            if (response.StatusCode != HttpStatusCode.OK)
            {
                var errorstring = await response.Content.ReadAsStringAsync();
                Debug.WriteLine("ERROR ERROR ERROR " + errorstring);
                return null;
            }

            var jsonString = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<CustomerModel.CustomerObject>(jsonString);
        }

        public async Task<CustomerModel.CustomerObject> GetCustomer(int customerNumber)
        {
            var client = CreateClient.EconomicAsyncClient();
            var response = await client.GetAsync($"{CustomersUri}/{customerNumber}");
            var customer = JsonConvert.DeserializeObject<CustomerModel.CustomerObject>(await response.Content.ReadAsStringAsync());

            client.Dispose();
            return customer;
        }

        public int? GetCustomerNumber()
        {
            try
            {
                var startNumber = int.Parse(ConfigurationManager.AppSettings["CustomerStartNumber"]);
                List<CustomerModel.CustomerObject> customers = Customers();
                while (customers.Any(n => n.customerNumber == startNumber)) startNumber++;

                return startNumber;
            }
            catch (Exception)
            {
                return null;
            }
        }

        

        public async Task<string> GetProductNumber()
        {
            try
            {
                var startNumber = ConfigurationManager.AppSettings["ProductStartNumber"];
                List<Product> customers = await GetProducts();
                while (customers.Any(n => n.productNumber.Equals(startNumber))) { var number = int.Parse(startNumber); number++; startNumber = number.ToString(); };

                return startNumber;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<ProductGroup> GetProductGroups()
        {
            var client = CreateClient.EconomicHttpClient();
            var response = client.DownloadString(BaseURL + "product-groups/");
            var productGroups = JsonConvert.DeserializeObject<ProductGroup.RootObject>(response);

            client.Dispose();
            return productGroups.collection;
        }

        public DraftInvoice.Rootobject GetInvoiceDrafts(int customerNumber, EconomicCredentials economicCredentials)
        {
            var client = CreateClient.EconomicHttpClient(economicCredentials);
            var response = client.DownloadString($"https://restapi.e-conomic.com/customers/{customerNumber}/invoices/drafts");

            client.Dispose();
            return JsonConvert.DeserializeObject<DraftInvoice.Rootobject>(response);
        }

        public byte[] GetInvoicePDF(string url)
        {
            var client = CreateClient.EconomicHttpClient();
            var response = client.DownloadData(url);

            client.Dispose();
            return response;
        }

        public BookedInvoice.Rootobject GetInvoices(int customerNumber)
        {
            var client = CreateClient.EconomicHttpClient();
            var response = client.DownloadString($"https://restapi.e-conomic.com/customers/{customerNumber}/invoices/booked");

            client.Dispose();
            return JsonConvert.DeserializeObject<BookedInvoice.Rootobject>(response);
        }

        public BookedInvoice.Rootobject GetInvoice(int invoiceNumber)
        {
            var client = CreateClient.EconomicHttpClient();
            var response = client.DownloadString($"https://restapi.e-conomic.com/invoices/booked/{invoiceNumber}");

            client.Dispose();
            return JsonConvert.DeserializeObject<BookedInvoice.Rootobject>(response);
        }


        public List<int> GetLayoutNumbers(EconomicCredentials economicCredentials)
        {
            var client = CreateClient.EconomicHttpClient(economicCredentials);
            var response = client.DownloadString(BaseURL + "layouts");

            client.Dispose();
            return new List<int>();
        }

        public async Task<List<Unit>> GetUnitsAsync()
        {
            var client = CreateClient.EconomicAsyncClient();

            var response = await client.GetAsync(BaseURL + "units/");

            if (response.StatusCode != HttpStatusCode.OK)
            {
                var errorstring = await response.Content.ReadAsStringAsync();
                Debug.WriteLine("ERROR ERROR ERROR " + errorstring);

                return null;
            }

            var units = JsonConvert.DeserializeObject<Unit.RootObject>(await response.Content.ReadAsStringAsync());

            return units.collection;
        }

        public PaymentTerms.Collection[] GetPaymentTerms()
        {
            var client = CreateClient.EconomicHttpClient();
            var response = client.DownloadString(BaseURL + "/payment-terms");

            var paymentTerms = JsonConvert.DeserializeObject<PaymentTerms.Rootobject>(response);

            client.Dispose();
            return paymentTerms.collection;
        }

        public async Task<string> GetCustomerGroups ()
        {
            var client = CreateClient.EconomicAsyncClient();
            var response = await client.GetAsync(BaseURL + "/customer-groups");
            var responseString = await response.Content.ReadAsStringAsync();
            client.Dispose();
            return responseString;
        }

        public List<int> GetProductNumbers(EconomicCredentials economicCredentials)
        {
            var client = CreateClient.EconomicHttpClient(economicCredentials);
            var response = client.DownloadString(BaseURL + "products");
            client.Dispose();
            return new List<int>();
        }

        public VatZones.Collection[] GetVatZone()
        {
            var client = CreateClient.EconomicHttpClient();
            var response = client.DownloadString(BaseURL + "/vat-zones");

            var vatZones = JsonConvert.DeserializeObject<VatZones.Rootobject>(response);

            client.Dispose();
            return vatZones.collection;
        }

        /// <summary>
        ///     Required: draftInvoiceNumber
        /// </summary>
        /// <param name="invoice"></param>
        /// <returns></returns>
        public async Task<BookedInvoice.Collection> IssueDraftInvoice(int draftInvoiceNumber, EconomicCredentials economicCredentials)
        {
            HttpClient client = CreateClient.EconomicAsyncClient(economicCredentials);
            var json = @"{'draftInvoice': {'draftInvoiceNumber':" + draftInvoiceNumber + "}}";
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");
            var response = await client.PostAsync(BaseURL + "invoices/booked/", content);
            client.Dispose();
            if (response.IsSuccessStatusCode)
            {
                var jsonString = await response.Content.ReadAsStringAsync();
                return JsonConvert.DeserializeObject<BookedInvoice.Collection>(jsonString);
            }
            return null;
        }

        public bool BarCustomer(int economicCustomerNumber, EconomicCredentials economicCredentials)
        {
            var customerEconomicsUrl = BaseURL + $"customers/{economicCustomerNumber}";
            var client2 = CreateClient.EconomicAsyncClient(economicCredentials);
            var client = CreateClient.EconomicHttpClient(economicCredentials);
            var json = client.DownloadString(customerEconomicsUrl);
            dynamic customer = JsonConvert.DeserializeObject(json);
            customer.barred = true;
            string returnjson = JsonConvert.SerializeObject(customer);
            var responseMessage = client2.PutAsync(customerEconomicsUrl, new StringContent(returnjson, Encoding.UTF8, "application/json")).Result;
            if (responseMessage.IsSuccessStatusCode)
            {
                return true;
            }
            return false;
        }

        public async Task<JournalGet.Rootobject> GetJournals(EconomicCredentials economicCredentials)
        {
            var journalUrl = BaseURL + $"journals-experimental";
            var client = CreateClient.EconomicAsyncClient(economicCredentials);
            var responseMessage = await client.GetAsync(journalUrl);
            var json = await responseMessage.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<JournalGet.Rootobject>(json);
        }

        public async Task<bool> CreateJournalEntry(List<JournalVoucher.Coordinate> journalVouchers, int journalNumber, EconomicCredentials economicCredentials)
        {
            var client = CreateClient.EconomicAsyncClient(economicCredentials);

            foreach (var journalVoucher in journalVouchers)
            {
                var journalUrl = BaseURL + $"journals-experimental/{journalNumber}/vouchers";
                var json = JsonConvert.SerializeObject(journalVoucher);
                HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

                var responseMessage = await client.PostAsync(journalUrl, content);
                var message = await responseMessage.Content.ReadAsStringAsync();
                if (!responseMessage.IsSuccessStatusCode)
                {
                    return false;
                }
            }

            return true;
        }

        public async Task<List<JournalPostback.Class1>> CreateJournalEntry(JournalVoucher.Coordinate journalVoucher, int journalNumber, EconomicCredentials economicCredentials)
        {
            var journalUrl = BaseURL + $"journals-experimental/{journalNumber}/vouchers";
            var json = JsonConvert.SerializeObject(journalVoucher);
            HttpContent content = new StringContent(json, Encoding.UTF8, "application/json");

            var client = CreateClient.EconomicAsyncClient(economicCredentials);
            var responseMessage = await client.PostAsync(journalUrl, content);

            var responseString = await responseMessage.Content.ReadAsStringAsync();
            if (responseMessage.IsSuccessStatusCode)
            {
                return JsonConvert.DeserializeObject<List<JournalPostback.Class1>>(responseString);
            }

            return null;
        }
    }

    public class NullToEmptyStringResolver : DefaultContractResolver
    {
        protected override IList<JsonProperty> CreateProperties(Type type, MemberSerialization memberSerialization)
        {
            return type.GetProperties()
                .Select(p =>
                {
                    var jp = base.CreateProperty(p, memberSerialization);
                    jp.ValueProvider = new NullToEmptyStringValueProvider(p);
                    return jp;
                }).ToList();
        }
    }

    public class NullToEmptyStringValueProvider : IValueProvider
    {
        private PropertyInfo _MemberInfo;

        public NullToEmptyStringValueProvider(PropertyInfo memberInfo)
        {
            _MemberInfo = memberInfo;
        }

        public object GetValue(object target)
        {
            var result = _MemberInfo.GetValue(target);
            if (_MemberInfo.PropertyType == typeof(string) && result == null) result = "";
            return result;
        }

        public void SetValue(object target, object value)
        {
            _MemberInfo.SetValue(target, value);
        }
    }
}