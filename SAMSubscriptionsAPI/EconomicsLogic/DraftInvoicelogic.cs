﻿using Microsoft.EntityFrameworkCore;
using SAMSubscriptionsAPI.Contexts;
using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApi.EconomicsLogic
{
    public static class DraftInvoicelogic
    {

        public static double? CalculateModulePrice (double? moduleBasePrice, 
            string subscriptionType, 
            string moduleGroup, 
            List<ModuleSettingValue> settingValues, List<PriceMultiplier> priceMultipliers, int? camoUsers, int? mroUsers)
        {

            var totalPrice = moduleBasePrice;

            if (subscriptionType.Equals("Concurrent Users", StringComparison.OrdinalIgnoreCase)) {
                switch(moduleGroup)
                {
                    case "Installation":
                        var totalUsers = camoUsers + mroUsers;
                        totalPrice = totalPrice * totalUsers;
                        break;

                    case "CAMO":
                        totalPrice = totalPrice * camoUsers;
                        break;

                    case "MRO":
                        totalPrice = totalPrice * mroUsers;
                        break;

                    default:
                        break;
                }
            }

            foreach (var item in priceMultipliers)
            {
                totalPrice = totalPrice * item.Factor;
            }

            foreach (var item in settingValues)
            {
                totalPrice = totalPrice * double.Parse(item.Value);
            }

            return totalPrice;
        }

        public static async Task UpdateActiveModule (ActiveModule activeModule, SAMSubscriptionsDbContext context)
        {
            activeModule.FirsttimePayment = false;
            activeModule.LastPaid = DateTime.Now.Date;
            activeModule.NextPayment = GetNextPaymentDate().AddDays(-1);

            context.Entry(activeModule).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
            }
        }

        public static async Task UpdateActiveConcurrentUsers(ActiveConcurrentUsers users, SAMSubscriptionsDbContext context)
        {
            users.LastPaid = DateTime.Now.Date;
            users.NextPayment = GetNextPaymentDate().AddDays(-1);

            context.Entry(users).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
            }
        }

        public static async Task UpdateActivePriceMultiplier(ActivePriceMultiplier priceMultiplier, SAMSubscriptionsDbContext context)
        {
            priceMultiplier.LastPaid = DateTime.Now.Date;
            priceMultiplier.NextPayment = GetNextPaymentDate().AddDays(-1);

            context.Entry(priceMultiplier).State = EntityState.Modified;

            try
            {
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
            }
        }

        public static async Task SetActiveModuleInactive (ActiveModule activeModule, SAMSubscriptionsDbContext context, List<ModuleSettingValue> settingValues = null)
        {
            var inactiveModule = new InactiveModule
            {
                Id = Guid.NewGuid().ToString(),
                EnterpriseId = activeModule.EnterpriseId,
                FirsttimePayment = activeModule.FirsttimePayment,
                Expiration = activeModule.NextPayment,
                ModuleId = activeModule.ModuleId,
                Paid = true,
                Start = activeModule.LastPaid != null ? activeModule.LastPaid : activeModule.Start,
                SubscriptionId = activeModule.SubscriptionId
            };

            await context.InactiveModule.AddAsync(inactiveModule);
            await context.SaveChangesAsync();

            //Delete active module
            context.ActiveModule.Remove(activeModule);
            await context.SaveChangesAsync();

            if (settingValues != null)
            {
                foreach (var item in settingValues)
                {
                    item.ActiveModuleId = null;
                    item.InactiveModuleId = inactiveModule.Id;

                    context.Entry(item).State = EntityState.Modified;

                    try
                    {
                        await context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                    }
                }
            }
        }

        public static DateTime GetNextPaymentDate ()
        {
            DateTime reference = DateTime.Now;
            DateTime firstDayThisMonth = new DateTime(reference.Year, reference.Month, 1);
            DateTime firstDayPlusTwoMonths = firstDayThisMonth.AddMonths(2);
            DateTime lastDayNextMonth = firstDayPlusTwoMonths.AddDays(-1);
            DateTime endOfLastDayNextMonth = firstDayPlusTwoMonths.AddTicks(-1);

            return endOfLastDayNextMonth;
        }

    }
}
