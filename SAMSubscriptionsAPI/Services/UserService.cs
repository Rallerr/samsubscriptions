using BCryptNet = BCrypt.Net.BCrypt;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Options;
using WebApi.Authorization;
using WebApi.Entities;
using WebApi.Helpers;
using WebApi.Models.Users;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using System;

namespace WebApi.Services
{
    public interface IUserService
    {
        AuthenticateResponse Authenticate(AuthenticateRequest model);
        IEnumerable<User> GetAll();
        User GetById(string id);
        Task<User> RegisterAsync(User user);

        bool ValidateToken(string token);
        string GetToken(AuthenticateRequest model);
    }

    public class UserService : IUserService
    {
        private DataContext _context;
        private IJwtUtils _jwtUtils;
        private readonly AppSettings _appSettings;

        public UserService(
            DataContext context,
            IJwtUtils jwtUtils,
            IOptions<AppSettings> appSettings)
        {
            _context = context;
            _jwtUtils = jwtUtils;
            _appSettings = appSettings.Value;
        }


        public AuthenticateResponse Authenticate(AuthenticateRequest model)
        {
            var user = _context.User.SingleOrDefault(x => x.Username == model.Username);

            // validate
            if (user == null || !BCryptNet.Verify(model.Password, user.Password))
                throw new AppException("Username or password is incorrect");

            // authentication successful so generate jwt token
            var jwtToken = _jwtUtils.GenerateJwtToken(user);

            return new AuthenticateResponse(user, jwtToken);
        }

        public string GetToken(AuthenticateRequest model)
        {
            var user = _context.User.SingleOrDefault(x => x.Username == model.Username);

            // validate
            if (user == null || !BCryptNet.Verify(model.Password, user.Password))
                throw new AppException("Username or password is incorrect");

            // authentication successful so generate jwt token
            var jwtToken = _jwtUtils.GenerateJwtToken(user);

            return jwtToken;
        }

        public IEnumerable<User> GetAll()
        {
            return _context.User;
        }

        public User GetById(string id) 
        {
            var user = _context.User.Find(id);
            if (user == null) throw new KeyNotFoundException("User not found");
            return user;
        }

        public async Task<User> RegisterAsync(User user) {
            var userExists = await _context.User.Where(u => u.Username == user.Username).FirstOrDefaultAsync();

            var newUser = new User
            {
                Id = Guid.NewGuid().ToString(),
                Username = user.Username,
                Password = BCryptNet.HashPassword(user.Password),
                Role = user.Role,
                EnterpriseId = user.EnterpriseId
            };
            if (userExists == null)
            {

                await _context.User.AddAsync(newUser);
                await _context.SaveChangesAsync();

                
            } else
            {
                throw new ArgumentException("Username already exists"); 
            }

            return newUser;
        }

        public bool ValidateToken(string token)
        {
            var userId = _jwtUtils.ValidateJwtToken(token);

            if (userId != null)
            {
                return true;
            }

            return false;
            //return isTokenValid;
        }
    }
}