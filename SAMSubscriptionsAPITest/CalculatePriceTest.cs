using SAMSubscriptionsAPI.Models;
using System;
using System.Collections.Generic;
using WebApi.EconomicsLogic;
using Xunit;

namespace SAMSubscriptionsAPITest
{
    public class CalculatePriceTest
    {
        [Fact]
        public void TestModulePriceCalculation()
        {
            var modulePrice = 100;
            var subscriptionType = "Concurrent Users";
            var moduleGroup = "CAMO";
            var moduleSettingValues = new List<ModuleSettingValue>();
            var priceMultipliers = new List<PriceMultiplier> { new PriceMultiplier
            {
                Factor = 1.5,
                Id = "1",
                Title = "MTOW > 5700kg",
                Group = "MTOW",
                Type = "checkbox"
            } };

            var camoUsers = 4;
            var mroUsers = 2;

            var calculatedModulePrice = DraftInvoicelogic.CalculateModulePrice(modulePrice, subscriptionType, moduleGroup, moduleSettingValues,
                priceMultipliers, camoUsers, mroUsers);

            Assert.Equal(600, calculatedModulePrice);
        }
    }
}
